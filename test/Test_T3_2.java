import java.util.LinkedList;

import Sequenic.T3.* ;
import Sequenic.T3.Sequence.Datatype.SUITE;
import static Sequenic.T3.Generator.GenCombinators.* ;

public class Test_T3_2 {
	
	static public void main(String[] args) throws Exception {
		
	   Config config = new Config() ;
	   config.CUT = LinkedList.class ;
	   config.setDirsToClasses("./bin") ;
	   CustomSuiteGenAPI sg = new CustomSuiteGenAPI(null,config) ;
	   sg.scope.configureForADTtesting();
	     
	   SUITE S = sg.suite(SequenceWhile(
	        		r -> ! r.isFail(),
	        		sg.create(),
	        		sg.mutatorsSegment(10),
	        		sg.instrument(),
	        		sg.method("add"),
	        		sg.method("remove"),
	        		sg.nonMutatorsSegment(3)
	        		),
	        		500
	        		) ;

	    sg.reportCoverage(S) ; 
	    sg.replay(S) ;
	   
	}
}
