package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;
import java.util.*;

public class ClassWithGenericMethod {
	
    static private void trace(String m, Object o) {
		TestLogger.log("** " + m + " FALSE: " + o.getClass()) ;
		System.out.println("** " + m + " gets " + o.getClass());
    }
	

    static private void trace(String m, Object o1, Object o2) {
		TestLogger.log("** " + m + " FALSE: " + o1.getClass() + ", " + o2.getClass()) ;
		System.out.println("** " + m + " gets " + o1.getClass() + ", " + o2.getClass());
    }
     
    
    abstract public static class A {}
    public static class A1 extends B {}
    public static class A2 extends B {}
    
    public static class B {}
    public static class B1 extends B {}
    public static class B2 extends B implements IX {}
    
    public static interface IX {}
    
    public static class C<V>{ V item ; public C(V x) { item = x ; } }
    public static class C1<V> extends C<V> {  public C1(V x) { super(x) ; } }
    public static class C2<V> extends C<V> {  public C2(V x) { super(x) ; } }
    
    
	public void standard(Collection o) {
		if (o == null) return ;
		if (!(o instanceof Collection)) trace("standard",o) ;
	}
	

	public <T extends B> void tyvarWithUpperBound(T o) {
		String mname = "tyvarWithUpperBound" ; 
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		if (o != null && !(o instanceof B)) trace(mname,o) ;
	}
	
	// this is nastier... T3 CANNOT actually generate a call to this method, because it does not know
	// how to instantiate A (well... it is an abstract class)
	public <T extends A> void tyvarWith_AbstractUpperBound(T o) {
		String mname = "tyvarWith_AbstractUpperBound" ; 
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		if (o != null && !(o instanceof A)) trace(mname,o) ;
	}
	
	public <T extends C<Double>> void tyvarWith_ParameterizedUpperBound(T o) {
		if (o == null) return ;
		String mname = "tyvarWith_ParameterizedUpperBound" ;
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		if (!(o instanceof C)) trace(mname,o) ;
		if (o.item == null) return ;
		System.out.println(">> " + mname + ", o.item: " + o.item.getClass()) ;
		if (!(o.item instanceof Double)) trace(mname,o.item) ;
	}
	
	public <T extends C<C<Integer>>> void tyvarWith_NestedParameterizedUpperBound(T o) {
		if (o == null) return ;
		String mname = "tyvarWith_NestedParameterizedUpperBound" ;
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		if (!(o instanceof C)) trace(mname,o) ;
		if (o.item == null) return ;
		System.out.println(">> " + mname + ", o.item: " + o.item.getClass()) ;
		if (!(o.item instanceof C)) trace(mname,o.item) ;
		if (o.item.item == null) return ;
		System.out.println(">> " + mname + ", o.item.item: " + o.item.item.getClass()) ;
		if (!(o.item.item instanceof Integer)) trace(mname,o.item.item) ;
	}
	

	// T3 currently unable to handle this case of multiple bounds:
	public <T extends B & IX> void tyvarWithMultipleUpperBounds(T o) {
		String mname = "tyvarWithMultipleUpperBounds" ; 
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		if (o != null && !(o instanceof B2)) trace(mname,o) ;
	}
	
	public <T> void linkedTogether_tyvars(T o1, T o2) {
		if (o1==null || o2==null) return ;
		String mname = "linkedTogether_tyvars" ;
		System.out.println(">> " + mname + ": " + o1.getClass() + ", " + o2.getClass()) ;
		if (!o1.getClass().equals(o2.getClass())) trace(mname,o1,o2) ;
	}
	
	public <T extends B> void linkedTogether_tyvars_withUpperBound(T o1, T o2) {
		if (o1==null || o2==null) return ;
		String mname = "linkedTogether_tyvars_withUpperBound" ;
		System.out.println(">> " + mname + ": " + o1.getClass() + ", " + o2.getClass()) ;
		if (!(o1 instanceof B)) trace(mname,o1) ;
		if (!o1.getClass().equals(o2.getClass())) trace(mname,o1,o2) ;
	}
	
	public void wildcard_unbounded(C<?> o) {
		if (o==null || o.item==null) return ;
		String mname = "wildcard_unbounded" ; 
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		System.out.println(">> " + mname + ", o.item: " + o.item.getClass()) ;
		if (!(o instanceof C)) trace(mname,o) ;
	}
	
	
	public void wildcard_with_upperbound(C<? extends B> o) {
		if (o==null || o.item==null) return ;
		String mname = "wildcard_with_upperbound" ; 
		System.out.println(">> " + mname + ": " + o.getClass()) ;
		System.out.println(">> " + mname + ", o.item: " + o.item.getClass()) ;
		if (!(o instanceof C)) trace(mname,o) ;
		if (!(o.item instanceof B)) trace(mname,o.item) ;
	}
	
	// wildcard with multiple upperbounds is not allowed in Java, so far
	// it is also not possible to have a wildcard with both lower and upperbound
	// it is also not possible to have a type-variable with a lower bound
	
	public void wildcard_with_lowerbound(C<? super B1> o) {
		if (o==null && o.item != null) return ;
		String mname = "wildcard_with_lowerbound" ; 
		System.out.println(">> " + mname + ", o.item: " + o.item.getClass()) ;
		Class C_ = o.item.getClass() ;
		if (! (C_.equals(B1.class) || C_.equals(B.class) || C_.equals(Object.class))) trace(mname,o.item) ;
	}
}
