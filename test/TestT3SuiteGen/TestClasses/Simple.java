package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;

public class Simple {
	
    public void fpublic() { TestLogger.log("fpublic") ; }	
    protected void fprotected() { TestLogger.log("fprotected") ; }	
    void fdeafult() { TestLogger.log("fdefault") ; }	
    private void fprivate() { TestLogger.log("fprivate") ; }	

}
