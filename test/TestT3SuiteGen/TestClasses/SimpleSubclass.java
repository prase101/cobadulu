package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;

public class SimpleSubclass extends Simple {

	public void newMethod() { TestLogger.log("newmethod") ; }
	
	@Override
	public void fpublic() { TestLogger.log("override public") ; }
		
	@Override
	void fdeafult() { TestLogger.log("override default") ; }
}
