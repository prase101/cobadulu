package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;

public class CreationMethod {
	
	Integer x = null ;
	
	static class JustAclass { }
	
	private CreationMethod(int x) { this.x = x ; }
	
	public static CreationMethod xxxCreationMethod() { 
		TestLogger.log("creation method") ;
		return new CreationMethod(10) ; 
	}
	
	public void foo() { if (x==10) TestLogger.log("foo") ; }

}
