package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;

public class Test_CreationMethod {
	
	@BeforeClass 
	public static void runT3() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = CreationMethod.class ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(null,config) ;
	    t3.suite(true) ;
	}

	@Test
	public void test0() {    
	    assertTrue(TestLogger.contain("foo")) ;
	    assertTrue(TestLogger.contain("creation method")) ;  
	}
}
