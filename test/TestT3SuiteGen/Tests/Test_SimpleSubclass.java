package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;

public class Test_SimpleSubclass {

	// Test that inherited and overshadowed methods are accounted correctly
	@Test
	public void test0() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = SimpleSubclass.class ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(null,config) ;
	    t3.suite(true) ;
	    assertTrue(TestLogger.containSegment("newmethod")) ;
	    assertTrue(TestLogger.contain("override public")) ;
	    assertFalse(TestLogger.contain("fpublic")) ;
	    assertTrue(TestLogger.contain("fprotected")) ;
	    assertTrue(TestLogger.contain("override default")) ;
	    assertFalse(TestLogger.contain("fdefault")) ;
	}
	
}
