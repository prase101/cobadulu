package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;

public class Test_ClassWithGenericMethod {
	
	@BeforeClass 
	public static void runT3() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = ClassWithGenericMethod.class ;
		config.regressionMode = true ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(null,config) ;
	    t3.suite(true) ;
	}

	@Test
	public void test_tyvar() { 
		assertFalse(TestLogger.containSegment("standard FALSE")) ;
		assertFalse(TestLogger.containSegment("tyvarWithUpperBound FALSE")) ;
		assertFalse(TestLogger.containSegment("tyvarWith_AbstractUpperBound FALSE")) ;
		assertFalse(TestLogger.containSegment("tyvarWith_ParameterizedUpperBound FALSE")) ;
		assertFalse(TestLogger.containSegment("tyvarWith_NestedParameterizedUpperBound FALSE")) ;
		assertFalse(TestLogger.containSegment("linkedTogether_tyvars FALSE")) ;
		assertFalse(TestLogger.containSegment("linkedTogether_tyvars_withUpperBound FALSE")) ;
	}
	
	@Test
	public void test_tyvarWithMultipleUpperBounds() {
		// currently T3 cannot handle this case:
		assertFalse(TestLogger.containSegment("tyvarWithMultipleUpperBounds FALSE")) ;
	}
	
	@Test
	public void test_wildcard() {
		assertFalse(TestLogger.containSegment("wildcard_unbounded FALSE")) ;
		assertFalse(TestLogger.containSegment("wildcard_with_upperbound FALSE")) ;
		assertFalse(TestLogger.containSegment("wildcard_with_lowerbound FALSE")) ;
	}
}
