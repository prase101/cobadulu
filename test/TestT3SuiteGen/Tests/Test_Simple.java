package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;

import Sequenic.T3.* ;


public class Test_Simple {

	// Test if visibility of methods are accounted correctly
	@Test
	public void test0() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = Simple.class ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(null,config) ;
	    t3.suite(true) ;
	    assertTrue(TestLogger.containSegment("public")) ;
	    assertTrue(TestLogger.containSegment("protected")) ;
	    assertTrue(TestLogger.containSegment("default")) ;
	    assertFalse(TestLogger.containSegment("private")) ;
	}
	
	@Test
	public void test1() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = Simple.class ;
		T3SuiteGenAPI t3 =  T3SuiteGenAPI.mkT3SuiteGenAPI(null, config) ;
	    t3.suite(true) ;
	    assertTrue(TestLogger.containSegment("public")) ;
	    assertTrue(TestLogger.containSegment("protected")) ;
	    assertTrue(TestLogger.containSegment("default")) ;
	    assertFalse(TestLogger.containSegment("private")) ;
	}
}
