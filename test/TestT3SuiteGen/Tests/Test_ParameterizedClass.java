package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;

public class Test_ParameterizedClass {
	
	@BeforeClass 
	public static void runT3() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = ParameterizedClass.class ;
		config.regressionMode = false ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(null,config) ;
	    t3.suite(true) ;
	}

	@Test
	public void test_tyvar() {
		assertFalse(TestLogger.containSegment("m1 FALSE")) ;
	}
	
	@Test
	public void test_tyvar_withUpperbound() {
		assertFalse(TestLogger.containSegment("m2 FALSE")) ;
	}
	
}
