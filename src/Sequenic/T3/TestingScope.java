package Sequenic.T3;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Examples.Cycles.PersonCycle;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Reflection.Reflection;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Representing the constructors, methods, and fields that are to be included in testing.
 */
public class TestingScope {

    /**
     * The class targeted for testing. This should not be an abstract nor interface.
     */
    public Class CUT ;

    /**
     * The proposed concrete type of CUT. If the CUT has type-variables; these will be
     * instantiated to concrete types specified by this JType.
     */
    //public JType CUTconcreteType ;  --> no longer needed.

    public ImplementationMap impsMap ;
    


    /**
     *  When true (default), will include all non-private members in the testing scope;
     *  else only public members are included.
     */
    public boolean testingFromTheSamePackagePespective = true ;
    
    // HACK... force certain constructors to be included, e.g. private ones:
    public List<Constructor> forceConstructors   = new LinkedList<Constructor>() ;

    
    public List<Constructor> constructors   = new LinkedList<Constructor>() ;
    public List<Method>      creatorMethods = new LinkedList<Method>() ;
    public List<Method> methods = new LinkedList<Method>();
    public List<Field> fields = new LinkedList<Field>() ;
    
    public List<Method> mutators = new LinkedList<Method>();
    public List<Method> nonMutators = new LinkedList<Method>();


    public TestingScope(ImplementationMap impsMap, Class CUT) {
       assert ! CUT.isInterface() ;
       this.impsMap = impsMap ;
       this.CUT = CUT ;
       // tySubstitution = Solver.solveClassTyvars(imap, JTypeUtils.naiveClassdecl2JType(CUT)) ;
       // CUTconcreteType = JTypeUtils.T3instantiateTypeParams(CUT) ;
    }
    
    /**
     * Clearing the scope.
     */
    public void clear() {
    	methods.clear();
    	constructors.clear();
    	fields.clear();
    	creatorMethods.clear();
    	mutators.clear();
    	nonMutators.clear();
    }
      
    private static boolean isMutator(Method M) {
        String name = M.getName() ;
        if (name == "size" 
        		|| name == "length" 
        		|| name == "toString"
                || name == "toArray"
                || name == "toInt"
        		|| name == "equals"
        		|| name == "clone"
        		|| name == "hashCode"       		
        		|| name == "indexOf"      		

           ) return false ;
        return !(name.startsWith("get") || name.startsWith("is") || name.startsWith("contains")) ;
    }
    
    private void calculateMutators() {
    	for (Method M : methods) {
    		if (isMutator(M)) mutators.add(M) ;
    		else nonMutators.add(M) ;
    	}
    }
    
    /**
     * Configure the scope for ADT-based testing.
     */
    public void configureForADTtesting() {
    	clear() ;
        // get the constructors in scope
        constructors =  getAccessibleConstructors()  ;
        // Hack:
        constructors.addAll(forceConstructors) ;
        // get the methods in scope
        List<Method> allMethods = getAccessibleMethods() ;
        for (Method M : allMethods) {
            if (Modifier.isStatic(M.getModifiers())) {
                // only include a static method if it actually takes a parameter of type CUT
                if (hasParameterOfExactlyThisType(M,CUT))  methods.add(M) ;
            }
            else methods.add(M) ;
        }
        // get the fields in scope (static fields are already excluded)
        for (Field F : getAccessibleFields()) {
            fields.add(F) ;
        }
        // get the creation methods
        for (Method M : allMethods) {
            if (M.getDeclaringClass() == CUT) {
                if (! Modifier.isStatic(M.getModifiers())) continue ;
                if (M.getReturnType() != CUT) continue ;
                if (hasParameterOfThisType(M,CUT)) continue ;
                creatorMethods.add(M) ;
            }
        }
        // split mutators and non-mutators:
        calculateMutators() ;
    }


    /**
     * Configure the scope for non-ADT-based testing. By default we will only include
     * static methods and fields in the scope. In principle we can also test an
     * abstract class in a non-ADT way, but we should then only include non-asbtract
     * methods.
     */
    public void configureForNonADTTesting() {
    	clear();
        for (Method M : getAccessibleMethods()) {
            int mod = M.getModifiers() ;
            if (Modifier.isStatic(mod)) {
                methods.add(M) ;
            }
        }        
        // static fields are to be excluded!!
        //for (Field F : getAccessibleFields()) {
        //    if (Modifier.isStatic(F.getModifiers()))
        //       fields.add(F) ;
        //}
        // split mutators and non-mutators:
        calculateMutators() ;
    }

    /**
     * Get all constructors of the specified signature. If no signature is given, then 
     * all members of constructors will be returned. If just one class is specified, any
     * constructor that takes a parameter of that class is considered a match. 
     * If c contains two or more classes, the a constructor that takes parameters of the
     * specified types-prefix is considered a match.
     */
    public List<Constructor> constructors(Class ... c) {
    	List<Constructor> candidates = new LinkedList<Constructor>() ;
    	if (constructors.isEmpty()) return candidates ;
    	if(c.length==0) {
    		return constructors ;
    	}
    	if(c.length==1) {
    	   for (Constructor C : constructors) {
    		   for(Class argty : C.getParameterTypes()) {
    			   if (argty==c[0]) {
    				   candidates.add(C) ;
    				   break ;
    			   }
    		   }
    	   }
    	}
    	else {// c.length > 1
    	  for (Constructor C : constructors) {
    	    Class[] paramTypes = C.getParameterTypes() ;
    	    if (c.length > paramTypes.length) continue ;
    	    boolean match = true ;
 		    for(int i=0; i<c.length; i++) {
 			   if (paramTypes[i]!=c[i]) {
 				   match = false ;
 				   break ;
 			   }
 		    }
 		    if (match) candidates.add(C) ;
  	      }
    	}
   	   return candidates ;
    }
    
    /**
     * Return all methods matching the signature. If name is null, return all methods.
     */
    static private List<Method> findMethod(List<Method> methods, String name, Class ... c) {
    	if (name==null) return methods ;
    	List<Method> candidates0 = new LinkedList<Method>() ;
    	if (methods.isEmpty()) return candidates0 ;
    	
    	for (Method M : methods) {
    		if (M.getName() == name) candidates0.add(M) ;
    	} 
    	
    	if(candidates0.isEmpty()) return candidates0 ;
    	
    	List<Method> candidates = new LinkedList<Method>() ;
    	if(c.length==1) {
    	   for (Method M : candidates0) {
    		   for(Class argty : M.getParameterTypes()) {
    			   if (argty==c[0]) {
    				   candidates.add(M) ;
    				   break ;
    			   }
    		   }
    	   }
    	}
    	else {// c.length > 1
    	  for (Method M : candidates0) {
    	    Class[] paramTypes = M.getParameterTypes() ;
    	    if (c.length > paramTypes.length) continue ;
    	    boolean match = true ;
 		    for(int i=0; i<c.length; i++) {
 			   if (paramTypes[i]!=c[i]) {
 				   match = false ;
 				   break ;
 			   }
 		    }
 		    if (match) candidates.add(M) ;
  	      }
    	}
   	   return candidates ;
    }
    
    public List<Method> methods(String name, Class... c) {
    	return findMethod(methods,name,c) ;
    }
    
    public List<Method> mutators(String name, Class... c) {
    	return findMethod(mutators,name,c) ;
    }
    
    public List<Method> creatorMethods(String name, Class... c) {
    	return findMethod(creatorMethods,name,c) ;
    }

    public List<Method> methods() {
    	return methods ;
    }
    
    public List<Method> mutators() {
    	return mutators ;
    }
    
    public List<Method> creatorMethods() {
    	return creatorMethods ;
    }
    /**
     * Filter the members in the scope using the given predicates.
     */
    public void filter(Predicate<Object> selector)
    {
        List<Constructor> selectedConstructors = new LinkedList<Constructor>() ;
        for (Constructor co : constructors)
            if (selector.test(co)) selectedConstructors.add(co) ;
        constructors = selectedConstructors ;

        List<Method> selectedMethods = new LinkedList<Method>() ;
        for (Method M : methods)
            if (selector.test(M)) selectedMethods.add(M) ;
        methods = selectedMethods ;
        calculateMutators() ;

        selectedMethods = new LinkedList<Method>() ;
        for (Method M : creatorMethods)
            if (selector.test(M)) selectedMethods.add(M) ;
        creatorMethods = selectedMethods ;

        List<Field> selectedFields = new LinkedList<Field>() ;
        for (Field F : fields)
            if (selector.test(F)) selectedFields.add(F) ;
        fields = selectedFields ;
    }


    /**
     * Check if the method m has a parameter of type D, to which we can pass
     * an instance of C (so, if D is an ancestor of C).
     */
    static private boolean hasParameterOfThisType(Method m, Class C) {
         for (Class param : m.getParameterTypes()) {
             if (param.isAssignableFrom(C)) return true ;
         }
         return false ;
    }

    /**
     * Check if the method m has a parameter of type C.
     */
    static private boolean hasParameterOfExactlyThisType(Method m, Class C) {
        for (Class param : m.getParameterTypes()) {
            if (param == C) return true ;
        }
        return false ;
    }


    /**
     * Return all methods of C that are visible and accessible.
     */
    private List<Method> getAccessibleMethods() {

        List<Method> methods = new LinkedList<Method>();
        List<Method> candidates = new LinkedList<Method>();

        for (Method M : CUT.getDeclaredMethods()) candidates.add(M) ;
        for (Class D : Reflection.getALLSuperClasses(CUT)) {
            for (Method M : D.getDeclaredMethods()) {
            	// if a method with the same name and type is already in the candidates, then M
            	// is overriden; so, not visible from the client:
            	boolean overriden = false ;
            	String mname = M.getName() ;
            	Class[] paramtypes = M.getParameterTypes() ;
            	for (Method K : candidates) {
            		if (! mname.equals(K.getName())) continue ;
            		if (K.getParameterTypes().length != paramtypes.length) continue ;
            		overriden = true ;
            		for(int i=0; i<paramtypes.length ; i++) {
            			if (paramtypes[i] != K.getParameterTypes()[i]) {
            				overriden = false ;
            				break ;
            			}
            		}
            		if (overriden) break ;
            	}
                if (!overriden) candidates.add(M) ;
            }
        }

        for (Method M : candidates) {
            int mod = M.getModifiers() ;
            // if (Modifier.isAbstract(mod)) continue ;
            String name = M.getName() ;
            if (name.equals(CONSTANTS.classinv_name)
                    || name.startsWith("<")  // exclude <init> and <clinit>
               )
               continue;
            if (isVisible(M.getDeclaringClass(),M.getModifiers())) methods.add(M) ;
        }
        return methods ;
    }
    
    private boolean isVisible(Class declaringClass, int modifier) {
    	if (testingFromTheSamePackagePespective) {
    		if (declaringClass.getPackage() == CUT.getPackage())    		
    		    return (! Modifier.isPrivate(modifier)) ;
    		else 
    			return (Modifier.isPublic(modifier)) ;
    	}
    	else return (Modifier.isPublic(modifier)) ;
    }

    private List<Constructor> getAccessibleConstructors() {

        List<Constructor> constructors = new LinkedList<Constructor>();

        // an abstract class cannot be instantiated through its constructors,
        // even if it has one, so don't bother to add its constructors:
        if (Modifier.isAbstract(CUT.getModifiers())) {
        	return constructors ;
        }
        if (CUT.isInterface()) return constructors ;

        for (Constructor con : CUT.getDeclaredConstructors()) {
            //int mod = con.getModifiers() ;
            //if (Modifier.isAbstract(mod)) continue ;
            if (isVisible(con.getDeclaringClass(),con.getModifiers()))
                constructors.add(con) ;
        }
        return constructors ;
    }


    /**
     * Get all fields that are visible and accessible from the client package. 
     * 
     * NOTE: Static fields will be EXCLUDED, because updating them may cause a test sequence
     * to influence the next ones. hence making test sequences not independent of
     * each other. Note that this cannot be fully prevented, since invoking a method
     * can still indirectly change a static field. But at least the risk is mitigated.
     */
    private List<Field> getAccessibleFields() {

        List<Field> fields = new LinkedList<Field>();
        List<Field> candidates = new LinkedList<Field>();

        for (Field F : CUT.getDeclaredFields()) {
        	// exclude static fields:
        	if (Modifier.isStatic(F.getModifiers())) continue ;
        	candidates.add(F) ;
        }
        for (Class D : Reflection.getALLSuperClasses(CUT)) {
            for (Field F : D.getDeclaredFields()) {
            	// exclude static fields:
            	if (Modifier.isStatic(F.getModifiers())) continue ;
            	// if field of the same name is already in candidates, then F is shadowed; 
            	// and so not visible from the client:
            	boolean shadowed = false ;
            	String fname = F.getName() ;
            	for (Field G : candidates) {
            		
            		if (fname.equals(G.getName())) {
            			shadowed = true ;
            			break ;
            		}
            	}
            	if (!shadowed) candidates.add(F) ;
            }
        }

        for (Field F : candidates) {
            int mod = F.getModifiers() ;
            if (Modifier.isAbstract(mod)) continue ;
            if (Modifier.isFinal(mod)) continue ;
            String fname = F.getName() ;
            if (fname.equals("serialVersionUID")) continue ;
            if (fname.startsWith(CONSTANTS.auxField_prefix)) continue ;
            if (isVisible(F.getDeclaringClass(),F.getModifiers())) fields.add(F) ;
        }
        return fields ;
    }

    private String printParamTypes(Class[] params) {
    	String s = "(" ;
    	for (int k=0; k< params.length ; k++) {
    		if (k>0) s += ", " ;
    		s += params[k].getName() ;
    	}
    	s += ")" ;
    	return s ;
    }

    public String toString() {
        String s = "Testing scope:" ;
        String accessScope = "\nIncluding only public members" ;
        if (testingFromTheSamePackagePespective) accessScope = "\nIncluding non-private members" ;
        s += "CUT = " + CUT.getName() ;
        // s += "\nConcrete type = " + CUTconcreteType ;
        s += accessScope ;
        s += "\nConstructors: " + constructors.size()  ;
        for (Constructor C : constructors) {
            s += "\n  " + Modifier.toString(C.getModifiers()) + " " + C.getName() ;
            s += " " + printParamTypes(C.getParameterTypes()) ;
        }
        s += "\nCreator methods: " + creatorMethods.size() ;
        for (Method M : creatorMethods) {
            s += "\n   " + Modifier.toString(M.getModifiers()) + " " +  M.getName()  ;
            s += " " + printParamTypes(M.getParameterTypes()) ;
        }
        s += "\nMutators: "  + mutators.size() ;
        for (Method M : mutators) {
            s += "\n   " + Modifier.toString(M.getModifiers()) + " " +  M.getName()  ;
            s += " " + printParamTypes(M.getParameterTypes()) ;
        }
        s += "\nNon-mutators: "  + nonMutators.size() ;
        for (Method M : nonMutators) {
            s += "\n   " + Modifier.toString(M.getModifiers()) + " " +  M.getName()  ;
            s += " " + printParamTypes(M.getParameterTypes()) ;
        }
        s += "\nFields: " + fields.size() ;
        for (Field F : fields)
            s += "\n   " + Modifier.toString(F.getModifiers()) + " " + F.getName() ;
        // s += "\n---------------" ;
        return s ;
    }
    

    public static void main(String[] args) {
    	String[] dirs = {"./bin"} ; 
    	ImplementationMap imap = new ImplementationMap(dirs) ;
        TestingScope scope = new TestingScope(imap,LinkedList.class) ;
        scope.configureForADTtesting();
        System.out.println(scope.toString());

    }

}
