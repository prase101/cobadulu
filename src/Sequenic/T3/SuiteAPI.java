/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import Sequenic.T3.Info.ObjectCoverage;
import Sequenic.T3.Sequence.Datatype.SUITE;
import Sequenic.T3.Sequence.Datatype.SUITE_RT_info;


/**
 * Provides some basic APIs for suite, such as to load and replay suites.
 * This will also be the parent class of generator-API-classes. 
 */
public class SuiteAPI {

	public Config config;
	
	protected SuiteAPI() { }
	
	public SuiteAPI(Config config) { this.config = config ; }
	
		
	protected void flushReportStream() {
		try {
			config.reportOut.flush();
		}
		catch(Exception e) { }
	}
	
	public Logger getT3logger() {
		return Logger.getLogger(CONSTANTS.T3loggerName) ;
	}
	
	public void injectOracles(SUITE S) {
		S.injectOracles(config.CUT, config.numberOfCores<=1, config.maxSuffixLength+2) ;
	}
	
	/**
     * Load a single suite-file, given a full path to the file.
     */
    public SUITE load(String filename) throws Exception{
        return SUITE.load(filename) ;
    }

    /**
     * Load all suite-files in the given directory, whose names match the give reg-expr.
     */
    public List<SUITE> load(String regexpr, String dir) throws Exception {
          return SUITE.loadMany(regexpr,dir) ;
    }
	
	/**
	 * To replay a suite.
	 */
    public SUITE_RT_info replay(SUITE S) throws Exception {
    	config.reportWriteln("-----") ;
        config.reportWriteln("** Replaying " + S.suitename ) ;
        SUITE_RT_info info = S.exec(
        		new Pool(), 
        		new ObjectCoverage(config.CUT),
        		config.replayShowLength, 
        		config.replayShowDepth, 
        		config.replayShowExcExecution, 
        		config.replayRunAll, 
        		config.regressionMode, 
        		config.sequencePrintOut) ;
        config.sequencePrintOut.flush(); 
        flushReportStream() ;
        return info ;
    }

}
