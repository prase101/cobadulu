/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import Sequenic.T3.JavaType.JType;
import Sequenic.T3.Pool;
import Sequenic.T3.OracleError;
import Sequenic.T3.Oracle.*;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;
import Sequenic.T3.utils.StringFormater;
import Sequenic.T3.utils.Maybe;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * This represents a STEP for constructing an object by calling a constructor.
 * The constructor will be remembered in this step, along with the STEPs
 * needed to construct its parameters.
 */
public class CONSTRUCTOR extends STEP implements Serializable {
	
	private static final long serialVersionUID = 1L;

    /**
     * The concrete type of the object to create with this constructor.
     */
    public JType cty ;
    public Constructor con = null ;
    public STEP[] params = null ;

    /**
     * Flag to indicate whether the step is a step that creates the object under test.
     */
    public boolean isObjectUnderTestCreationStep = false ;
    
    /**
     * An oracle, if any. The default is just TT (true) as the oracle.
     */
    public Oracle oracle = Oracle.TT ;

    
   /**
     * @param c   The constructor used in this step.
     * @param ps  STEPs needed to generate parameters for c.
     */
    public CONSTRUCTOR(JType concreteType, Constructor c, STEP[] ps) {
        cty = concreteType ;
        con = c;
        params = ps;
    }

    public CONSTRUCTOR() { }

    @Override
    public boolean isCreationStep() { return isObjectUnderTestCreationStep ; } ;
    
    @Override 
    public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder)
    		throws 
    		Exception
    {
    	STEP_RT_info r = new STEP_RT_info(this,CUT,pool.getObjectUnderTest()) ;
        //assert (! Modifier.isAbstract(con.getDeclaringClass().getModifiers())) ;
        //assert (! Modifier.isAbstract(con.getModifiers())) ;
    	
    	STEP_RT_info[] argsResults = STEP.execMany(CUT,pool, params) ;
    	int N = argsResults.length ;
    	r.args = new Object[N] ;
    	// if one of the argument fails or throws an exception, then this MKVAL also fails:
    	for (STEP_RT_info i : argsResults) {
    	   if (i==null || i.exc != null) {
    		   return null ;
    	   }
    	}
        for (int i=0; i<N; i++) r.args[i] = argsResults[i].returnedObj ;
        
        if (precondPlaceHolder!=null) precondPlaceHolder.test(null, this, r.args);

        con.setAccessible(true); // force it to be accessible
        r.returnedObj = null ;
        try {
           r.returnedObj = con.newInstance(r.args) ;
           pool.put(cty,r.returnedObj) ;
           if (CUT.isInstance(r.returnedObj)) checkClassinv(CUT,pool,r.returnedObj,r)  ;
        } 
        catch (InstantiationException e) { throw e ; }
        catch (IllegalAccessException e) { throw e ; }
        catch (IllegalArgumentException e) { 
        	   // this is the case e.g. when the number of supplied parameters are incorrect
        	   throw e ; } 
        catch (InvocationTargetException e) { 
        	   // the invoked method has thrown an exception
        	   r.exc = e.getCause() ;
        	   assert r.exc != null ;
        }
          
        String check1 = oracle.check(null, Maybe.lift(r.returnedObj), new Maybe(r.exc)) ;
           
        if (check1 != null) {
        	r.exc = new OracleError("T3: violating an oracle. Info: " + check1) ;
        }     
        return r ;
    }

    @Override
    public void clearOracle() {
        oracle = Oracle.TT ;
    }

    /**
     * For serialization.
     */
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
       stream.writeObject(cty);
       stream.writeObject(con.getDeclaringClass().getName());
       //System.out.println(">> saving constructor: " + con.getDeclaringClass().getName() + "");
       Class[] paramTypes   = con.getParameterTypes();
       String[] paramTypes_ = new String[paramTypes.length];
       for (int i = 0; i < paramTypes.length; i++) {
            paramTypes_[i] = paramTypes[i].getName();
            //System.out.println("    > " + paramTypes[i].getName())    ;
       }
       stream.writeObject(paramTypes_);
       stream.writeObject(params);
       stream.writeObject(new Boolean(isObjectUnderTestCreationStep));
       stream.writeObject(oracle) ;
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream) 
    		throws 
    		IOException, 
    		ClassNotFoundException,
    		NoSuchMethodException, 
    		SecurityException
    {
    	cty= (JType) stream.readObject() ;
      	Class C = Class.forName((String) stream.readObject()) ;
        String[] paramTypes_ = (String[]) stream.readObject();
        Class[] paramTypes   = new Class[paramTypes_.length];
        for (int i = 0; i < paramTypes_.length; i++) {
            paramTypes[i] = Reflection.getClassFromName(paramTypes_[i]);
        }
        con = C.getDeclaredConstructor(paramTypes) ;
        params = (STEP[]) stream.readObject();
        isObjectUnderTestCreationStep = (Boolean) stream.readObject() ;
        oracle = (Oracle) stream.readObject() ;
    }

    
    public String toString() {
        String result = "CON " + con.getName() + "(";
        for (int i=0; i<params.length; i++) {
            result += params[i] ;
            if (i<params.length-1) result += ",\n" ;
        }
        result += ")" ; 
        return StringFormater.indentButFirst(result,3) ;
    }

}
