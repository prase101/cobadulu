/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import Sequenic.T3.Info.ObjectCoverage;
import Sequenic.T3.Oracle.Oracle;
import Sequenic.T3.Pool;

import java.util.* ;
import java.util.function.Predicate;
import java.io.*; 


public class SEQ implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public LinkedList<STEP> steps ;

    public SEQ() {
        steps = new LinkedList<STEP>() ;
    }

    public boolean all(Predicate<STEP> p) {
    	for (STEP step : steps) if (! p.test(step)) return false ;
    	return true ;
    }

    public boolean some(Predicate<STEP> p) {
    	for (STEP step : steps) if (p.test(step)) return true ;
    	return false ;
    }

    
    /**
     * Execute this sequence. No printing.
     */
    public SEQ_RT_info exec(Class CUT,Pool pool) throws Exception {
        return exec(CUT,pool,steps.size(),null) ;
    }

    /**
     * Execute this sequence. The steps starting from the specified index will also
     * be printed to the given output-stream.
     */
    public SEQ_RT_info exec(Class CUT, Pool pool, int startingFrom, OutputStream out) throws Exception {
        return exec(CUT,pool,null,startingFrom,4,out) ;
    }
    
    public SEQ_RT_info exec(Class CUT, 
    		Pool pool, 
    		ObjectCoverage ocDataCollector,
    		int startingFrom, 
    		OutputStream out) throws Exception {
        return exec(CUT,pool,ocDataCollector,startingFrom,4,out) ;
    }

  

    /**
     * Execute this sequence. The steps starting from the specified index will also
     * be printed to the given output-stream.
     */
	public SEQ_RT_info exec(Class CUT, 
			Pool pool, 
			ObjectCoverage ocDataCollector,
			int startingFrom, 
			int showDepth, 
			OutputStream out) throws Exception {
		//assert steps != null ;
		pool.reset() ;
		
		int N = steps.size() ;
        // System.out.println("** seq-length = " + N) ;
		assert N>=0 ;
		SEQ_RT_info info = new SEQ_RT_info(this) ;
        info.executionCounter = 0 ;

		for (STEP currentSTEP : steps) {
			
			if (currentSTEP instanceof INSTRUMENT) {
			   if (ocDataCollector==null) continue ;
			   Object o = pool.getObjectUnderTest() ;
			   if (o != null) {
				   ocDataCollector.addCoveredPairs(o);
			   }
			   info.executionCounter++ ;
			   continue ;
			}

			STEP_RT_info stepInfo = currentSTEP.exec(CUT,pool,null) ;

            if (stepInfo == null) {
                // the step fails, --> terminate the sequence
                return info ;
            }

            info.lastInfo = stepInfo ;

            if (info.executionCounter==0 && currentSTEP.isCreationStep()) {
            	// System.out.println(">> creation-step = " + currentSTEP) ;
                // if the first step is a creation of object-under-test :
                Object objectUnderTest =  info.lastInfo.returnedObj ;
                if (objectUnderTest == null) {
                	// the creation step does not yield an object! e.g. due exception or
                	// if it is a factory, it could just return a null. Then we should
                	// stop the execution of the sequence here:
                	printStep(info,showDepth,out) ;
                	return info ;	
                }
                pool.markAsObjectUnderTest(objectUnderTest) ;
                info.lastInfo.objectUnderTest =  objectUnderTest ;
            }

            if (info.executionCounter >= startingFrom)  {
            	printStep(info,showDepth,out) ;
                //System.out.println("** printing step no. " + info.executionCounter) ;
            }
			if (info.isFail()){
                //System.out.println("** fails at step no. " + info.executionCounter) ;
				return info ;
			}
            info.executionCounter++ ;
		}
		return info ;
	}
	
	/**
	 * Calculate the effective length of the sequence. This is the literal length of
	 * the sequence, minus all the INSTRUMENT steps.
	 */
	public int length() {
		if (steps==null) return 0 ;
		int k = 0 ;
		for (STEP step : steps) {
			if (step instanceof INSTRUMENT) continue ;
			k++ ;
		}
		return k ;
	}
	
	private void printStep(SEQ_RT_info info, int showDepth, OutputStream out) throws Exception {
		if (out == null) return ;
		out.write(("\n** [step " + info.executionCounter + "] ") .getBytes());
        info.lastInfo.print(showDepth,out);
	}

    /**
     * For clearing out all oracles attached to each step in the sequence.
     */
    public void clearOracle() {
        for (STEP s : steps) s.clearOracle();
    }


}
