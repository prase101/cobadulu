/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.OracleError;
import Sequenic.T3.Oracle.Oracle;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.Pool;

import java.lang.reflect.*;
import java.io.*;
import java.util.*;



/**
 * This represents a STEP where we update a field of a target object.
 */
public class UPDATE_FIELD extends STEP implements Serializable {
	
	private static final long serialVersionUID = 1L;

    public Field field = null ;
    public STEP  val = null ;

    public Oracle oracle = Oracle.TT ;

    /**
     * @param f The field to update.
     * @param v A STEP producing a new value for the field f.
     */
    public UPDATE_FIELD(Field f, STEP v) {
        field = f;
        val = v;
    }

    public UPDATE_FIELD() { }
    
    public String getName() {
        return field.getName();
    }
    
    @Override
    public boolean isCreationStep() { return false ; } ;
    
    @Override
    public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder) throws Exception {
    	
    	STEP_RT_info r = new STEP_RT_info(this,CUT,pool.getObjectUnderTest()) ;
    	
    	STEP_RT_info valInfo = val.exec(CUT,pool,null) ;
    	if (valInfo==null || valInfo.exc != null) {
    		return null ;
    	}
    	r.args = new Object[1] ;
        r.args[0] = valInfo.returnedObj ;
    	r.returnedObj = null ;

    	if (precondPlaceHolder!=null) precondPlaceHolder.test(r.objectUnderTest, this, r.args);

    	field.setAccessible(true); // for it to be accessible
    	field.set(pool.getObjectUnderTest(),valInfo.returnedObj) ;
       	
    	checkClassinv(CUT,pool,pool.getObjectUnderTest(),r)  ;
        
        String check1 = oracle.check(Maybe.lift(r.objectUnderTest), null, new Maybe(r.exc)) ;
        if (check1 != null) {
        	r.exc = new OracleError("T3: violating an oracle. Info: " + check1) ;
        }     
        return r ;
    }

    @Override
    public void clearOracle() {
        oracle = Oracle.TT ;
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream)
            throws 
            IOException, 
            ClassNotFoundException,
            NoSuchFieldException
    {
      Class C = Class.forName((String) stream.readObject()) ;
      String fieldName = (String) stream.readObject();
      field = C.getDeclaredField(fieldName) ;
      val = (STEP) stream.readObject();
      oracle = (Oracle) stream.readObject() ;
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
       stream.writeObject(field.getDeclaringClass().getName());
       stream.writeObject(field.getName());
       stream.writeObject(val);
       stream.writeObject(oracle) ;
    }
    
    @Override
    public String toString() {
        return "UPDATE " + field.getName() + " with " + val ;
    }
}
