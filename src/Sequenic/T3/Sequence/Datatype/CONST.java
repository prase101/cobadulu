/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T3.Sequence.Datatype;

import java.io.*;

import Sequenic.T3.JavaType.JTfun;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.Pool;
import Sequenic.T3.utils.Cloner;



/**
 * This represents a STEP intended to construct a value from a constant/
 * literal value. The object used as 'constant' is remembered in 
 * the STEP.
 */
public class CONST extends STEP implements Serializable {

	private static final long serialVersionUID = 1L;
	
    public Object val = null ;
    
    /**
     * When val is null, then we can't ask via reflection what it
     * class was; so we'll also keep track its concrete type as well.
     */
    public JType cty = null ;

    /**
     * @param v The object used as the constant supplied by this step.
     */
    public CONST(Serializable v, JType concreteType) {
        val = v;
        cty = concreteType ;
    }
    
    public CONST() { }

    @Override
    public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder)
    		throws 
    		IOException, ClassNotFoundException 
    {
    	STEP_RT_info r = new STEP_RT_info(this,CUT,pool.getObjectUnderTest()) ;
    	
    	if (precondPlaceHolder!=null) precondPlaceHolder.test(null, this, new Object[0]);
    			
    	r.returnedObj = Cloner.clone(this.val) ;
    	return r ;
    }
    
    public boolean isNull() {
        return val == null;
    }

    @Override
    public boolean isCreationStep() { return false ; } ;
    
    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException
		{
			cty= (JType) stream.readObject() ;
			val = stream.readObject() ;
		}
    
    
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(cty);
        stream.writeObject(val) ;
    }
    
    
    public String toString() {
        // if(val instanceof String) ... TODO filter unprintable chars and show real value?
        if (val instanceof String) return ("\"" + val + "\"") ;
    	return ("" + val) ;
    }
    
    
    public static void main(String[] args) throws Exception {
    	CONST C = new CONST(null, new JTfun(String.class)) ;
    	System.out.println("C type " + C.cty + ", val = " + C) ;
    	ByteArrayOutputStream s = new ByteArrayOutputStream() ;
    	ObjectOutputStream s_ = new ObjectOutputStream(s) ;
    	C.writeObject(s_) ;
    	s_.close() ;
    	ByteArrayInputStream t = new ByteArrayInputStream(s.toByteArray()) ;
    	ObjectInputStream t_ = new ObjectInputStream(t) ;
    	CONST D = new CONST() ;
    	D.readObject(t_) ;
    	System.out.println("D type " + D.cty + ", val = " + D) ;
    	
    }

}