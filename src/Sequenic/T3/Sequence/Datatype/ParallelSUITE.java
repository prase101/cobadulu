/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T3.Sequence.Datatype;


import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Pool;
import Sequenic.T3.Violation;
import Sequenic.T3.utils.Maybe;

import java.io.OutputStream;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class representing a test-suite, which is a set of test-sequences. This variant
 * supports parallel suite-execution.
 */
public class ParallelSUITE extends SUITE {

    private static final long serialVersionUID = 1L;

    public ParallelSUITE(String CUTname) {
        super(CUTname) ;
    }

    /**
     * To convert a suite to a parallel suite.
     */
    public ParallelSUITE(SUITE S) {
        CUTname = S.CUTname ;
        timeStamp = S.timeStamp ;
        suitename = S.suitename ;
        suite = S.suite ;
    }

    /**
     * To convert a parallel suite to a plain suite.
     */
    public SUITE covertToSUITE() {
        SUITE S = new SUITE() ;
        S.CUTname = CUTname ;
        S.timeStamp = timeStamp ;
        S.suitename = suitename ;
        S.suite = suite ;
        return S ;
    }

    /**
     * Parallel version of exec. However, no execution will be showed (else the show will be
     * chaotic due to interleaved printing to the output stream). Also, unlike the sequential
     * version, it is not possible to stop the suite execution after a sequence triggers a
     * violation. All sequences will be executed.
     *
     * If the rethrowViolation flag is set to true, at the end of the suite execution, the
     * first violation, if any, will be rethrown.
     */
    public void parallelExec(boolean rethrowViolation,OutputStream out) throws Exception
    {
        long runtime = System.currentTimeMillis() ;
        Class CUT = Class.forName(CUTname) ;
        Function<SEQ,Maybe<SEQ_RT_info>> exec1 = seq -> {
            try {
                SEQ_RT_info r = seq.exec(CUT,new Pool()) ;
                return new Maybe(r) ;
            }
            catch(Exception e) {
                return null ;
            }
        }  ;
        List<Maybe<SEQ_RT_info>> results = suite.parallelStream().map(exec1).collect(Collectors.toList()) ;
        int numOfViolatingSequences = 0 ;
        int numOfAsmBreachingSequences = 0 ;
        int numOfFailingSequences = 0 ;
        Throwable violation = null ;
        for (Maybe<SEQ_RT_info> r : results) {
            if (r==null || r.val.lastInfo==null) {
                numOfFailingSequences++  ; continue ;
            }
            if (r.val.lastInfo.inputWasIncorrect()) numOfAsmBreachingSequences++ ;
            if (r.val.lastInfo.raisesViolation()) numOfViolatingSequences++ ;
            if (violation == null && r.val.lastInfo.exc != null) violation = r.val.lastInfo.exc ;
        }
        runtime = System.currentTimeMillis() - runtime ;
        if (out!=null) {
            // printing statistics:
            out.write(("\n--------------------------").getBytes()) ;
            int SS = suite.size() ;
            out.write(("\nSuite size : " + SS).getBytes()) ;
            int totLength = 0 ;
            for (SEQ sigma : suite) {
                if (sigma != null) totLength += sigma.steps.size() ;
            }
            if (SS>0)
                out.write(("\nAverage length : " + (((float) totLength)/(float) SS)).getBytes()) ;
            out.write(("\nExecuted   : all").getBytes()) ;
            out.write(("\nViolating  : " + numOfViolatingSequences).getBytes()) ;
            out.write(("\nInvalid    : " + numOfAsmBreachingSequences).getBytes()) ;
            out.write(("\nFailing    : " + numOfFailingSequences).getBytes()) ;
            out.write(("\nRuntime    : " + runtime + " ms").getBytes()) ;
            Logger.getLogger(CONSTANTS.T3loggerName).info("** CUT: "
                    + CUTname
                    + " ,N=" + SS
                    + " ,vio=" + numOfViolatingSequences
                    + " ,asm-vio=" +  numOfAsmBreachingSequences
                    + " ,fail=" +  numOfFailingSequences
            );
        }
        if (rethrowViolation && violation != null)  {
            Logger.getLogger(CONSTANTS.T3loggerName).info("** CUT, suite rethrows an exception.") ;
            throw new Violation(violation) ;
        }
    }

    /**
     * Enhance the non-failing sequences in the suite with injected oracles.
     */
    public void injectOracles(Class CUT, boolean isSingleCore,  int injectionSuffixLength) {
        if (isSingleCore) {
            super.injectOracles(CUT,isSingleCore,injectionSuffixLength);
            return ;
        }
        Function<SEQ,Boolean> f = seq -> {
            boolean injected = injectOracles(CUT,seq,injectionSuffixLength) ;
            return injected ;
            //return seq ;
        } ;
        // suite = suite . parallelStream() . map(f) . collect(Collectors.toList()) ;
        List<Boolean> r = suite . parallelStream() . map(f) . collect(Collectors.toList()) ;
        int k = 0 ;
        for (Boolean b : r) if (b) k++ ;
        Logger.getLogger(CONSTANTS.T3loggerName).info("** Injected " + k + " oracles to a suite of " + CUTname + " (parallel) ...") ;
    }

}
