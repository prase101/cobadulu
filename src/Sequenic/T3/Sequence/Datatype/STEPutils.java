package Sequenic.T3.Sequence.Datatype;

import java.util.* ;

public class STEPutils {

	static public String getMethodOrConstrName(STEP s) {
  	  if (s instanceof METHOD) {
  		  METHOD s_ = (METHOD) s ;
  		  return s_.method.getName() ;
  	  }
  	  if (s instanceof CONSTRUCTOR) {
  		  CONSTRUCTOR c = (CONSTRUCTOR) s ;
  		  return c.con.getName() ;
  	  }
  	  return null ;
    }
	
	static public Set<Integer> getRefs(STEP s) {
		Set<Integer> result = new HashSet<Integer>() ;
		getRefs(result,s) ;
		return result ;
	}
    
    static private void getRefs(Set<Integer> result, STEP s) {
  	     if (s instanceof CONST) return ;
  	     if (s instanceof INSTRUMENT) return ;
  	     if (s instanceof REF) {
  	    	 REF r = (REF) s ;
  	    	 result.add(((REF) s).index) ;
  	    	 return ;
  	     }
  	     if (s instanceof CONSTRUCT_COLLECTION) {
  	    	CONSTRUCT_COLLECTION c = (CONSTRUCT_COLLECTION) s ;
  	    	for (STEP z : c.elements) getRefs(result,z) ;
  	    	if (c.keys != null) for (STEP z : c.keys) getRefs(result,z) ;
  	    	return ;
  	     }
  	     if (s instanceof CONSTRUCTOR) {
  	    	CONSTRUCTOR c = (CONSTRUCTOR) s ;
  	    	for (STEP z : c.params) getRefs(result,z) ;
  	    	return ;
  	     }
  	     if (s instanceof METHOD){
  	    	 METHOD m = (METHOD) s ;
  	    	 for (STEP z : m.params) getRefs(result,z) ;
  	    	 return ;
  	     }
  	     if (s instanceof UPDATE_FIELD) {
  	    	UPDATE_FIELD f = (UPDATE_FIELD) s ;
  	    	getRefs(result,f.val) ;
  	    	return ;
  	     }
    }
    
    /**
     * Return the max REF index of all steps in the prefix of a given sequence.
     * The prefix is up to (but not including) step-k.
     */
    static public Integer getMaxRefOnPrefix(SEQ seq, int k) {
    	int k_ = Math.min(k, seq.length()) ;
    	Set<Integer> refs = new HashSet<Integer>() ;
    	for(int i=0; i<k_ ; i++) {
    		getRefs(refs,seq.steps.get(i)) ;
    	}
        if (refs.size() == 0) return null ;
        int max = 0 ;
        for (Integer i : refs) max = Math.max(max, i) ;
        return max ;
    }
    
    /**
     * Return the minimum REF index of all steps in the suffix of a given sequence.
     * The suffix starts from k.
     */
    static public Integer getMinRefOnSuffix(SEQ seq, int k) {
    	int k_ = Math.max(k,0) ;
    	Set<Integer> refs = new HashSet<Integer>() ;
    	int N = seq.length() ;
    	for(int i=k_ ; i<N ; i++) {
    		getRefs(refs,seq.steps.get(i)) ;
    	}
        if (refs.size() == 0) return null ;
        int min = Integer.MAX_VALUE ;
        for (Integer i : refs) min = Math.min(min, i) ;
        return min ;
    }
}
