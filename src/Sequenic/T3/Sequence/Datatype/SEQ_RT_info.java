/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import java.util.HashMap;
import java.util.Map;

import Sequenic.T3.JavaType.JType;

public class SEQ_RT_info {
	
	/**
	 * The test-sequence to which this info belongs to.
	 */
	public SEQ seq ;

    /**
     * The  index of the next step to execute.
     */
	public int executionCounter = 0 ;
	
	/**
	 * Type substitution to be used for the entire sequence.
	 */
	public Map<String,JType> tySubsts = null ;
	
	/**
	 * This contains the info of the last step executed. If the sequence does not
	 * fail, the step will be literally the sequence's last step. Else it would be
	 * the failing step.
	 */
	public STEP_RT_info lastInfo ;

	public SEQ_RT_info(SEQ s) { 
		seq = s ;
		tySubsts = new HashMap<String,JType>() ;
	}

    /**
     * True if the execution passes a step that produces an exception.
     */
	public boolean isFail() {
		return lastInfo != null && lastInfo.exc != null ;
	}

    /**
     * check if the execution was maximal (all steps in the sequence were executed).
     */
    public boolean executionWasMaximal() { return (executionCounter >= seq.steps.size()) ; }
    

}
