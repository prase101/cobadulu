/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Reflection;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This scan all the classes residing under a given directory. If a class is not loaded yet,
 * it will be loaded.
 */
public class ClassesScanner {

    protected List<Class> scanned = new LinkedList<Class>() ;

    /**
     * A set of paths to .class directories that are to be scanned.
     * For now we require that this path must be part of the executing JVM's classpath.
     */
    protected String[] dirsToBeScanned ;

    public ClassesScanner(String[] pathsToClassesDir) {
        dirsToBeScanned =  pathsToClassesDir ;
    }

    synchronized public List<Class> getScanned() { return scanned ; }

    synchronized public void scan() {
        for (String dirToScan : dirsToBeScanned) {
           Logger.getLogger("T3").log(Level.INFO,"** Scanning classes in " + dirToScan);
           try {
              Path dir = FileSystems.getDefault().getPath(dirToScan) ;
              scanWorker("", dir, 3) ;
           }
           catch(IOException e) {
               Logger.getLogger("T3").log(Level.WARNING,"** Fail to scanning classes: " + e.toString());
           }
           Logger.getLogger("T3").log(Level.INFO,"** " + scanned.size() + " classes scanned.");
        }
    }

    void scanWorker(String packageName, Path dir, int indent) throws IOException {
        if (!Files.isDirectory(dir)) return ;
        DirectoryStream<Path> stream = Files.newDirectoryStream(dir) ;
        for (Path entry: stream) {
          //System.out.println("++ "+ entry.getFileName() ) ;
          String entry_ =  entry.getFileName().toString() ;
          if (entry_.startsWith(".")) continue;
          if (entry_.endsWith(".class"))     {
              registerClass(packageName,entry_) ;
              //System.out.println("" + indent(indent) + ">> " + entry);
          }
          else {
              String newPckgName = "" +  packageName ;
              if (!newPckgName.equals("")) newPckgName += "." ;
              newPckgName +=  entry_ ;
              scanWorker(newPckgName, entry, indent + 3) ;
          }
        }
    }

    private String indent(int k) {
        String s = "" ;
        for (int i=0; i<k; i++) s+= " " ;
        return s ;
    }

    void registerClass(String pckName, String classFileName) {
        int k = classFileName.lastIndexOf('.') ;
        String fullClassName = "" + pckName ;
        if (!fullClassName.equals("")) fullClassName += "."  ;
        fullClassName += classFileName.substring(0,k) ;
        try {
           //System.out.println("** registering " + fullClassName) ;
           Class C = Class.forName(fullClassName) ;
           scanned.add(C) ;
        }
        catch (Throwable e) {
            Logger.getLogger("T3").log(Level.WARNING,"Fail to load the class " + fullClassName);
        }
    }


    /**
     * Used to maintain a single global scanner, for efficiency.  This way we will only do
     * the scanning once.
     */
    //static ClassesScanner globalScanner = null ;

    /**
     * Create a single global class-scanner over the specified directory. The directory
     * will then be scanned. If such a global scanner is already created, we will not
     * scan again. The existing scanner is then returned.
     */
    /*
    static public synchronized ClassesScanner createGlobalScanner(String[] pathsToClassesDir) {
        if (globalScanner != null) return  globalScanner ;
        globalScanner = new ClassesScanner(pathsToClassesDir) ;
        globalScanner.scan();
        return globalScanner ;
    }
    */
    
    public static void main(String[] args){
       String[] dirs = {"D:/workshop/T3workspace/T3_v0/out/production/T3_v0"} ;
       new ClassesScanner(dirs).scan();
       //new ClassesScanner().scan("D:/workshop/t2framework/repos/T3/libs/jars/reflections/gson-1.4.jar") ;

    }


}
