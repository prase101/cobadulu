/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import java.util.*;
import java.util.Random;

public class T3Random {

    static Random t3rnd ;

    public static Random getRnd() {
        if (t3rnd==null) t3rnd = new Random() ;
        return t3rnd ;
    }

    /**
     * Produce a list of N random integers, each in the range [0..upperbound)
     * The method will try to make each integer in the resulting list to be
     * unique; but this is not guaranteed.
     */
    public static List<Integer> getRandomIndices(int upperbound, int N) {
        List<Integer> R = new LinkedList<Integer>() ;
        for (int n=0; n<N; n++) {
            int x = 0 ;
            for (int k=0; k<10; k++) {  // try at most 10 times
               x = getRnd().nextInt(upperbound) ;
               if (!R.contains(x)) {
                   R.add(x) ;
                   break ;
               }
            } ;
            // have tried 100 times... add x anyway:
            R.add(x) ;
        }
        return R ;
    }
}
