/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Step;

import Sequenic.T3.Generator.*;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.TestingScope;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.utils.Unit;

import java.lang.reflect.Field;
import java.util.*;

public class UPDATE_FIELDMG {

    public Random rnd = T3Random.getRnd() ;
    public Generator<PARAM,STEP> valueGenerator ;
    public TestingScope testingscope ;

    public UPDATE_FIELDMG(TestingScope ts, Generator<PARAM, STEP> valueGenerator) {
        this.valueGenerator = valueGenerator ;
        testingscope = ts ;
    }

    public Generator<SEQ_RT_info,STEP> random() {
        return info -> {
            if (testingscope.fields.isEmpty()) return null ;
            // randomly select a field :
            Field F = testingscope.fields.get(rnd.nextInt(testingscope.fields.size()))  ;

            // Solve the type:
            // System.out.println(">> orig type = " + F.getGenericType()) ;
            JType ty = Solver.solve(testingscope.impsMap, info.tySubsts, JTypeUtils.convert(F.getGenericType())) ;
            if (ty==null) return null ;
            //System.out.println("** about to create a meta FIELD UPDATE, ty = " + ty) ;
            // construct the value for the field; assuming the value-generator never fails:
            Maybe<STEP> v = valueGenerator.generate(new PARAM(F.getName(),ty)) ;
            //System.out.println("** created FIELD UPDATE = " + v) ;
            if (v == null) {
                //System.out.println("** fail to create a FIELD UPDATE " + F.getName() + ", " + ty) ;
                return null ;
            }
            //System.out.println("** created FIELD UPDATE ====> " + v) ;
            return  new Maybe(new UPDATE_FIELD(F,v.val))  ;
        }   ;
    }

}
