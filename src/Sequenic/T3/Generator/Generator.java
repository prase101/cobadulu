/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator;

import Sequenic.T3.T3Random;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.utils.*  ;

import java.util.function.Predicate;

@FunctionalInterface
public interface Generator<Requirement,T> {

    /**
     * Generate an object an instance of T.
     */
    public Maybe<T> generate(Requirement R)  ;


    default public Generator<Requirement,T> WithPreRequirement (Predicate<Requirement> p) {
        return R -> {
            if (!p.test(R)) return null ;
            return generate(R) ;
        } ;
    }


    default public Generator<Requirement,T> WithPreCondition (Predicate<Requirement> p) {
        return R -> {
            if (! p.test(R)) throw new AssertionError() ;
            return generate(R) ;
        } ;
    }
    
    default public Generator<Requirement,T> If (Predicate<Requirement> p) {
        return WithPreRequirement(p) ;
    }


    default public Generator<Requirement,T> WithChance (double chance) {
        //assert chance <= 1.0 ;  --> somehow cause the compiler to think the class has illegal field modifier
        //assert 0 < chance ;
        return R -> {
        	if (chance <= 0.0) return null ;
            if (T3Random.getRnd().nextDouble() >= chance) return null ;
            return generate(R) ;
        } ;
    }
    
    /*
    default public Generator<Requirement,T> WithChance (double chance) {
        if (chance > 0) throw new AssertionError();
        return R -> generate(R) ;
    }
    */

}
