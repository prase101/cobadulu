/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import java.lang.reflect.Method;

import Sequenic.T3.Pool;
import Sequenic.T3.TestingScope;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.*;



/**
 * Just a parent class for all sequence-generators-classes. They all share the 
 * members specified below.
 *
 * In general all sequence generators will provide these generators:
 *
 * no_asmviol: generates a sequence that does not throw an assumption-like exception;
 * it may throw a post-condition like violation.
 * 
 * grey_seq: generates a sequence that may throw an exception. So, this may also 
 * produce negative tests (those that throw pre-condition-like exception).
 * 
 * no_exc_seq: generates a sequence that does not throw any exception. E.g. this is needed to
 * generate prefixes.
 * 
 */
public class AbstractSeqGenerator {

    public TestingScope scope ;
    public Pool pool ;
    public Generator<PARAM,STEP> valueGenerator ;
    public int maxNumberOfStepRetry = 30 ;
    
    /**
     * A constructor to set up some common state variables.
     */
    public AbstractSeqGenerator(TestingScope scope, Pool pool, int maxNumberOfStepRetry, Generator<PARAM,STEP> valueGenerator) {
        this.scope = scope ;
        this.pool = pool ;
        this.maxNumberOfStepRetry = maxNumberOfStepRetry ;
        this.valueGenerator = valueGenerator ;
    }

    //public void setMaxNumberOfStepRetry(int N) { maxNumberOfStepRetry = N ; }
    
    
    /**
     * To insert an INSTRUMENT step.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> instrument() {
    	return info -> {
    		info.seq.steps.addLast(new INSTRUMENT()) ;
    		return new Maybe(info) ;
    	} ;
    }
}
