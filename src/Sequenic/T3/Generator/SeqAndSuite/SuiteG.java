package Sequenic.T3.Generator.SeqAndSuite;

import java.lang.reflect.Method;
import java.util.LinkedList;

import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Pool;
import Sequenic.T3.TestingScope;
import Sequenic.T3.Info.* ;
import Sequenic.T3.Examples.Abstractx.PaperVehicle;
import Sequenic.T3.Examples.Abstractx.Vehicle;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.Value.ValueMG;
import Sequenic.T3.Info.FunctionalCoverage;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.Maybe;


public class SuiteG {
	
    public TestingScope scope ;
    public int maxNumberOfSeqRetry  = 5  ;
    
    public SuiteG(TestingScope scope) {
    	this.scope = scope ;
    }
    
    
    /**
     * Pass the pool that is used by sequence generator.
     */
    public SUITE run(Pool pool, Generator<SEQ_RT_info,SEQ_RT_info> seqGenerator, int maxSamplesToCollect) {
        SUITE S = new SUITE(scope.CUT.getName()) ;
        int n = 0 ;
        //System.out.println("##>> SuiteG: maxSamplesToCollect = " + maxSamplesToCollect) ;
        while (n<maxSamplesToCollect) {
            int i = 0 ;
            while(i < maxNumberOfSeqRetry) {
                pool.reset();
                Maybe<SEQ_RT_info> r = seqGenerator.generate(new SEQ_RT_info(new SEQ()))  ;
                //if (r!=null) System.out.println("##>> SuiteG: r = " + r.val.seq.steps.size()) ;
                if (r != null && ! r.val.seq.steps.isEmpty()) {
                    S.suite.add(r.val.seq);
                    //System.out.println("##>> SuiteG: adding a sequence.") ;
                    break ;
                }
                i++ ;
            }
            // always count the top iteration :
            n++ ;
        }
        return S ;
    }
    
    public static void main(String[] args) throws Exception {
        test1() ;
        //test2() ;
    	//test3() ;
    }
    
    // testing against the class Person; ADT
    private static void test1() throws Exception {
       	Class CUT = Person.class ;
        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        scope.configureForADTtesting();

        ValueMG valuegens = new ValueMG(3,2,impsmap) ;     
        T3SeqG sgens = new T3SeqG(pool,scope,30,valuegens.gen1closed(pool)) ;

        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;

        System.out.println(scope.toString());

        Method m1 = scope.methods("friend").get(0) ;
        Method m2 = scope.methods("unfriend").get(0) ;
        
        System.out.println(">>> m1 = " + m1.getName());
        System.out.println(">>> m2 = " + m2.getName());
        
        SuiteG suitegen = new SuiteG(scope) ;
        SUITE S = suitegen.run(pool,sgens.grey_adt(3,3,true,0.2,m1,m2), 50) ;
        
        SUITE_RT_info suiteinfo = S.exec(new Pool(), new ObjectCoverage(CUT), 0, 0, false, true, false, System.out) ;

        suiteinfo.printReport(System.out);
        
        FunctionalCoverage fc = new FunctionalCoverage(scope,System.out) ;
        fc.calculate(S);
        fc.printReport(1);   
    }
    
    private static void test2() throws Exception {
       	Class CUT = Vehicle.class ;
        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        scope.configureForADTtesting();

        ValueMG valuegens = new ValueMG(3,2,impsmap) ;     
        T3SeqG sgens = new T3SeqG(pool,scope,30,valuegens.gen1closed(pool)) ;

        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;

        System.out.println(scope.toString());

        Method m1 = scope.methods("accelerate").get(0) ;
        Method m2 = scope.methods("move").get(0) ;
        
        System.out.println(">>> m1 = " + m1.getName());
        System.out.println(">>> m2 = " + m2.getName());
        
        SuiteG suitegen = new SuiteG(scope) ;
        SUITE S = suitegen.run(pool,sgens.grey_adt(10,3,true,0.2,m1,m2), 5) ;
        
        SUITE_RT_info suiteinfo = S.exec(new Pool(), new ObjectCoverage(CUT), 0, 0, false, true, false, System.out) ;

        suiteinfo.printReport(System.out);
        
        FunctionalCoverage fc = new FunctionalCoverage(scope,System.out) ;
        fc.calculate(S);
        fc.printReport(1);   
    }
    
    
    private static void test3() throws Exception {
       	Class CUT = LinkedList.class ;
        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        scope.configureForADTtesting();

        ValueMG valuegens = new ValueMG(3,2,impsmap) ;     
        T3SeqG sgens = new T3SeqG(pool,scope,30,valuegens.gen1closed(pool)) ;

        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;

        System.out.println(scope.toString());

        Method m1 = scope.methods("get").get(0) ;
        Method m2 = scope.methods("remove").get(0) ;
        
        System.out.println(">>> m1 = " + m1.getName());
        System.out.println(">>> m2 = " + m2.getName());
        
        SuiteG suitegen = new SuiteG(scope) ;
        SUITE S = suitegen.run(pool,sgens.grey_adt(10,3,true,0.2,m1,m2), 5) ;
        
        SUITE_RT_info suiteinfo = S.exec(new Pool(), new ObjectCoverage(CUT), 0, 0, false, true, false, System.out) ;

        suiteinfo.printReport(System.out);
        
        FunctionalCoverage fc = new FunctionalCoverage(scope,System.out) ;
        fc.calculate(S);
        fc.printReport(1);   
    }

}
