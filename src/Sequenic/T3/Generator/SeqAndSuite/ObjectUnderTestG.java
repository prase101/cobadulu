/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import static Sequenic.T3.Generator.GenCombinators.Sequence;

import java.util.function.Predicate;
import java.lang.reflect.*;

import Sequenic.T3.Examples.Cycles.Cycle;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Generator.Value.ValueMG;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Pool;
import Sequenic.T3.TestingScope;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.* ;

/**
 * Providing generators to create a sequence containing a single step that creates 
 * the object under test.
 */
public class ObjectUnderTestG extends AbstractSeqGenerator {

	ObjectUnderTestMG objectUnderTestMetaGen ;
	
    public ObjectUnderTestG(TestingScope scope,
                          Pool pool,
                          int maxNumberOfStepRetry,
                          Generator<PARAM,STEP> valueGenerator
                          )
    {
        super(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
        objectUnderTestMetaGen =  new ObjectUnderTestMG(this.scope,this.valueGenerator) ;
    }

    /**
     * A generic worker to construct the generator; customized by the predicate.
     */
    private Generator<SEQ_RT_info,SEQ_RT_info> create(Predicate<STEP_RT_info> requirement, String creatorMethodName, Class...paramtypes) {
    	return STEPexec.until(
    			requirement,
    			maxNumberOfStepRetry,
    			objectUnderTestMetaGen.createObjectUnderTest(creatorMethodName, paramtypes), 
    			scope.CUT, 
    			pool) ;
    }
    
    private Generator<SEQ_RT_info,SEQ_RT_info> create(Predicate<STEP_RT_info> requirement, Constructor co) {
    	return STEPexec.until(
    			requirement,
    			maxNumberOfStepRetry,
    			objectUnderTestMetaGen.useConstructor(co), 
    			scope.CUT, 
    			pool) ;
    }
    
    private Generator<SEQ_RT_info,SEQ_RT_info> create(Predicate<STEP_RT_info> requirement, Method cm) {
    	return STEPexec.until(
    			requirement,
    			maxNumberOfStepRetry,
    			objectUnderTestMetaGen.useCreationMethod(cm), 
    			scope.CUT, 
    			pool) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey(Constructor co) {
    	return create(r -> true, co) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey(Method cm) {
    	return create(r -> true, cm) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey(String creatorMethodName, Class...paramtypes) {
    	return create(r -> true, creatorMethodName, paramtypes) ;
    }
        
    public Generator<SEQ_RT_info,SEQ_RT_info> grey(Class...paramtypes) {
    	return grey(null,paramtypes) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(Constructor co) {
    	return create(r -> !r.inputWasIncorrect(),co) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(Method cm) {
    	return create(r -> !r.inputWasIncorrect(),cm) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(String creatorMethodName, Class...paramtypes) {
    	return create(r -> !r.inputWasIncorrect(), creatorMethodName, paramtypes) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(Class...paramtypes) {
    	return no_asmviol(null,paramtypes);
    }
    
    /**
     * Generate a non-null target object, and avoid throwing any exception.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> nonnull_and_no_exc(Constructor co) {
    	return create(r -> r.exc == null && r.returnedObj != null, co) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> nonnull_and_no_exc(Method cm) {
    	return create(r -> r.exc == null && r.returnedObj != null, cm) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> nonnull_and_no_exc(String creatorMethodName, Class...paramtypes) {
    	return create(r -> r.exc == null && r.returnedObj != null, creatorMethodName, paramtypes) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> nonnull_and_no_exc(Class...paramtypes) {
    	return nonnull_and_no_exc(null,paramtypes) ;
    }
    
    
    
    public static void main(String[] args) throws Exception {

        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,Person.class)  ;
        scope.configureForADTtesting();
        System.out.println(scope.toString());

        ValueMG valueg = new ValueMG(3,2,impsmap) ;
        
        Class CUT = Person.class ;
        ObjectUnderTestG outg = new ObjectUnderTestG(scope,pool,30,valueg.gen1closed(pool)) ;
        
        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;
        
        Maybe<SEQ_RT_info> info2 = outg.nonnull_and_no_exc().generate(info) ;

        //info2.val.seq.exec(Person.class,pool,0,System.out) ;
        //info2.val.seq.exec(BuildSortedList.class,pool,0,System.out) ;
        info2.val.seq.exec(Cycle.class,pool,0,System.out) ;

    }
    
}
