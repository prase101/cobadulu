/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.Maybe;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class StringMG {

    public Random rnd = T3Random.getRnd() ;
    public String[] samples ;

    public StringMG(String ... samples) {
        this.samples = samples ;
    }

    static public boolean isString(JType ty) {
        Class C = JTypeUtils.getTopClass(ty) ;
        if (C==null) return false ;
        return (C == String.class) ;
    }
    
    public static String[] names1() {
       String [] r = {"Spongebob", "Patrick", "Beach"} ;
       return r ;
    }

    public static String[] names2() {
        String[] r = {"Sponge Bob", "S B", "Bikini Bottom"}   ; return r ;
    }

    public static String[] numbers1() {
        String[] r = {"0", "12"} ; return r ;
    }

    public static String[] numbers2() {
        String[] r = {"01", "030"} ; return r ;
    }

    public static String[] numbers3() {
        String[] r = {"-1", "-12"}  ; return r ;
    }

    public static String[] nameswithNonNumerics1() {
      String[] r = {"Sponge@bob", "Sponge!bob", "Sponge.bob",
                    "Sponge?bob", "Sponge*bob", "Sponge-bob" }    ;
      return r ;
    }

    public static String[] quotes1() {
        String[] r = {"\"spongebob\"", "'spongebob'", "`spongebob`"} ;
        return r ;
    }

    public static String[] white() {
        String[] r = {"", "\n", "\t", "  "} ;
        return r ;
    }
    
    private static <T> void addTo(List<T> s, T[] a)  {
       for (T x : a) {
           //System.out.println("...adding " + x);
           s.add(x);
       }
    }

    public static String[] samples1() {
        LinkedList<String> samples = new LinkedList<String>() ;
        addTo(samples,names1()) ;
        addTo(samples,names2()) ;
        addTo(samples,numbers1()) ;
        addTo(samples,numbers2()) ;
        addTo(samples,numbers3()) ;
        addTo(samples,nameswithNonNumerics1()) ;
        addTo(samples,white()) ;
        String [] samples_ = new String[samples.size()] ;
        int i=0 ;
        for (String s : samples)  {
            samples_[i] = s ;
            i++ ;
        }
        return samples_ ;
    }

    /**
     * Generate strings from the stored samples.
     * Note that currently parameters names cannot be correctly queried yet in Java.
     * But we do pass these names, to be exploited. (E.g. when we have a parameter
     * of type string, called userId, or phoneNumber)
     */
    public Generator<PARAM,STEP> fromSamples() {
        return
                P -> {
                    Class C = JTypeUtils.getTopClass(P.ty) ;
                    if (C==null) return null ;
                    if (C == String.class) {
                       return new Maybe(new CONST(samples[rnd.nextInt(samples.length)], P.ty)) ;
                       //return new Maybe(new CONST("blabla", P.cty)) ;
                    }
                    else return null ;
                } ;
    }

}
