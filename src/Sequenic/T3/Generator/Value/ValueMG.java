/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.*;
import Sequenic.T3.Examples.Cycles.PersonCycle;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.* ;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.*;
import static Sequenic.T3.Generator.GenCombinators.* ;

/**
 * Provide some examples of value generators.
 */
public class ValueMG {

    public ImplementationMap ImpsMap ;
    protected BoolMG boolgens = new BoolMG() ;
    protected ByteMG bytegens = new ByteMG();
    protected ShortMG shortgens = new ShortMG();
    protected IntMG intgens = new IntMG();
    protected LongMG longgens = new LongMG();
    protected FloatMG floatgens = new FloatMG();
    protected DoubleMG doublegens = new DoubleMG();
    protected CharMG chargens = new CharMG();
    protected StringMG stringgens = new StringMG(StringMG.samples1()) ;
    protected EnumMG enumgens = new EnumMG();
    protected NullMG nullgens = new NullMG();   
    protected ObjectMG constructorgens ;
    protected CollectionLikeMG 	collectiongens ;
    
    
    public ValueMG(int maxLevelOfObjectsNesting,
                    int collectionSize,
                    ImplementationMap ImpsMap) {
        this.ImpsMap = ImpsMap ;
        constructorgens = new ObjectMG(maxLevelOfObjectsNesting,ImpsMap) ;
        collectiongens = new CollectionLikeMG(collectionSize,ImpsMap) ;
    }

    static boolean isPrimitive(PARAM P) {
        return  Reflection.isPrimitiveLike(JTypeUtils.getTopClass(P.ty)) ;
    }

    static boolean isIntLike(PARAM P) {
        Class C = JTypeUtils.getTopClass(P.ty) ;
        return  (C == Integer.class)  || (C == Integer.TYPE) ;
    }

    static boolean isCharLike(PARAM P) {
        Class C = JTypeUtils.getTopClass(P.ty) ;
        return  (C == Character.class)  || (C == Character.TYPE) ;
    }
    
    static boolean isConcreteType(PARAM P) {
        return  P.ty.isConcrete() ;
    }

    public Generator<PARAM,STEP> primitives1() {
        return  FirstOf(
                        boolgens.uniform(),
                        bytegens.uniform(-128, 127),
                        shortgens.uniform(-10,10),
                        FirstOf(
                        	  chargens.dashSpAtDot().WithChance(0.05),
                        	  chargens.alphaNum()
                        ).WithPreRequirement(P -> isCharLike(P)),
                        FirstOf(
                              intgens.uniform(-2,2).WithChance(0.5),
                              intgens.uniform(-1,13).WithChance(0.8),
                              intgens.uniform(-1,999)
                        ). WithPreRequirement(P -> isIntLike(P)),
                        longgens.uniform(-999,999) ,
                        floatgens.roundedUniform(-999,999),
                        doublegens.roundedUniform(-999,999)
                )
                . WithPreRequirement(P -> isPrimitive(P)) ;
    }

    /**
     * A value meta-generator, where the recursion is bound to itself, but is left open
     * to be bound to something else.
     */
    public Generator<PARAM,STEP> gen1open(Pool pool, FUN<Generator<PARAM,STEP>> recGenerator) {
    	REFMG refgens = new REFMG(pool) ;
    	Generator<PARAM,STEP> g =
    			FirstOf(
                        nullgens.constant().WithChance(0.05),
                        primitives1(),
                        enumgens.random(),
                        stringgens.fromSamples(),
                        refgens.objectUnderTest().WithChance(0.4),
                        refgens.random().WithChance(0.4),
                        refgens.objectUnderTestIfSubclass().WithChance(0.1),
                        collectiongens.collectionlike(recGenerator),
                        constructorgens.useConstructor(recGenerator),
                        constructorgens.useCreatorMethod(recGenerator),
                        // if all the above fails.. then generate null
                        nullgens.defaulting()
                )
                //. WithPreCondition(P -> {
                //	// System.out.println(">> " + P.name + " : " + P.cty + ", concrete=" + P.cty.isConcrete()) ;
                //    return P.ty.isConcrete() ; }
                //) 
                ;
    	recGenerator.fun = g ;
    	return g ;
    }
    
    /**
     * The version of gen1, recursive to itself, but the recursion is closed (it can no
     * longer be changed).
     */
    public Generator<PARAM,STEP> gen1closed(Pool pool) {
    	FUN<Generator<PARAM,STEP>> recGenerator = new FUN<Generator<PARAM,STEP>>() ;
    	return gen1open(pool,recGenerator) ;
    }


    public static void main(String[] args) {
        String[] dirsToScan = {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        ValueMG gen = new ValueMG(3,2,impsmap) ;
        PARAM P1 = new PARAM(null,new JTfun(Person.class)) ;
        System.out.println("" + gen.gen1closed(new Pool()).generate(P1).val);

        //impsmap = new ImplementationMap("D:/workshop/T3workspace/T3_v0/out/production/T3_v0") ;
        PARAM P2 = new PARAM(null,new JTfun(PersonCycle.class)) ;
        System.out.println("" + gen.gen1closed(new Pool()).generate(P2).val);
        
        PARAM P3 = new PARAM(null, new JTfun(Integer.TYPE)) ;
        System.out.println("" + gen.gen1closed(new Pool()).generate(P3).val);

        PARAM P4 = new PARAM(null, new JTfun(Integer.class)) ;
        System.out.println("" + gen.gen1closed(new Pool()).generate(P4).val);
        
        PARAM P5 = new PARAM(null, new JTfun(Long.TYPE)) ;
        System.out.println("" + gen.gen1closed(new Pool()).generate(P5).val);
        
        PARAM P6 = new PARAM(null, new JTfun(Long.class)) ;
        System.out.println("" + gen.gen1closed(new Pool()).generate(P6).val);

    }

}
