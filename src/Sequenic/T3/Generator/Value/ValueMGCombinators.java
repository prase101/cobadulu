package Sequenic.T3.Generator.Value;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import Sequenic.T3.T3Random;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Sequence.Datatype.CONST;
import Sequenic.T3.Sequence.Datatype.METHOD;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.utils.Unit;

public class ValueMGCombinators {
	
	
	public static Supplier<Integer> OneOf(int ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return (s[k]) ;
        } ;
    }
	
	// e.g. use it as gInteger(OneOf(2,2,3)) 
	public static Generator<PARAM,STEP> Integer(Supplier<Integer> g) {
        return P -> {
        	if (IntMG.isIntOrInteger(P.ty)) {
        	   Integer x = new Integer(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	
	public static Supplier<Boolean> OneOf(boolean ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Boolean(Supplier<Boolean> g) {
        return P -> {
        	if (BoolMG.isboolOrBool(P.ty)) {
        	   Boolean x = new Boolean(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<Byte> OneOf(byte ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Byte(Supplier<Byte> g) {
        return P -> {
        	if (ByteMG.isbyteOrByte(P.ty)) {
        	   Byte x = new Byte(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<Short> OneOf(short ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Short(Supplier<Short> g) {
        return P -> {
        	if (ShortMG.isshortOrShort(P.ty)) {
        	   Short x = new Short(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<Long> OneOf(long ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Long(Supplier<Long> g) {
        return P -> {
        	if (LongMG.islongOrLong(P.ty)) {
        	   Long x = new Long(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<Float> OneOf(float ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Float(Supplier<Float> g) {
        return P -> {
        	if (FloatMG.isfloatOrFloat(P.ty)) {
        	   Float x = new Float(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<Double> OneOf(double ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Double(Supplier<Double> g) {
        return P -> {
        	if (DoubleMG.isdoubleOrDouble(P.ty)) {
        	   Double x = new Double(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<String> OneOf(String ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> String(Supplier<String> g) {
        return P -> {
        	if (StringMG.isString(P.ty)) {
        	   String x = new String(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	public static Supplier<Character> OneOf(char ... s) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(s.length) ;
        	return s[k] ;
        } ;
    }
	
	public static Generator<PARAM,STEP> Char(Supplier<Character> g) {
        return P -> {
        	if (CharMG.ischarOrChar(P.ty)) {
        	   Character x = new Character(g.get()) ;
        	   return new Maybe(new CONST(x, P.ty)) ;
        	}
        	else return null ;
        } ;
    }
	
	/**
	 * Choose one of the given objects. These objects should be of primitive
	 * or primitive-like types (e.g. int, or Integer or String).
	 */
	public static Supplier<Serializable> MixedOneOf(Serializable ... values) {
        return () -> {
        	int k = T3Random.getRnd().nextInt(values.length) ;
        	return values[k] ;
        } ;
    }
	
	/**
	 * To generate a value, using the function g. This will not check the requirement
	 * imposed by the PARAM.
	 */
	public static Generator<PARAM,STEP> UnguardedValue(Supplier<Serializable> g) {
        return P -> {
        	Serializable value = g.get() ;
        	return new Maybe(new CONST(value, JTypeUtils.naiveClassdecl2JType(value.getClass()))) ;
        } ;
    }
	
	/**
	 * Just another name for UnguardedValue-generator.
	 */
	public static Generator<PARAM,STEP> Serializable(Supplier<Serializable> g) {
		return UnguardedValue(g) ;
	}
	
	public static Predicate<PARAM> hasParamName(String name) {
		return P -> P != null && P.name != null && P.name.equals(name) ;
	}
	
	/**
	 * A different way to write g.If(hasParamName(n))
	 */
	public static Generator<PARAM,STEP> Param(String paramName, Generator<PARAM,STEP> gen) {
		return gen.If(hasParamName(paramName)) ;
	}
	
	/**
	 * True, if the parameter specifies a class exactly C.
	 */
	public static Predicate<PARAM> hasClass(Class C) {
		return P -> {
			if (P != null && P.ty != null) { 
	            return C == JTypeUtils.getTopClass(P.ty) ;
			}
			return false ;
		} ;				
	}

	public static Predicate<PARAM> isSubclassOf(Class C) {
		return P -> {
			Class D = JTypeUtils.getTopClass(P.ty) ;
			if (D==null) return false ;
			return C.isAssignableFrom(D) ;
		} ;
	}
	
	public static Generator<PARAM,STEP> Apply(Class C, String methodName, Supplier<Integer> g) {
        return P -> {
        	Integer k = g.get() ;
        	try {
        	   Method M = C.getDeclaredMethod(methodName, Integer.TYPE) ;
        	   STEP[] args = { new CONST(k, JTypeUtils.convert(Integer.class)) } ;
        	   return new Maybe(new METHOD(M,null,args)) ;
        	}
        	catch(Exception e) { return null ; }       	
        } ;
    }
	

	/**
	 * Just another name for Apply.
	 */
	public static Generator<PARAM,STEP> Object(Class C, String methodName, Supplier<Integer> g) {
		return Apply(C,methodName,g) ;
	}
	
}
