/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.*;
import static Sequenic.T3.Generator.GenCombinators.* ;

import java.util.*;
import java.util.stream.Collectors;

public class CollectionLikeMG {

    public Random rnd = T3Random.getRnd() ;
    public ImplementationMap ImpsMap ;

    /**
     * The size of arrays/collections/maps to generate.
     */
    public int size = 3 ;


    public CollectionLikeMG(int size,
                             ImplementationMap ImpsMap) {
        this.size = size ;
        this.ImpsMap = ImpsMap ;
    }

    private STEP[] mkArgs(Generator<PARAM,STEP> valueMetaGenerator, int N, JType argsType) {
        List<STEP> args = new LinkedList<STEP>() ;
        for (int n=0; n<N; n++) {
            Maybe<STEP> r = valueMetaGenerator.generate(new PARAM(null,argsType)) ;
            if (r==null) {
                // System.out.println(">> fail! " + D);
                // failing to generate an element; too bad, then simply don't include it
                continue ;
            }
            args.add(r.val) ;
        }
        /*  should not do this in parallel...
        List<STEP> args =  argsTypes_
                . stream()
                . parallel()
                . map(D -> { Maybe<STEP> r = recGenerator.fun.generate(new PARAM(D)) ;
                             // recGenerator should not return null !
                             assert r!=null ;
                             return r.val ;
                           }
                )
                . collect(Collectors.toList())    ;
        */
        STEP[] r = new STEP[args.size()] ;
        int i= 0 ;
        for (STEP S : args) {
            r[i] = S ;
            i++ ;
        }
        return r ;
    }

    public Generator<PARAM,STEP> array(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
         return P -> {
        	 // P.ty is assumed to have been solved by the caller:
        	 //System.out.println("**>> MG array: " + P.ty) ;
             if (! P.ty.isConcreteArray()) return null ;
             //System.out.println("**>> MG array: " + P.ty) ;
             JTfun ty_ = (JTfun) P.ty ;
             assert ty_.args.length > 0 ;
             JType elemType = ty_.args[0] ;
             // choose the size to be generated:
             int actualSize = rnd.nextInt(size+1) ;
             STEP[] args = mkArgs(recvalueMetaGenerator.fun,actualSize,elemType) ;
             //for (int k=0; k < args.length; k++) System.out.println(">> arg[" + k + "]" + ((CONST) args[k]).cty) ;
             Maybe<STEP> col = new Maybe(new CONSTRUCT_COLLECTION(P.ty,args)) ;
             //System.out.println(">> looks ok... " + col.val);
             //System.out.println(">> P-type is concrete? " + P.cty.isConcrete()) ;
             return col ;
         }  ;
    }

    public Generator<PARAM,STEP> collection(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        return P -> {
        	// P.ty is assumed to have been solved by the caller:
            if (! P.ty.isConcreteCollection()) return null ;
            //System.out.println("**>> MG collection: " + P.ty) ;
            Class C = JTypeUtils.getTopClass(P.ty) ;
            JTfun ty_ = (JTfun) P.ty ;
            
            JType elemType = null ;
            if (ty_.args.length == 0) elemType = new JTfun(SomeObject.class) ;
            else elemType = ty_.args[0] ;

            // choose the size to be generated:
            int actualSize = rnd.nextInt(size+1) ;
            STEP[] args = mkArgs(recvalueMetaGenerator.fun,actualSize,elemType) ;
            return new Maybe(new CONSTRUCT_COLLECTION(P.ty,args)) ;
        }  ;
    }

    public Generator<PARAM,STEP> map(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        return P -> {
        	// P.ty is assumed to have been solved by the caller:
            if (! P.ty.isConcreteMap()) return null ;
            Class C = JTypeUtils.getTopClass(P.ty) ;
            JTfun ty_ = (JTfun) P.ty ;

            JType elemType = null ;
            JType keyType  = null ;
            if (ty_.args.length < 2) {
                keyType  = new JTfun(Integer.class) ;
                elemType = new JTfun(SomeObject.class) ;
            }
            else {
                keyType  = ty_.args[0] ;
                elemType = ty_.args[1] ;
            }

            // choose the size to be generated:
            int actualSize = rnd.nextInt(size+1) ;

            STEP[] keys = mkArgs(recvalueMetaGenerator.fun,actualSize,keyType) ;
            STEP[] args = mkArgs(recvalueMetaGenerator.fun,actualSize,elemType) ;

            return new Maybe(new CONSTRUCT_COLLECTION(P.ty,keys,args)) ;
        }  ;
    }

    public Generator<PARAM,STEP> collectionlike(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        //return R -> {
        //		System.out.println("***>>> ty = " + R.ty) ;
        		return 
        		FirstOf( array(recvalueMetaGenerator), 
        		collection(recvalueMetaGenerator), 
        		map(recvalueMetaGenerator))
                . WithPreRequirement(P -> P.ty.isConcrete()) ;
        //} ;
    }

}
