/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.Maybe;

public class NullMG {

    public Generator<PARAM,STEP> constant() {
        return
                P -> {
                    Class C = JTypeUtils.getTopClass(P.ty) ;
                    if (C==null) return null ;
                    if (! C.isPrimitive()) {
                       return new Maybe(new CONST(null, P.ty)) ;
                    }
                    else return null ;
                } ;
    }

    /**
     * Will generate null, and a warning.
     */
    public Generator<PARAM,STEP> defaulting() {
        return P -> {
            // Logger.getLogger("T3").log(Level.WARNING, "defaulting to generating null...");
            //System.out.println(">> defaulting to generating null...")   ;
            return constant().generate(P) ;
        }  ;
    }

}
