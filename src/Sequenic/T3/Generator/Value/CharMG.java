/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.Maybe;

import java.util.Random;

import static Sequenic.T3.Generator.GenCombinators.* ;

public class CharMG {

    public Random rnd = T3Random.getRnd() ;

    static public boolean ischarOrChar(JType ty) {
        Class C = JTypeUtils.getTopClass(ty) ;
        if (C==null) return false ;
        return (C == Character.TYPE || C == Character.class) ;
    }

    /**
     * Construct a UTF16 char generator that will randomly and uniformly generates chars whose codes
     * are between the specified integer lowerbound (inclusive) and upperbound (exclusive).
     * Consult Javadoc on the type Character.
     */
    public Generator<PARAM,STEP> uniformUTF16(int lowerbound, int upperbound) {
        assert lowerbound >= 0 ;
        assert upperbound <= 65535 + 1 ;
        assert lowerbound < upperbound ;
        return
                P -> { if (ischarOrChar(P.ty)) {
                    Character x = new Character((char) (rnd.nextInt(upperbound - lowerbound) + lowerbound))  ;
                    return new Maybe(new CONST(x, P.ty)) ;
                }
                else return null ;
                } ;
    }

    public Generator<PARAM,STEP> uniformPrintableASCII() {
        return uniformUTF16(32,127)  ;
    }

    public Generator<PARAM,STEP> uniformAlpha() {
        return FirstOf(uniformUTF16(65,91).WithChance(0.5) ,
                       uniformUTF16(97,123)
                      ) ;
    }

    public  Generator<PARAM,STEP> uniformNum() {
        return uniformUTF16(48,59)  ;
    }

    public  Generator<PARAM,STEP> alphaNum() {
        return FirstOf(uniformNum().WithChance(0.2) ,
                       uniformAlpha()
                      ) ;
    }

    /**
     * Construct a generator that randomly generates a -, space, @, or .
     */
    public  Generator<PARAM,STEP> dashSpAtDot() {
        return P -> {
            if (ischarOrChar(P.ty)) {
               int k = rnd.nextInt(4) ;
               char c = '.' ;
               switch (k)  {
                 case 1 : c = '-' ; break ;
                 case 2 : c = ' ' ; break ;
                 case 3 : c = '@' ; break ;
                 default :
               }
               return new Maybe(new CONST(new Character(c), new JTfun(Character.class))) ;
            }
            else return null ;
        }  ;
    }

    public Generator<PARAM,STEP> alphaNum_dashSpAtDot() {
        return   FirstOf(alphaNum().WithChance(0.9) ,
                         dashSpAtDot()
                        ) ;
    }

}
