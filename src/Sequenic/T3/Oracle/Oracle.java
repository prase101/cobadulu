package Sequenic.T3.Oracle;

/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
import java.io.Serializable;
import Sequenic.T3.utils.Maybe;

/**
 * Used to represent injected oracles.
 * 
 * @author Wishnu Prasetya
 *
 */
public abstract class Oracle implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Check the receiver object, returned object, and thrown exception 
	 * against an oracle. If oracle is satisfied, a null is returned, 
	 * else a string is returned explaining why it is not satisfied.
	 * 
	 * If null is passed, the object is not checked.
	 */
	public String check(
			Maybe<Object> receiverObj,
			Maybe<Object> returnedObj,
			Maybe<Throwable> thrownException) 
	{ return null ; }
	
	
	public Oracle AND(Oracle o) {
		CONJOracle c = new CONJOracle() ;
		c.o1 = this ;
		c.o2 = o ;
		return c ;
	}
	
	/**
	 * For internally representing a conjunction of two oracles.
	 */
	static class CONJOracle extends Oracle {

        private static final long serialVersionUID = 1L;
		
		public Oracle o1 ;
		public Oracle o2 ;
		
        public String check(Maybe<Object> receiverObj,
    			Maybe<Object> returnedObj,
    			Maybe<Throwable> thrownException)
        {
        	String r1 = o1.check(receiverObj,returnedObj,thrownException) ;
        	if (r1 != null) return r1 ;
        	return o2.check(receiverObj,returnedObj,thrownException) ;
        }
	}
	

	/**
	 * For internally representing True as an oracle.
	 */
	static class TTOracle extends Oracle {

        private static final long serialVersionUID = 1L;
				
	}
	
	
	public static Oracle TT = new TTOracle() ;

	
}
