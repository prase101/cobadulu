/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Oracle;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.utils.PP;
import Sequenic.T3.utils.Show;
import Sequenic.T3.utils.Maybe;
//import Sequenic.T3.utils.Show;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.logging.Logger;

import static Sequenic.T3.Reflection.Reflection.*;

/**
 * Representing an oracle of the form target = expectation. They are considered
 * equal if their string-dump is the same. Because making a string dump can be
 * expensive (if the objects are comples), only the object's own fields will be
 * dumped. So, subfields will not be dumped.
 * 
 * @author Wishnu Prasetya
 * 
 */
public class StrDumpEqOracle extends Oracle {

	private static final long serialVersionUID = 1L;

	public Maybe<String> expectedReceiverObj = null;
	public Maybe<String> expectedReturnedObj = null;
	public Maybe<String> expectedException = null;

	public StrDumpEqOracle(Maybe<Object> receiverObj, 
			Maybe<Object> returnObj,
			Maybe<Throwable> thrownException) 
	{
		if (receiverObj != null)
			try {			
  				expectedReceiverObj = new Maybe(StrDump(receiverObj.val));
  				//System.out.println(">>> injecting rec-obj oracle") ;
			} catch (Exception e) {
				// cannot str-dump! So, no expectation too.
			} ;
			
		if (returnObj != null)
			try {
				expectedReturnedObj = new Maybe(StrDump(returnObj.val));
				//System.out.println(">>> injecting RET-obj oracle") ;
			} catch (Exception e) {
				// cannot str-dump! So, no expectation too.
			} ;
		if (thrownException != null) {
			expectedException = new Maybe(ExcStrDump(thrownException.val)) ;
		    //System.out.println(">>> injecting EXC-obj oracle") ;
		}
	}

	public static String StrDump(Object o) {
		return Show.show(o, 3, 2);
	}

	private static String ExcStrDump(Throwable e) {
		if (e==null) return "null" ;
		return e.getClass().getName() ;
	}
	
	/**
	 * This is supposed to be a better StrDump.. but for some reason it does not
	 * work! FIX THIS!
	 */
	public static String StrDump___(Object o) throws Exception {
		Set visited = new HashSet();
		return StrDumpWorker(visited, 0, o);
	}

	private static List<Class> noShow;

	// Class initialization:
	static {
		noShow = new LinkedList<Class>();
		noShow.add(Random.class);
		noShow.add(Reader.class);
		noShow.add(Writer.class);
		noShow.add(InputStream.class);
		noShow.add(OutputStream.class);
		noShow.add(RandomAccessFile.class);
		noShow.add(File.class);
		noShow.add(Class.class);
	}

	static int MAX_DEPTH = 5;

	private static String StrDumpWorker(Set visited, int depth, Object o)
			throws Exception {
		Class C = o.getClass();
		if (isBoxingType(C) || C.isEnum() || (C == String.class))
			return o.toString();
		for (Class D : noShow) {
			if (D.isAssignableFrom(C))
				return ("?:" + C.getName());
		}
		if (visited.contains(o))
			return ">>> visited";
		if (depth > MAX_DEPTH)
			return ">>> MAX DEPTH";

		visited.add(o);

		// Array :
		if (C.isArray()) {
			String r = "array[";
			for (int i = 0; i < Array.getLength(o); i++) {
				if (i > 1)
					r += ", ";
				r += StrDumpWorker(visited, depth + 1, Array.get(o, i));
			}
			r += "]";
			return r;
		}

		// Collection:
		if (Collection.class.isAssignableFrom(C)) {
			String r = "collection<";
			int i = 0;
			for (Object element : (Collection) o) {
				if (i > 1)
					r += ", ";
				r += StrDumpWorker(visited, depth + 1, element);
				i++;
			}
			r += ">";
			return r;
		}

		// Map
		if (Map.class.isAssignableFrom(C)) {
			String r = "map<";
			int i = 0;
			for (Object element : ((Map) o).entrySet()) {
				if (i > 1)
					r += ", ";
				Map.Entry e = (Map.Entry) element;
				r += "(" + StrDumpWorker(visited, depth + 1, e.getKey()) + ","
						+ StrDumpWorker(visited, depth + 1, e.getValue()) + ")";
				i++;
			}
			r += ">";
			return r;
		}

		// getting all C's fields, including those declared by superclasses,
		// and we first remove those fields which have been specified not
		// to be shown:
		List<Field> allfields = getAllNonStaticFields(C);
		String r = ":" + depth + "(" + C.getName() + ") ";
		for (Field field : allfields) {
			r += "\n" + field.getName() + " -> ";
			r += StrDumpWorker(visited, depth + 1, field.get(o));
		}
		return r;
	}

	/**
	 * Get ALL fields of a class. This includes private fields, and fields of
	 * the superclasses. Fields from class Objects will be excluded. These will
	 * be the fields that will be shown.
	 * 
	 * Static, abstract, and final fields are excluded. Auxiliary fields are
	 * excluded.
	 */
	private static List<Field> getAllNonStaticFields(Class C) {
		List<Field> fields = new LinkedList<Field>();
		List<Class> ancestors = new LinkedList<Class>();
		ancestors.add(C);
		ancestors.addAll(getALLSuperClasses(C));
		for (Class D : ancestors) {
			Field[] fieldsOfD = D.getDeclaredFields();
			for (Field f : fieldsOfD) {
				int mf = f.getModifiers();
				if (Modifier.isStatic(mf) || Modifier.isAbstract(mf)
						|| Modifier.isFinal(mf))
					continue;
				// if (f.getName().startsWith(CONSTANTS.auxField_prefix))
				// continue ;
				fields.add(f);
			}
		}
		return fields;
	}

	public String check(
			Maybe<Object> receiverObj, 
			Maybe<Object> returnObj,
			Maybe<Throwable> thrownException) 
	{
		//System.out.println(">>> checking an oracle..., RET: " + returnObj + " vs " + expectedReturnedObj) ;
		if (thrownException!=null && expectedException != null) {
			String s = ExcStrDump(thrownException.val) ;
			if (! s.equals(expectedException.val)) {
				//System.out.println(">>> violating exception oracle!") ;
				String msg ;
				if (expectedException.val=="null") 
				   msg = "Expecting no exception, but got " + s ;
				else
				   msg = "Expecting: " + expectedException.val + " to be thrown, but getting: " + s ;	
				return msg;
			}
		}
		
		if (receiverObj!=null && expectedReceiverObj != null) {
			try {
				String s = StrDump(receiverObj.val);
				if (! s.equals(expectedReceiverObj.val)) {
				  String msg = "Expecting: " + expectedReceiverObj.val + "\nbut getting: " + s;
				  // Logger.getLogger(CONSTANTS.T3loggerName).info(">>> An StrDumbEqOracle detects a VIOLATION!")
				  // ;
				  return msg;
				}
			} catch (Exception e) {
				// StrDump unfortunately throws an exception, so we cannot really
				// check if it would have violated
				// the expectation --> ignore
			}
		}
		
		if (returnObj!=null && expectedReturnedObj != null) {
			try {
				String s = StrDump(returnObj.val);
				if (! s.equals(expectedReturnedObj.val)) {
				  String msg = "Expecting: " + expectedReturnedObj.val + "\nbut getting: " + s;
				  //System.out.println(">>> RET-oracle violation! " + msg) ;
				  return msg;
				}
			} catch (Exception e) {
				// ignore
			}
		}
		// none of the oracles is violated:
        return null ;	
	}

}
