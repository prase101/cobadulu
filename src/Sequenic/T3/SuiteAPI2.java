package Sequenic.T3;

import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import Sequenic.T3.Info.FunctionalCoverage;
import Sequenic.T3.Info.ObjectCoverage;
import Sequenic.T3.Sequence.Datatype.*;


/**
 * A slight extension of SuiteAPI, with common methods needed by subclasses,
 * but require java-8, or depends directly or indirectly on classes
 * that need java-8.
 * 
 * These methods were originally in SuiteAPI, but have to be moved out to allow
 * SuiteAPI to be compiled in Java7.
 */
public class SuiteAPI2 extends SuiteAPI {
	
	public TestingScope scope = null;
	public ImplementationMap impMap = null;

	public SuiteAPI2(Config config) { super(config) ; }

	/**
	 *  To drop non-violating sequences
	 */
	protected void dropNonViolatingSequences(SUITE suite) {
	    Stream<SEQ> suite_ ;
	    if (config.numberOfCores<=1) suite_ = suite.suite.stream() ;
	    else suite_ = suite.suite.parallelStream() ;
	    final Predicate<SEQ> isViolating = seq -> {
	        try {
	            SEQ_RT_info info = seq.exec(config.CUT,new Pool()) ;
	            return info.isFail() ;
	        }
	        catch (Exception e) {
	            // broken sequence; drop it:
	            return false ;
	        }
	    } ;
	    suite.suite = suite_ . filter(isViolating) . collect(Collectors.toList()) ;
	}
	
	/**
	 * To drop sequences that violate the assumption side (e.g. throwing illegal
	 * argument exception).
	 */
	protected void dropAsmViolatingSequences(SUITE suite) {
		 Stream<SEQ> suite_ ;
		    if (config.numberOfCores<=1) suite_ = suite.suite.stream() ;
		    else suite_ = suite.suite.parallelStream() ;
		    final Predicate<SEQ> isOK = seq -> {
		        try {
		            SEQ_RT_info info = seq.exec(config.CUT,new Pool()) ;
		            return ! info.lastInfo.inputWasIncorrect() ;
		        }
		        catch (Exception e) {
		            // broken sequence; drop it:
		            return false ;
		        }
		    } ;
		    suite.suite = suite_ . filter(isOK) . collect(Collectors.toList()) ;
	}

	/**
	 *  To drop sequences that for some reason fail to execute
	 */
	protected void dropBrokenSequences(SUITE suite) {
		Stream<SEQ> suite_ ;
		if (config.numberOfCores<=1) suite_ = suite.suite.stream() ;
		else suite_ = suite.suite.parallelStream() ;
		final Predicate<SEQ> isOk = seq -> {
			Pool pool = new Pool() ;
			try {
				SEQ_RT_info info = seq.exec(config.CUT,new Pool()) ;
				return true ;
			}
			catch (Exception e) {
				// failing sequence; drop it:
				return false ;
			}
		} ;
		suite.suite = suite_ . filter(isOk) . collect(Collectors.toList()) ;
	}

	public void reportCoverage(SUITE S) throws Exception {
		
	    SUITE_RT_info suiteinfo = S.exec(
	    		new Pool(), 
	    		new ObjectCoverage(config.CUT), 
	    		0, 
	    		0, 
	    		false, 
	    		true, 
	    		config.regressionMode, 
	    		null) ;
	
	    suiteinfo.printReport(config.reportOut);
	    
	    FunctionalCoverage fc = new FunctionalCoverage(scope,config.reportOut) ;
	    fc.calculate(S);
	    fc.printReport(config.reportVerbosity); 
	    flushReportStream() ;
	}

}
