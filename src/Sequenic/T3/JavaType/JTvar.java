/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.JavaType;

import Sequenic.T3.utils.SomeObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Represent a type variable/parameter. Such a type variable may have a set of upperbounds.
 * An upperbound is another type expression. 
 * Check Java Language Specification, for the specific syntax and structure of type
 * variable/parameter.
 */
public class JTvar implements JType {

    private static final long serialVersionUID = 1L;

    public String name ;
    public JType[] upperbounds ;


    public JTvar(String name, JType ... upperbounds) {
        this.name = name ;
        this.upperbounds = upperbounds ;
    }

    public boolean equals (Object o) {
        if (! (o instanceof JTvar)) return false ;
        JTvar v2 = (JTvar) o ;
        return  name.equals(v2.name) && Arrays.equals(upperbounds, v2.upperbounds) ;
    }

    public int hashCode() {
        int r = name.hashCode() ;
        for (JType u : upperbounds) r = r ^ u.hashCode() ;
        return r ;
    }

    public String toString(){
        String r = "" + name ;
        if (upperbounds.length > 0) {
            r += " extends " ;
            int i=0;
            for (JType u : upperbounds) {
                if (i>0) r += " & " ;
                r += u.toString() ;
                i++ ;
            }
        }
        return r ;
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException
    {
        name =  (String) stream.readObject()  ;
        upperbounds = (JType[]) stream.readObject()  ;
    }


    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(name);
        stream.writeObject(upperbounds) ;
    }

    public boolean isUnparameterizedType() { return false ; }
    public boolean isConcrete() { return false ; }
    public boolean isConcreteArray() { return false ; }
    public boolean isConcreteCollection() { return false ;}
    public boolean isConcreteMap() { return false ; }

    
    public JType subst(Map<String,JType> substitutions) {
    	JType t = substitutions.get(name) ;
    	if (t!=null) return t ; // we don't check if t is an allowed instantiation of this type variable 
    	                        // ... should be checked at the upper level 
    	
    	for (int i=0; i<upperbounds.length; i++) upperbounds[i] = upperbounds[i].subst(substitutions) ;
    	return this ;
    }
}
