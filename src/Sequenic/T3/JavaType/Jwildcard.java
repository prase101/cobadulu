/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.JavaType;

import Sequenic.T3.utils.SomeObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * Representing a wildcard type.
 */
public class Jwildcard implements JType{

    private static final long serialVersionUID = 1L;

    /**
     * According to Java Specification Language, Java-7, there can only be
     * maximum one lower or one upper bound.
     */
    public JType[] lowerbounds ;
    public JType[] upperbounds ;

    public Jwildcard(JType ... upperbounds) {
        this.upperbounds = upperbounds ;
        lowerbounds = new JType[0] ;
    }

    public void setLowerBounds(JType ... lowerbounds)  {
        this.lowerbounds = lowerbounds ;
    }

    public boolean equals(Object o) {
        if (! (o instanceof Jwildcard)) return false ;
        Jwildcard w2 = (Jwildcard) o ;
        return Arrays.equals(lowerbounds,w2.lowerbounds) &&
               Arrays.equals(upperbounds,w2.upperbounds) ;
    }

    public int hashCode() {
        int r = 7723 ;
        for (JType C : upperbounds) r = r ^ C.hashCode() ;
        for (JType C : lowerbounds) r = r ^ C.hashCode() ;
        return r ;
    }

    public String toString(){
        String r = "?" ;
        if (lowerbounds.length > 0) {
            r += " super " ;
            int i=0;
            for (JType lo : lowerbounds) {
                if (i>0) r += ", " ;
                r +=  lo.toString() ;
                i++ ;
            }
            return r ;
        }
        if (upperbounds.length > 0) {
            r += " extends " ;
            int i=0;
            for (JType u : upperbounds) {
                if (i>0) r += ", " ;
                r +=  u.toString() ;
                i++ ;
            }
        }
        return r ;
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException
    {
        lowerbounds = (JType[]) stream.readObject()  ;
        upperbounds = (JType[]) stream.readObject()  ;
    }


    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(lowerbounds);
        stream.writeObject(upperbounds);
    }

    public boolean isUnparameterizedType() { return false ; }
    public boolean isConcrete() { return false ; }
    public boolean isConcreteArray() { return false ; }
    public boolean isConcreteCollection() { return false ;}
    public boolean isConcreteMap() { return false ; }
    
    public JType subst(Map<String,JType> substitutions) {
    	for (int i=0; i<lowerbounds.length; i++) lowerbounds[i] = lowerbounds[i].subst(substitutions) ;
    	for (int i=0; i<upperbounds.length; i++) upperbounds[i] = upperbounds[i].subst(substitutions) ;
    	return this ;
    }
}
