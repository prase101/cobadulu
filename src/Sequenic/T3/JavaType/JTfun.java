/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.JavaType;

import Sequenic.T3.Reflection.Reflection;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;


/**
 * Used to type expressions representing a class or interface. Note that such a 
 * type expression may have parameters, as in {@literal List<Integer>}, or
 * {@literal List<T>}.
 *
 * Note: e.g. the type LinkedList (without parameter) will be represented as a type without
 * parameter.
 */
public class JTfun implements JType {

    private static final long serialVersionUID = 1L;

    public Class fun ;
    public JType[] args ;

    public JTfun(Class fun, JType ... args) {
        this.fun = fun ;
        this.args = args ;
    }

    public boolean equals(Object o) {
        if (! (o instanceof JTfun)) return false ;
        JTfun f2 = (JTfun) o ;
        // args cannot be null
        return fun.equals(f2.fun) && Arrays.equals(args,f2.args) ;
    }

    public int hashCode() {
        int r = fun.hashCode() ;
        for (JType ty : args) r = r ^ ty.hashCode() ;
        return r ;
    }

    public String toString(){
        String r = "" + fun.getSimpleName() ;
        //System.out.println("=== " + r) ;
        if (args.length > 0) {
            r += " <" ;
            int i=0;
            for (JType ty : args) {
               if (i>0) r += ", " ;
               r += ty ;
               i++ ;
            }
            r += ">" ;
        }
        return r ;
    }


        /**
         * For serialization.
         */
    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException, SecurityException
    {
        String className =  (String) stream.readObject() ;
        if (className.equals("boolean")) fun = Boolean.TYPE ;
        else if (className.equals("byte")) fun = Byte.TYPE ;
        else if (className.equals("short")) fun = Short.TYPE ;
        else if (className.equals("int")) fun = Integer.TYPE ;
        else if (className.equals("long")) fun = Long.TYPE ;
        else if (className.equals("char")) fun = Character.TYPE ;
        else if (className.equals("float")) fun = Float.TYPE ;
        else if (className.equals("double")) fun = Double.TYPE ;
        else fun =  Reflection.getClassFromName(className)  ;
        args = (JType[]) stream.readObject() ;
    }


    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(fun.getName());
        stream.writeObject(args);
    }

    public boolean isUnparameterizedType() {
        return (args.length==0) ;
    }

    public boolean isConcrete() {
        if (args.length==0) return true ;
        for (JType ty : args) {
            if (!ty.isConcrete()) return false ;
        }
        return true ;
    }

    public boolean isConcreteArray() {
        if (! (fun == Array.class))  return false ;
        return isConcrete() ;
    }

    public boolean isConcreteCollection() {
        if (! Collection.class.isAssignableFrom(fun))  return false ;
        return isConcrete() ;
    }

    public boolean isConcreteMap() {
        if (! Map.class.isAssignableFrom(fun))  return false ;
        return isConcrete() ;
    }

    
    public JType subst(Map<String,JType> substitutions) {
    	for (int i=0; i<args.length; i++) args[i] = args[i].subst(substitutions) ;
    	return this ;
    }

}
