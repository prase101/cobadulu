/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;


import Sequenic.T3.JavaType.* ;

import java.util.* ;
import java.util.Map.Entry;

/**
 * Represents an object pool. It is used by T3's test engine to store
 * objects that have been created, to be re-used along a test-sequence,
 * for testing side-effect over them.
 *
 * <p> A Pool will be organized as follows. Essentially, it is a set
 * of objects. Let's call this set S. The pool contains an <u>object
 * map</u>, which is a mapping from unique integer keys to objects in
 * S. The pool also has a <u>domain map</u>, which is a mapping from
 * concrete types to 'domains'. If the domain map maps a type T to a domain D,
 * then D is essentially a set of objects, all should be of the type T.
 * Each domain will be implemented as a linked list of the integer
 * indices over the object map.
 *
 * Note (1): currently, only concrete types are allowed to be put in the pool
 * (as explained above, to group the objects).
 *
 * Note (2): null should not be stored in the pool.
 *
 */

public class Pool {

	protected HashMap<Integer,Object>   objectMap ;
	protected HashMap<JType,LinkedList<Integer>> domainMap ;
	protected int objectCount ;
    protected Integer indexOfObjectUnderTest = null ;
    protected Object  objectUnderTest = null ;
    protected JType typeOfObjectUnderTest = null ;


	/**
	 * Just a random generator.
	 */
	private Random rnd ;

	/**
	 * Create an empty pool.
	 */
	public Pool() {
		objectMap = new HashMap<Integer,Object>() ;
		domainMap = new HashMap<JType,LinkedList<Integer>>() ;
		objectCount = 0 ;
		rnd = T3Random.getRnd() ;
	}

	/**
	 * To reset the pool to its initial empty state.
	 */
	public void reset() {
		objectMap = new HashMap<Integer,Object>() ;
		domainMap = new HashMap<JType,LinkedList<Integer>>() ;
		objectCount = 0 ;
		indexOfObjectUnderTest = null ;
		objectUnderTest = null ;
	    typeOfObjectUnderTest = null ;
	}

	/**
	 * Returns the object from objectMap indexed with i; i should be an existing index.
	 */
	public Object get(int i) { return objectMap.get(i) ; }
	
	/**
	 * Find an object in the pool. It returns its index, else null.
	 */
	public Integer find(Object x) {
		for (Map.Entry<Integer,Object> e : objectMap.entrySet()) {
			if (e.getValue() == x) return e.getKey() ;
		}
		return null ;
	}

	/**
	 * Randomly draw an object of a concrete type ty from the pool. It actually
	 * returns the index of the object rather than the object
	 * itself. It returns null if no instance of ty is found in the
	 * pool.
     *
     * Note that we will only look into the objects that have been explicitly
     * marked to have the type ty. We will not search for possible matching
     * instances via subclassing etc.
     *
	 */
	public Integer rndGetIndex(JType ty) {
        List<Integer> domain = domainMap.get(ty) ;
        if (domain == null || domain.isEmpty()) return null ;
		else return domain.get(rnd.nextInt(domain.size())) ;
	}
	
	/**
	 * Add an object u of concrete type ty into the pool. It returns the index
	 * of u in the pool's object map.  Note that ty must be a concrete type (it
     *  should not contain type variables nor wildcards).
	 */
	public int put(JType ty, Object u) {
		assert u != null ;
		//if (ty==null) System.out.println("### ty is NULL!!!") ;
		int index = objectCount ;
        // System.out.println(">> putting an " + ty + " at REF " + index) ;
		objectCount++ ;
		objectMap.put(index,u) ;
		if (domainMap.containsKey(ty))
			(domainMap.get(ty)) . add(index) ;
		else {
			LinkedList<Integer> domain = new LinkedList<Integer>() ;
			domain.add(index) ;
			domainMap.put(ty,domain) ;
		} 
		return index ;
	}

    /**
     * To mark that an object in the pool is an object under test in the pool. The object must
     * be already in the pool.
     */
    public int markAsObjectUnderTest(Object u) {
    	assert (u != null) ;
        //System.out.println("** u:" + u.getClass().getSimpleName() + ", val = " + u );
        Integer k = find(u) ;
        if (k==null) {
            System.out.println(">>> this obj under test is missing form the pool!: " + u.getClass().getName()) ;
        }
        assert k != null ;
        indexOfObjectUnderTest = k ;
        objectUnderTest = u ;
        //System.out.println("** u:" + u.getClass().getSimpleName() + ", val = " + u + ", at " + k);
        
        for(Map.Entry<JType,LinkedList<Integer>> e : domainMap.entrySet()) {
             if (e.getValue().contains(k))  {
                 typeOfObjectUnderTest = e.getKey() ;
                 break ;
             }
        }

        return k ;
    }

    public Integer getIndexOfObjectUnderTest() {
        return indexOfObjectUnderTest ;
    }

    public Object getObjectUnderTest() {
        return objectUnderTest ;
    }

    public JType getTypeOfObjectUnderTest() {
        return typeOfObjectUnderTest;
    }
    
    public String toString() {
    	String s = "Object under test @" + indexOfObjectUnderTest 
    			+ " : " + typeOfObjectUnderTest ;
    	for (Entry e : objectMap.entrySet()){
    		s += "\n [" + e.getKey() + "] " + e.getValue().getClass() ;
    	}
    	return s ;
    }

	/**
	 * For testing the class.
	 */
	/*
	public static void main(String[] args) {
		A a1 = new A("Alice") ;
		A a2 = new A("Bob") ;
		A2 a3 = new A2("Vilain",100) ;
		A2 a4 = new A2("Vilain",999) ;

		Pool p = new Pool() ;
		p.put(a1) ;
		p.put(a2) ;
		p.put(a3) ;
		p.put(a4) ;

		Class A  = a1.getClass() ;
		Class A2 = a3.getClass() ;

		System.out.println("# " + p.get(p.rndGetIndex(A))) ;
		System.out.println("# " + p.get(p.rndGetIndex(A))) ;
		System.out.println("# " + p.get(p.rndGetIndex(A))) ;
		System.out.println("# " + p.get(p.rndGetIndex(A))) ;
		System.out.println("# " + p.get(p.rndGetIndex(A))) ;
		System.out.println("> " + p.get(p.rndGetIndex(A2))) ;
		System.out.println("> " + p.get(p.rndGetIndex(A2))) ;
		System.out.println("> " + p.get(p.rndGetIndex(A2))) ;
		System.out.println("> " + p.get(p.rndGetIndex(A2))) ;
		System.out.println("> " + p.get(p.rndGetIndex(A2))) ;
		
	}
	 */

}

