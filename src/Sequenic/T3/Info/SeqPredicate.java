package Sequenic.T3.Info;

import Sequenic.T3.Pool;
import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Sequence.Datatype.*;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class SeqPredicate {
	
	static private boolean isCreationStep(STEP step) {
		if (step instanceof CONSTRUCTOR) {
			return ((CONSTRUCTOR) step).isObjectUnderTestCreationStep ;
		}
		if (step instanceof METHOD) {
			return ((METHOD) step).isObjectUnderTestCreationStep ;
		}
		return false ;
	}
	
	abstract static class LTL {
		
		abstract void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) ;
		
		// DSL combinators, to mimic binary operator:
		public LTL until(StepPredicate q) { return new UNTIL(this, new NOW(q)) ; }
		public LTL until(LTL q) { return new UNTIL(this,q) ; }
		public LTL unless(StepPredicate q) { return new WEAKUNTIL(this, new NOW(q)) ; }
		public LTL unless(LTL q) { return new WEAKUNTIL(this,q) ; }
		public LTL and(StepPredicate q) { return new AND(this, new NOW(q)) ; }
		public LTL and(LTL q) { return new AND(this,q) ; }
		public LTL or(StepPredicate q) { return new OR(this, new NOW(q)) ; }
		public LTL or(LTL q) { return new OR(this,q) ; }		
		public LTL imp(StepPredicate q) { return new OR(not(this), new NOW(q)) ; }
		public LTL imp(LTL q) { return new OR(not(this),q) ; }	
		
		
		// prepare a list of labelled-execution; each position i will be labbeled by TT.
		Set[] mkInitialLabelledExecution(List<STEP> seq) {
			int k = 0 ;
			for (STEP step : seq) {
				if (step instanceof INSTRUMENT) continue ;
				k++ ;
			}
			Set[] r = new Set[k] ;
			for (int i=0; i<k ; i++) { HashSet label = new HashSet<LTL>() ; label.add(TT) ; r[i] = label ; }
			return r ;
		}
		
		
		/**
		 * An LTL formula is valid, if we can label its first step with the formula.
		 */
		boolean valid(Class CUT, Pool pool, SEQ seq)  {
			Set[] labelledExecution = mkInitialLabelledExecution(seq.steps) ;
			Set   seenSubFormulas = new HashSet() ;
			labelling(CUT, pool, seq.steps, labelledExecution, seenSubFormulas);
			return labelledExecution.length>0 && labelledExecution[0].contains(this) ;
		}
	}
	
	// representation of true-LTL
	public static final LTL TT = new NOW(new StepPredicate()) ;

	
	static class NOW extends LTL {
		StepPredicate p ;
		
		NOW(StepPredicate p) { this.p = p ; }
		
		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			// if the execution has already been labelled with p:
			if (seenSubFormulas.contains(p) || seenSubFormulas.contains(this)) return ;
			
			// else execute the sequence and label:
			
			pool.reset() ;
			int k = 0 ;
			for (STEP step : seq) {
				if (step instanceof INSTRUMENT) continue ;
				try {
					STEP_RT_info info = step.exec(CUT,pool,p.precond) ;				
					if (k==0 && isCreationStep(step) && info.returnedObj != null) { 
						pool.markAsObjectUnderTest(info.returnedObj) ;
					}
					if (p.postcond != null && p.postcond.test(info)) {
						labelledExecution[k].add(p) ;
						labelledExecution[k].add(this) ;
					}
				}
				catch(Exception e) { 
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					
					Logger.getLogger(CONSTANTS.T3loggerName).warning("*** A sequence failed to execute completely!" + step
							+ ", throwing " + sw.toString()
							) ;
					break ;
				}
				k++ ;
			}
			
			seenSubFormulas.add(p) ;
			seenSubFormulas.add(this) ;
		}
		
	}
	

	static class NOT extends LTL {
		LTL p ;
		NOT(LTL p) { this.p = p ; }
		
		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			if (!seenSubFormulas.contains(p)) {
				p.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			int N = labelledExecution.length ;
			for (int k=0; k<N; k++) {
				if (! labelledExecution[k].contains(p)) labelledExecution[k].add(this) ;
			}
			seenSubFormulas.add(this) ;
		}
	}
	
	static class NEXT extends LTL {
		LTL p ;
		NEXT(LTL p) { this.p = p ; }
		
		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			if (!seenSubFormulas.contains(p)) {
				p.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			
			int N = labelledExecution.length ;
			for (int k=0; k<N-1; k++) {
				if (labelledExecution[k+1].contains(p)) labelledExecution[k].add(this) ;
			}
			
			seenSubFormulas.add(this) ;
		}
		
	}
	
	static class AND extends LTL {
		LTL p1 ;
		LTL p2 ;
		
		AND(LTL p1, LTL p2) { this.p1 = p1 ; this.p2 = p2 ;}
				
		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			// handle the labelling with the sub-formulas first:
			if (!seenSubFormulas.contains(p1)) {
				p1.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			if (!seenSubFormulas.contains(p2)) {
				p2.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			// now label with p1 && p2 :
			int N = labelledExecution.length ;
			for (int k=0; k<N ; k++) {
				if (labelledExecution[k].contains(p1) && labelledExecution[k].contains(p2))
					labelledExecution[k].add(this) ;
			}
			seenSubFormulas.add(this) ;
		}
		
	}
	
	
	static class OR extends LTL {
		LTL p1 ;
		LTL p2 ;
		
		OR(LTL p1, LTL p2) { this.p1 = p1 ; this.p2 = p2 ;}
				
		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			// handle the labelling with the sub-formulas first:
			if (!seenSubFormulas.contains(p1)) {
				p1.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			if (!seenSubFormulas.contains(p2)) {
				p2.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			// now label with p1 && p2 :
			int N = labelledExecution.length ;
			for (int k=0; k<N ; k++) {
				if (labelledExecution[k].contains(p1) || labelledExecution[k].contains(p2))
					labelledExecution[k].add(this) ;
			}
			seenSubFormulas.add(this) ;
		}
		
	}
	
	static class UNTIL extends LTL {
		LTL p1 ;
		LTL p2 ;
		
		UNTIL(LTL p1, LTL p2) { this.p1 = p1 ; this.p2 = p2 ;}

		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			// handle the labelling with the sub-formulas first:
			if (!seenSubFormulas.contains(p1)) {
				p1.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			if (!seenSubFormulas.contains(p2)) {
				p2.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			// now label with p1 UNTIL p2 :
			int N = labelledExecution.length ;		
			for (int startingIndex=0 ; startingIndex<N ; startingIndex++) {
				for (int j=startingIndex; j<N ; j++) {
					// eventually hold:
					if (labelledExecution[j].contains(p2)) {
						for (int k=startingIndex; k<=j ; k++) labelledExecution[k].add(this) ;
						startingIndex = j ;
						break ;
					}
					// persistence is broken, the until prop does not hold; dont add label:
					if (!labelledExecution[j].contains(p1)) {
						startingIndex = j ;
						break ;
					}
				}		
			}
			seenSubFormulas.add(this) ;
		}	
		
	}
	
	
	static class WEAKUNTIL extends LTL {
		LTL p1 ;
		LTL p2 ;
		
		WEAKUNTIL(LTL p1, LTL p2) { this.p1 = p1 ; this.p2 = p2 ;}

		void labelling(Class CUT, Pool pool, List<STEP> seq, Set[] labelledExecution, Set seenSubFormulas) {
			// handle the labelling with the sub-formulas first:
			if (!seenSubFormulas.contains(p1)) {
				p1.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			if (!seenSubFormulas.contains(p2)) {
				p2.labelling(CUT,pool,seq,labelledExecution, seenSubFormulas);
			}
			// now label with p1 UNTIL p2 :
			int N = labelledExecution.length ;		
			for (int startingIndex=0 ; startingIndex<N ; startingIndex++) {
				int j ;
				for (j=startingIndex; j<N ; j++) {
					// eventually hold:
					if (labelledExecution[j].contains(p2)) {
						for (int k=startingIndex; k<=j ; k++) labelledExecution[k].add(this) ;
						startingIndex = j ;
						break ;
					}
					// persistence is broken, the until prop does not hold; dont add label:
					if (!labelledExecution[j].contains(p1)) {
						startingIndex = j ;
						break ;
					}
				}	
				if (j==N) {
					// then p1 holds from startingIndex upto N-1 
					for (int k=startingIndex; k<=j ; k++) labelledExecution[k].add(this) ;
					startingIndex = j ; // effectively this breaks the loop
				}
			}
			seenSubFormulas.add(this) ;
		}	
	}
	
	// unary DSL combinators:
	public static LTL now(StepPredicate p) { return new NOW(p) ; }
	public static LTL next(StepPredicate p) { return new NEXT(new NOW(p)) ; }
	public static LTL next(LTL p) { return new NEXT(p) ; }
	public static LTL not(StepPredicate p) { return new NOT(new NOW(p)) ; }
	public static LTL not(LTL p) { return new NOT(p) ; }
	public static LTL eventually(StepPredicate p) { return new UNTIL(TT, new NOW(p)) ; }
	public static LTL eventually(LTL p) { return new UNTIL(TT, p) ; }
	public static LTL always(StepPredicate p) { return not(eventually(not(p))) ; }
	public static LTL always(LTL p) { return not(eventually(not(p))) ; }
	

	
 

}
