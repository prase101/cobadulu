package Sequenic.T3.Info;

import java.util.function.Predicate;

import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.* ;
import Sequenic.T3.utils.Pair;
import static Sequenic.T3.Info.StepPredicate.* ;
import static Sequenic.T3.Info.SeqPredicate.* ;

public class SuiteQuery {
	
	SUITE suite ;
	Class CUT ;
	Pool pool ;
	
	public SuiteQuery(SUITE s) throws Exception {
		this.suite = s ;
		CUT = Class.forName(s.CUTname) ;
		pool = new Pool() ;
	}
	
	SeqPredicate.LTL phi = SeqPredicate.TT ;
	
	public SuiteQuery with(SeqPredicate.LTL phi) {
		this.phi = phi ;
		return this ;
	}
		
	/**
	 * Execute the query, producing a new suite.
	 */
	public SUITE exec() {
		SUITE newSuite = new SUITE(suite.CUTname) ;
		for (SEQ sigma : suite.suite) {
			if (phi.valid(CUT,pool,sigma)) newSuite.suite.add(sigma) ;
		}
		return newSuite ;
	}
	
	public int count() {
		return exec().suite.size() ;
	}
	
	public boolean satisfied() {
		return count() > 0 ;
	}
	
	public boolean valid() {
		return count() == suite.suite.size() ;
	}
	
	/**
	 * Execute the query, and show the execution of the k-th sequence that results from
	 * the query.
	 */
	public void show(int k) throws Exception {
		SUITE T = exec() ;
		if (T.suite.isEmpty()) System.out.println("** No sequence to show.") ;
		SEQ sigma = T.suite.get(k) ;
		sigma.exec(CUT, pool, null, 0, 4, System.out) ;
	}
	
	public void show() throws Exception {
		show(0) ;
	}
	
	public static void main(String[] args) throws Exception {
		// some examples of the query
		SUITE S = SUITE.load("ADT_SimpleSortedList.tr") ;
		System.out.println(">>> size = " + S.suite.size()) ;
		
		
		SuiteQuery query1 = new SuiteQuery(S) 
		                   . with(eventually(preCond(ofMethod("get").and(s -> s.tobj != null))
		                                     .andPostCond(s -> s.returnedObj == null))
		                                )
		                    ;
		
		// using hoare-combinator:
		SuiteQuery query2 = new SuiteQuery(S).with(eventually(hoare(
				   s -> s.tobj != null,
				   "get",
				   s -> s.returnedObj == null))
				) ;

		System.out.println(">> query 1 = " + query1.count()) ;
		System.out.println(">> query 2 = " + query2.count()) ;
		
		System.out.println(">> showing query 1: ") ;
		query1.show() ;
		
	}
	
}
