package Sequenic.T3.Info;

import static Sequenic.T3.Reflection.Reflection.getALLSuperClasses;

import java.io.OutputStream;
import java.lang.reflect.*;
import java.util.*;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.utils.Pair;

public class ObjectCoverage {
	
	public Class CUT ;
	/**
	 * All non-static, non-abstract, and non-final fields of CUT
	 */
	public LinkedList<Field> fields ;
	
	/**
	 * A set of integers, encoding all possible pairs of abstracted values 
	 * over the fields of CUT.
	 */
	public Set<Integer> allpairs ;
	
	public Set<Integer> coveredPairs = new HashSet<Integer>() ;
	
	
	public ObjectCoverage(Class CUT) {
		this.CUT = CUT ;
		fields = getAllNonStaticFields(CUT) ;
		allpairs = this.allPairsToCover() ;
	}
	
	
	static private LinkedList<Field> getAllNonStaticFields(Class C) {
		LinkedList<Field> fields = new LinkedList<Field>();
        List<Class> ancestors = new LinkedList<Class>() ;
        ancestors.add(C) ;
        ancestors.addAll(getALLSuperClasses(C))  ;
        for (Class D : ancestors) {
            Field[] fieldsOfD = D.getDeclaredFields() ;
            for (Field f : fieldsOfD)   {
                int mf = f.getModifiers() ;
                if (Modifier.isStatic(mf) || Modifier.isAbstract(mf) || Modifier.isFinal(mf)) continue ;
                if (f.getName().startsWith(CONSTANTS.auxField_prefix)) continue ;
                fields.add(f) ;
            }
        }
        return fields ;
    }
	
	private int[] getProfile() {
		LinkedList<Integer> profile = new LinkedList<Integer>() ;
		for (Field f : fields) {
			Class fclass = f.getClass() ;
			if (fclass==Character.class || fclass==Character.TYPE) {
				profile.addLast(1) ; continue ;
			}
			if (fclass==Boolean.class || fclass==Boolean.TYPE) {
				profile.addLast(1) ; continue ;
			}
			if (Reflection.isPrimitiveLike(CUT)
			    || fclass==String.class
		        || fclass.isArray()
		        || Collection.class.isAssignableFrom(CUT)
				) {
				profile.addLast(2) ; continue ;
			}
			profile.addLast(1) ;
		}
		int[] profile_ = new int[profile.size()] ;
		for (int i=0; i<profile_.length; i++) profile_[i] = profile.get(i) ;
		return profile_ ;
	}
	
	
	static private int calculateCombination(int x, int valx, int y, int valy) {
		return (x+1)*1000 + valx*100 + (y+1)*10 + valy ;
	}
	
	private Set<Integer> allPairsToCover() {
		
		Set<Integer> allpairs = new HashSet<Integer>() ;
		
		int[] profile = getProfile() ;
		int N = profile.length ;
		
		for(int x = 0; x<N; x++) {
			for (int y=x+1; y<N; y++) {
				for(int valx = 0 ; valx <= profile[x]; valx++) {
					for(int valy=0; valy <= profile[y]; valy++) {
						allpairs.add(calculateCombination(x,valx,y,valy)) ;
						//System.out.println("*>> to cover: " + calculateCombination(x,valx,y,valy)) ;
					}
				}
			}
		}	
		return allpairs ;
	}

	
	/**
	 * Get the abstract value of the field f in an object o.
	 */
	private int getAbstractValue(Field f, Object o) {
		Class fclass = f.getClass() ;
		Object val = null ;
		try {
		   f.setAccessible(true) ;
		   val = f.get(o) ;
		}
		catch(Exception e) { 
			// if we can't get the value of the field, assume it is 0
			return 0 ;
		}
		if (val == null) return 0 ;
	    
		if (fclass==Character.class || fclass==Character.TYPE) return 1 ;
		
	    if (fclass==Boolean.class || fclass==Boolean.TYPE) {
		   if (((boolean) val)==false) return 0 ;
		   else return 1 ;
		}
	    if (Reflection.isPrimitiveLike(fclass)) {
	    	if (((double) val)==0)  return 0 ;
	    	if (((double) val) > 0) return 1 ;
	    	return 2 ;
	    }
	    if (fclass.isArray()) {
	    	if (Array.getLength(o) == 0) return 2 ;
	    	return 1 ;
	    }
	    if (Collection.class.isAssignableFrom(fclass)) {
	    	if (((Collection) o).size() > 0) return 2 ;
	    	return 1 ;
	    }
	    // for other types of the field, if it is not null then map it to 1
	    return 1 ;
	}
	
	/**
	 * Clear the list of covered pairs.
	 */
	public void reset() {
		coveredPairs.clear(); 
	}
	
	/**
	 * Add the pairs covered by the given object to the set of covered pairs.
	 */
	public void addCoveredPairs(Object o) {
		int N = fields.size() ;
		int[] vector = new int[N] ;
		int i = 0 ;
		for (Field f : fields) {
			vector[i] = getAbstractValue(f,o) ;
			i++ ;
		}
		for(int x = 0; x<N; x++) {
			for (int y=x+1; y<N; y++) {
				coveredPairs.add(calculateCombination(x,vector[x],y,vector[y])) ;
				//System.out.println("*>> adding " + calculateCombination(x,vector[x],y,vector[y])) ;
			}
		}
	}
	
	
    private void write(OutputStream out, String s) throws Exception {
    	out.write(s.getBytes());
    }
    
    private void writeln(OutputStream out, String s) throws Exception {
    	write(out, s + "\n") ;
    }
    
    private String coverage(int covered, int tobeCovered) {
    	String s = "" + covered + "/" + tobeCovered ;
    	if (tobeCovered==0) s += " (100%)" ;
    	else {
    		double p = Math.round(10000 * covered/tobeCovered)/100.0 ;
    		s += " (" + p + "%)" ;
    	}
    	return s ;	
    }
    
    
    public void printReport(OutputStream out) throws Exception {
    	writeln(out, "** Field-pairs' abstract values coverage : " +  coverage(coveredPairs.size(),allpairs.size())) ;
    }

	public static void main(String[] args) {
	
	}

}
