package Sequenic.T3.Info;

import java.util.*;

/**
 * Simple representation of coverage info.
 */

public class Coverage<C> {

	public HashMap<C,Integer> map ;
	
	public Coverage() { map = new HashMap<C,Integer>() ; }
	
	public void register(C x) {
		if (map.containsKey(x)) {
			int k = map.get(x) ;
			map.put(x, k+1) ;
		}
		else map.put(x, 1) ;
	}
	
	public void register(List<C> xs) {
		for (C x : xs) register(x) ;
	}
	
	public Coverage<C> merge(Coverage<C> D) {
		Coverage<C> E = new Coverage() ;
		for (Map.Entry<C,Integer> e : map.entrySet()) {
			E.map.put(e.getKey(), e.getValue()) ;
		}
		for (Map.Entry<C,Integer> e : D.map.entrySet()) {
			C x = e.getKey() ;
			if (E.map.containsKey(x)) {
				int k = E.map.get(x) ;
				E.map.put(x, k + e.getValue()) ;
			}
			else E.map.put(x, e.getValue()) ;
		}
		return E ;
	}
	
	public float getPercentage(List<C> goals) {
		int coveredGoals = 0 ;
		for (C x : goals) {
			Integer k = map.get(x) ;
			if (k!=null && k>0) coveredGoals++ ;
		}
		return ((float) coveredGoals) / ((float) goals.size()) ;
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer() ;
		int k = 0 ;
		for (Map.Entry<C,Integer> e : map.entrySet()) {
			if (k>0) buf.append("\n") ;
			buf.append("  " + e.getKey().toString() + " : " + e.getValue()) ;
		}
		return buf.toString() ;
	}
	
}
