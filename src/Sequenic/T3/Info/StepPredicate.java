package Sequenic.T3.Info;

import java.util.function.Function;
import java.util.function.Predicate;
import Sequenic.T3.Sequence.Datatype.*;


public class StepPredicate {
	
	
	public static class PreState {
		Object tobj ;
		STEP step ;
		Object[] args ;
		public PreState(Object tobj, STEP step, Object[] args) {
			this.tobj = tobj ; this.step = step ; this.args = args ;
		}
	}
	   	
	private static class MyPreConditionCheckPlaceHolder extends STEP.PreConditionCheckPlaceHolder {
		Predicate<PreState> precond ;
		PreState state = new PreState(null,null,null) ;
		MyPreConditionCheckPlaceHolder(Predicate<PreState> precond) { this.precond = precond ; }

		public void test(Object targetObj, STEP step, Object[] stepArgs) { 
			state.tobj = targetObj ;
			state.step = step ;
			state.args = stepArgs ;
			result = precond.test(state) ;
		}
	}
	
	
	MyPreConditionCheckPlaceHolder precond = null ;
	Predicate<STEP_RT_info> postcond = s -> true ;
	
	public StepPredicate withPreCond(Predicate<PreState> p) {
		precond = new MyPreConditionCheckPlaceHolder(p) ;
		return this ;
	}
	
	public static StepPredicate preCond(Predicate<PreState> p) {
		StepPredicate sp = new StepPredicate() ;
		sp.withPreCond(p) ;
		return sp ;
	}
	
	public static StepPredicate pre(Predicate<PreState> p) { return preCond(p) ; }
	
	public StepPredicate andPostCond(Predicate<STEP_RT_info> q) {
		postcond = state -> {
			if (! precond.result) return false ;
			return q.test(state) ;
		} ;
		return this ;
	}
	
	public StepPredicate andPost(Predicate<STEP_RT_info> q) {
		return andPostCond(q) ;
	}
	
    public StepPredicate impPostCond(Predicate<STEP_RT_info> q) {
		postcond = state -> {
			if (! precond.result) return true ;
			return q.test(state) ;
		} ;	
		return this ;
	}
    
    public StepPredicate impPost(Predicate<STEP_RT_info> q) {
    	return impPostCond(q) ;
    }
    
	public static StepPredicate hoare(Predicate<PreState> p, String mname, Predicate<STEP_RT_info> q) {
		StepPredicate sp = preCond(ofMethod(mname).and(p)).impPostCond(q) ;
		return sp ;
	}
		
	static public Predicate<PreState> ofMethod(String name) {
		return state -> {
			STEP step = state.step ;
			if (step instanceof METHOD) {
				METHOD m = (METHOD) step ;			
				return (m.method.getName().equals(name)) ;
			} 
			if (step instanceof CONSTRUCTOR) {
				CONSTRUCTOR c = (CONSTRUCTOR) step ;
				return (c.con.getDeclaringClass().getSimpleName().equals(name)) ;
			}
			return false ;
		} ;
	}
	
	static public Predicate<PreState> ofUpdateOnField(String name) {
		return state -> {
			STEP step = state.step ;
			if (step instanceof UPDATE_FIELD) {
				UPDATE_FIELD f = (UPDATE_FIELD) step ;		
				return (f.field.getName().equals(name)) ;
			} 
			return false ;
		} ;
	}
		
}
