package Sequenic.T3;

import java.io.* ;
import java.lang.reflect.*;
import java.util.* ;
import java.util.Map.Entry;
import java.util.concurrent.* ;
import java.util.logging.*;

import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Reflection.ClassesScanner;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.MyEnum;
import Sequenic.T3.utils.SomeObject;


/**
 * This class essentially implements a mapping between classes, and how their instance
 * can be created. These are used for creating values needed as parameters in our
 * test-sequences. These values are in themselves part of the testing-scope, so we will
 * just limit ourselves to public-way of creating thoses instances.
 *
 * A class is called publicly instantiable if it is not an interface,
 * and it has either a public constructor, or a public creation-method. A creation-method
 * of a class C is a static method of C that returns an instance of C, and takes no parameter
 * of type C. Note that an abstract class can be publicly instantiable if it provides
 * a concrete creation method.
 *
 * We will here maintain a map between classes that are not publicly instantiable, and their
 * publicly instantiable implementations.
 *
 */
public class ImplementationMap {


    public static Class[] standardKnownInstantiableClasses() {
        Class[] classes = {
                LinkedList.class,
                HashSet.class,
                HashMap.class,
                SomeObject.class,
                Integer.class,  Long.class, Byte.class, Short.class, Float.class, Double.class, Boolean.class,
                MyEnum.class,
                FileWriter.class,
                FileReader.class,
                StringBuilder.class,
                StringWriter.class,
                org.xml.sax.helpers.AttributesImpl.class
        }  ;
        return classes ;
    }

    /**
     * The set of instantiable classes known to this map. We use a map to encode
     * the set; but it is a concurrent hash-map to facilitate concurrent accesses.
     * When C is in the set, this is represented by (C,i) being in the map, not
     * any non-null integer.
     */
    protected ConcurrentHashMap<Class,Integer> knownInstantiableClasses ;
    
    public List<Class> getAllKnownInstantiableClasses() {
    	List<Class> result = new LinkedList<Class>() ;
    	knownInstantiableClasses.forEach((C,i)-> result.add(C));
    	return result ;
    }
 
    /**
     * This maintains a mapping from uninstantiable classes to their instantiable
     * set of implementations. For efficiency, the map will also be updated on-the-fly.
     * We are using concurrent hash-map. The list of implementations (for each uninstantiabl
     * class) will be represented by a 2nd level concurrent hash-map. The integer in the
     * 2nd map is just a dummy; any int will do.
     */
    protected ConcurrentHashMap<Class, ConcurrentHashMap<Class,Integer>> impsMap  ;

    /**
     * Constructor. It will pre-populate the map the given classes, and by
     * scanning the given directory for classes.
     */
    public ImplementationMap(String[] classesDirs, Class ... someKnownClasses) {

        knownInstantiableClasses = new ConcurrentHashMap<Class,Integer>(150) ;

        // populate with the given classes first:
        for (Class C : someKnownClasses) {
            //System.out.println("** adding " + C.getName());
            knownInstantiableClasses.put(C,1) ;
        }
        
        for (Class C : standardKnownInstantiableClasses()) {
            //System.out.println("** adding " + C.getName());
            knownInstantiableClasses.put(C,1) ;
        }

        // further populate by scanning:
        ClassesScanner scanner = new ClassesScanner(classesDirs) ;
        scanner.scan();
        for (Class C : scanner.getScanned()) {
            if (isPubliclyInstantiable(C)) knownInstantiableClasses.put(C,1) ;
        }

        // construct implementation map:
        impsMap = new ConcurrentHashMap<Class,ConcurrentHashMap<Class,Integer>>(150)  ;

        for (Class C : scanner.getScanned())  {
            // if C is already instantiable, then it does not need mapping
            if (isPubliclyInstantiable(C)) continue ;
            // C is unintsantiable; add it to the map:
            final ConcurrentHashMap<Class,Integer> implementations = new ConcurrentHashMap<Class,Integer>(10) ;

            knownInstantiableClasses.forEachKey(Long.MAX_VALUE,
               D ->
               { if (D != C && C.isAssignableFrom(D)) implementations.put(D,1) ; }
            ) ;
            impsMap.put(C,implementations) ;
        }
        
        //for (Class C : this.getAllKnownInstantiableClasses()) System.out.println("**>> " + C) ;

    }

    /**
     * Return the creator-methods of the class C.
     */
    public static List<Method> getPublicCreatorMethods(Class C) {
        // if (C.isInterface() ||  Modifier.isAbstract(C.getModifiers())) return null ;
        if (C.isInterface()) return null ;
        List<Method> creatorMethods =  new LinkedList<Method>() ;
        Method[] declaredmethods = null ;
        try {
            // this can fail :(
            declaredmethods = C.getDeclaredMethods() ;
        }
        catch (Throwable e) { return creatorMethods ; }
        for (Method m : declaredmethods) {
            int mod = m.getModifiers() ;
            if(! Modifier.isStatic(mod) ||  ! Modifier.isPublic(mod) || Modifier.isAbstract(mod)) continue ;
            if(!(m.getReturnType() == C)) continue ;
            boolean takeC = false ;
            for (Class D : m.getParameterTypes())
                if (D==C) { takeC = true ; break ; }
            if (! takeC) {
                creatorMethods.add(m) ;
            }
        }
        return creatorMethods ;
    }


    /**
     * Check if a given class has a public constructor or creator method.
     */
    public static boolean isPubliclyInstantiable(Class C) {
        // if (C.isInterface() ||  Modifier.isAbstract(C.getModifiers()))  return false ;
        if (C.isInterface()) return false ;
        // note that this will only return PUBLIC constructors!
        //System.out.println("**xx " + C.getName());
        if (Modifier.isAbstract(C.getModifiers())) return ! getPublicCreatorMethods(C).isEmpty() ;
        try {
            return C.getConstructors().length > 0 || ! getPublicCreatorMethods(C).isEmpty()  ;
        }
        catch (Throwable e) {
            // if for some reason we simply can't get the class the it is not instantiable.
            return false ;
        }
    }

    // given a concrete and instantiable class D, look for classes in the map
    // that still have empty implementations, and check if D can be added:
    private void improveMap(Class D) {
        impsMap.forEach(
          (C,Imps) ->
          { if (C.isAssignableFrom(D) && Imps.isEmpty()) Imps.put(D,1) ; }
        );
    }

    /**
     * Inquiry for known concrete and instantiable implementations of a given class C.
     * If C itself is already such a class, then a singleton containing only C will be
     * returned.
     *
     * Else we will retrieve the imps from the map. If no imp can be found, then we will
     * search in the known classes that we maintain. The result is returned, and also
     * added to the map.
     *
     * Additionally if C is a class which is not known, it will be added, and imps are
     * searched from the list of known classes.
     */
      public List<Class> getImps(Class C) {

        final List<Class> imps = new LinkedList<Class>() ;
        if (isPubliclyInstantiable(C)) {
            //System.out.println("** Marked as concrete and instantiable: " + C.getName());
            if (! knownInstantiableClasses.contains(C)) {
                knownInstantiableClasses.put(C,1) ;
                improveMap(C);
            }
            imps.add(C) ;
            return imps ;
        }
        // else C is not instantiable
        ConcurrentHashMap<Class,Integer> imps_ = impsMap.get(C) ;
        if (imps_ != null) {
            imps_.forEach(
               (I,x) -> { imps.add(I) ; }
            );
            return imps ;
        }
        // if imps_ is null, then we have not check for C's implementations; so, let's do so:
        final ConcurrentHashMap<Class,Integer> imps2 =  new ConcurrentHashMap<Class,Integer>() ;
        knownInstantiableClasses.forEach(
           (I,x) ->  {
               if (C.isAssignableFrom(I)) {
                   imps2.put(I,1) ;  imps.add(I) ;
               }
           }
        );
       // updating the implementation map:
       impsMap.put(C,imps2) ;
       // now, return the implementations
       return imps ;
    }

      
    /**
     * As getImps, but returns only one implementation-class, chosen randomly.
     * If there is none, it will return null.
     */
    public Class getImp1(Class C) {
    	List<Class> imps = getImps(C) ;
    	if (imps == null || imps.size()==0) return null ;
    	int k = T3Random.getRnd().nextInt(imps.size()) ;
    	return imps.get(k) ;	  
    }
      
    
    public void print() {
    	System.out.println("** Known instantiable classes:") ;
    	int k = 0 ;
    	for (Entry<Class,Integer> e : knownInstantiableClasses.entrySet()) {
    		if (e.getValue() != null)  {
    			System.out.println("     " + e.getKey().getName()) ;
    			k++ ;
    		}
    	}
    	System.out.println("** Total: " + k) ;
    	
    	System.out.println("** Mapped classes: " + impsMap.size()) ;
    	k = 0 ;
    	for (Entry<Class, ConcurrentHashMap<Class,Integer>> e : impsMap.entrySet()) {
    		System.out.print("     " + e.getKey().getName() + " --> ") ;
    	    k++ ;
    	    int j = 0 ;
    		for (Class C : e.getValue().keySet()) {
    			if (j>0) System.out.print(",") ;
    		    System.out.print("" + C.getName()) ;
    		    j++ ;
    		}
    		System.out.println("") ;
    	}
    	System.out.println("** Total: " + k) ;
    	
    	
    }
    
    public static void main(String[] args) throws IOException {
        String[] dirs = {"D:/workshop/T3workspace/T3_v0/out/production/T3_v0"} ;
        ImplementationMap IM = new ImplementationMap(dirs) ;
        List<Class> imps = IM.getImps(REF.class) ;
        for (Class I : imps) System.out.println("REF's imp: " + I) ;
        imps = IM.getImps(STEP.class) ;
        for (Class I : imps) System.out.println("STEP's imp: " + I) ;
        imps = IM.getImps(Person.class) ;
        for (Class I : imps) System.out.println("Person's imp: " + I) ;
        imps = IM.getImps(Collection.class) ;
        for (Class I : imps) System.out.println("Collection's imp: " + I) ;
        imps = IM.getImps(File.class) ;
        for (Class I : imps) System.out.println("File's imp: " + I) ;
        System.out.println("DONE")  ;
        IM.print();
    }


}
