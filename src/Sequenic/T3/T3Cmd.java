package Sequenic.T3;

import org.apache.commons.cli.* ;

import java.io.FileOutputStream;
import java.util.List;
import java.util.function.Function;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.Value.ValueMG;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.FUN;
import Sequenic.T3.utils.Pair;
import static Sequenic.T3.Generator.GenCombinators.* ;

import java.util.logging.*;

/**
 * T3 command line tool. Using this will not allow you to use a custom value generator.
 */
public class T3Cmd {

    Option o_help = new Option("help", "help",false,"Print this message.") ;
    Option o_otherpckg = new Option("tfop", "tfop", false, "When set, we assume to test from a different package.") ;
    Option o_savedir = new Option("d","savedir",true,"Save the generated suite in this directory.") ;
    Option o_scandir = new Option("sd","scandir",true,"Directory to scan for classes.") ;
    
    Option o_logging   = new Option("lf","logfile",true,"To echo messages to a log-file.") ;
    Option o_reportOut = new Option("rs","reportStream",true,"To send report to a file.") ;
    Option o_reportVerbosity = new Option("rv","reportVerbosity",true,"Verbosity level of the report.") ;
    Option o_seqOut    = new Option("tps","tracePrintStream",true,"To send sequence-prints to a file.") ;
    
    Option o_maxLevelOfObjectsNesting = new Option("obn","objectsNesting",true,"Maximum object-depth that T3 generates. Default 4.") ;
    Option o_collection = new Option("co","collection",true,"Maximum size of collections and arrays that T3 generates. Default 3.") ;
    Option o_maxNumberOfStepRetry =  new Option("str","stepretry",true,"Maximum number that each step is retried. Default 30.") ;
    Option o_maxNumberOfSeqRetry = new Option("sqr","seqretry",true,"Maximum number that each sequence is retried. Default 5.") ;
    Option o_maxPrefixLength = new Option("pl", "prefixlength",true,"Maximum prefix length of each sequence. Default 8.") ;
    Option o_variablePrefix = new Option("vp", "variprefix", false, "If set, T3 will generate variable legth prefixes, up to the specified maximum.") ;
    Option o_dropDuplicates = new Option("dd","dropDuplicates",false,"If set, will drop duplicate sequences from the generated suite.") ;
    Option o_maxSuffixLength = new Option("sl", "suffixlength",true,"Maximum suffix length of each sequence. Default 3.") ;
    Option o_samplesMultiplier = new Option("ms","msamples",true,"Multiplier on number of samples to collect per goal. Default 4.0.") ;
    Option o_splitSuite = new Option("ss", "splitsuite",true,"Split the generated suite to this number of smaller suites. Default 1.") ;
    Option o_regression = new Option("reg", "regressionmode", false, "If set, T3 will generate a suite for regression test; the target class is assumed correct.") ;
    Option o_supressInjectOracle = new Option("norc", "nooracle", false, "If set, T3 will not inject oracles in when ran in the regression mode.") ;
    Option o_ADT = new Option("adt", "adtOption", true, "true to create only ADT suite, false for only non-ADT. Both if left unset (default).") ;
    
    
    Option o_Core = new Option("core", "core", true, "To specify the number of cores.") ;
    Option o_dropGood = new Option("dg", "dropgood", false, "If set, non-violating traces will be dropped.")  ;
    Option o_fieldUpdateProb = new Option("fup", "fieldupdate", true, "The probability to do a field update. Default is 0.1.") ;
    Option o_customValGenerator = new Option("cvg","customvalgen",true,"A class specifying a custom values generator.") ;
    
    Option o_timeout = new Option("to","timeout",true,"When set, specifies time out in ms.") ;
    Option o_replay1 = new Option("sex","showexc",false,"When set, will show the first exception throwing execution.") ;
    
    
    Options options = new Options();

    public FileHandler loghandler = null ;
    public long startingTime ;
    public T3SuiteGenAPI T3API = null ;
    public String dirToSaveTrace = null ;
    
    private CommandLine cmd ;

    private T3Cmd() throws Exception {
      startingTime = System.currentTimeMillis() ;
      prepOptions();
    }

    private void prepOptions() {
        options.addOption(o_help) ;
        options.addOption(o_otherpckg) ;
        o_savedir.setArgName("dir"); options.addOption(o_savedir) ;
        o_maxLevelOfObjectsNesting.setArgName("int"); options.addOption(o_maxLevelOfObjectsNesting)  ;
        o_scandir.setArgName("dir"); options.addOption(o_scandir) ;
        o_logging.setArgName("file"); options.addOption(o_logging) ;
        o_reportOut.setArgName("file"); options.addOption(o_reportOut) ;
        o_reportVerbosity.setArgName("int"); options.addOption(o_reportVerbosity) ;
        o_seqOut.setArgName("file"); options.addOption(o_seqOut) ;
        o_collection.setArgName("int"); options.addOption(o_collection) ;
        o_maxNumberOfStepRetry.setArgName("int"); options.addOption(o_maxNumberOfStepRetry) ;
        o_maxNumberOfSeqRetry.setArgName("int"); options.addOption(o_maxNumberOfSeqRetry) ;
        o_maxPrefixLength.setArgName("int") ; options.addOption(o_maxPrefixLength) ;
        options.addOption(o_variablePrefix) ;
        options.addOption(o_dropDuplicates) ;
        o_maxSuffixLength.setArgName("int") ; options.addOption(o_maxSuffixLength) ;
        o_samplesMultiplier.setArgName("float"); options.addOption(o_samplesMultiplier) ;
        o_splitSuite.setArgName("int") ;  options.addOption(o_splitSuite) ;
        options.addOption(o_regression) ;
        options.addOption(o_supressInjectOracle) ;
        o_ADT.setArgName("boolean") ;
        options.addOption(o_ADT) ;
        o_Core.setArgName("int") ; options.addOption(o_Core)  ;
        options.addOption(o_dropGood)  ;
        o_fieldUpdateProb.setArgName("float"); options.addOption(o_fieldUpdateProb) ;
        o_customValGenerator.setArgName("class-name");  options.addOption(o_customValGenerator) ;
        o_timeout.setArgName("int"); options.addOption(o_timeout) ;
        options.addOption(o_replay1) ;
    }


    private String getTarget() {
        String[] residual = cmd.getArgs() ;
        for (int k=0; k<residual.length; k++)
            if (residual[k] != null && !residual[k].isEmpty() && !residual[k].equals(" "))
                return residual[k] ;
        return null ;
    }

    private void printHelpInfo() {
        HelpFormatter formatter = new HelpFormatter();
        String header = "T3, automatic testing tool for Java. v0" ;
        String footer = "(c) author: Wishnu Prasetya" ;
        formatter.printHelp(100,"java -cp <T3jar><:otherpath>* Sequenic.T3.T3cmd <options>* <targetclass>", header, options, footer) ;
    }


    private void configure(Class CUT) throws Exception {
    	        
    	Config config = new Config() ;
        config.CUT = CUT ; 
       
        if (cmd.hasOption(o_logging.getLongOpt())) {
            String logname = cmd.getOptionValue(o_logging.getLongOpt())  ;
            loghandler = new FileHandler("" + logname + "_" + System.currentTimeMillis() + ".txt") ;
            // get the root logger
            Logger logger =  Logger.getLogger(CONSTANTS.T3loggerName) ;
            if (logger.getParent() != null && logger.getParent()!=logger) logger = logger.getParent()  ;
            logger.addHandler(loghandler);
            loghandler.setLevel(Level.FINE);
            loghandler.setFormatter(new SimpleFormatter());

            //for (Handler H : logger.getHandlers()) {
            //    System.out.println(">>> log-handler: " + H.getClass().getName()) ;
            //}
        }
        
        if (cmd.hasOption(o_savedir.getLongOpt())) {
        	dirToSaveTrace = cmd.getOptionValue(o_savedir.getLongOpt()) ;
        }

            
        if (cmd.hasOption(o_otherpckg.getLongOpt())) {
            config.assumeClientInTheSamePackage = false ;
        }
        
        if (cmd.hasOption(o_scandir.getLongOpt())) {
            config.dirsToClasses = cmd.getOptionValues(o_scandir.getLongOpt()) ;
        }
        else config.dirsToClasses = new String[0] ;
        
        if (cmd.hasOption(o_reportOut.getLongOpt())) {
        	String streamName = cmd.getOptionValue(o_reportOut.getLongOpt()) ;
        	if (streamName.equals("System.err")) {
        		//System.out.println("**>> streamName = " + streamName) ;
        		config.reportOut = System.err ;
        	}
        	else config.reportOut = new FileOutputStream(streamName) ;
        }
        
        if (cmd.hasOption(o_reportVerbosity.getLongOpt())) {
        	config.reportVerbosity =  Integer.parseInt(cmd.getOptionValue(o_reportVerbosity.getLongOpt())) ;
        }
        
        
        
        if (cmd.hasOption(o_seqOut.getLongOpt())) {
        	String streamName = cmd.getOptionValue(o_seqOut.getLongOpt()) ;
        	if (streamName.equals("System.err")) config.sequencePrintOut = System.err ;
        	else config.sequencePrintOut = new FileOutputStream(streamName) ;
        }

        if (cmd.hasOption(o_maxLevelOfObjectsNesting.getLongOpt()))
            config.maxLevelOfObjectsNesting = Integer.parseInt(cmd.getOptionValue(o_maxLevelOfObjectsNesting.getLongOpt())) ;
        if (cmd.hasOption(o_collection.getLongOpt()))
            config.maxCollectionSize = Integer.parseInt(cmd.getOptionValue(o_collection.getLongOpt())) ;
        if (cmd.hasOption(o_maxNumberOfStepRetry.getLongOpt()))
            config.maxNumberOfStepRetry = Integer.parseInt(cmd.getOptionValue(o_maxNumberOfStepRetry.getLongOpt()));
        if (cmd.hasOption(o_maxNumberOfSeqRetry.getLongOpt()))
            config.maxNumberOfSeqRetry = Integer.parseInt(cmd.getOptionValue(o_maxNumberOfSeqRetry.getLongOpt())) ;
        config.maximizePrefix = ! cmd.hasOption(o_variablePrefix.getLongOpt()) ;
        config.dropDuplicates = cmd.hasOption(o_dropDuplicates.getLongOpt()) ;   
        if (cmd.hasOption(o_maxPrefixLength.getLongOpt()))
            config.maxPrefixLength = Integer.parseInt(cmd.getOptionValue(o_maxPrefixLength.getLongOpt())) ;
        if (cmd.hasOption(o_maxSuffixLength.getLongOpt()))
            config.maxSuffixLength = Integer.parseInt(cmd.getOptionValue(o_maxSuffixLength.getLongOpt())) ;
        if (cmd.hasOption(o_samplesMultiplier.getLongOpt()))
            config.suiteSizeMultiplierPerGoal =  Double.parseDouble(cmd.getOptionValue(o_samplesMultiplier.getLongOpt())) ;
        if (cmd.hasOption(o_fieldUpdateProb.getLongOpt()))
            config.fieldUpdateProbability =  Double.parseDouble(cmd.getOptionValue(o_fieldUpdateProb.getLongOpt())) ;
        if (cmd.hasOption(o_splitSuite.getLongOpt()))
            config.splitSuite = Integer.parseInt(cmd.getOptionValue(o_splitSuite.getLongOpt())) ;
        config.regressionMode = cmd.hasOption(o_regression.getLongOpt()) ;
        config.injectOracles  = ! cmd.hasOption(o_supressInjectOracle.getLongOpt()) ;
        config.keepOnlyRedTraces = cmd.hasOption(o_dropGood.getLongOpt()) ;
        if (cmd.hasOption(o_Core.getLongOpt()))
            config.numberOfCores = Math.max(1,Integer.parseInt(cmd.getOptionValue(o_Core.getLongOpt()))) ;
    
       // hooking custom-value generator, if specified        
        Function<ImplementationMap,Function<Pool,Generator<PARAM,STEP>>> valMetaGen = null ;
        if (cmd.hasOption(o_customValGenerator.getLongOpt())) {
        	try {
        	   Class customValGenClass = Class.forName(cmd.getOptionValue(o_customValGenerator.getLongOpt())) ;
        	   Generator<PARAM,STEP> customGen = (Generator<PARAM,STEP>) customValGenClass.getDeclaredField("myvalgen").get(null) ;
        	   valMetaGen = impmap -> {
        		   ValueMG defaultValMetaGen = new ValueMG(config.maxLevelOfObjectsNesting, 
        				                                   config.maxCollectionSize, 
        				                                   impmap) ;  
        		   return pool -> FUN.recbind(g -> FirstOf(customGen, defaultValMetaGen.gen1open(pool,g))) ;
        	   } ;
        	}
        	catch(Exception e) {
        		Logger.getLogger(CONSTANTS.T3loggerName).warning("** T3 fails to hook the given custom value generator.");
        	}	
        }
        
        T3API = new T3SuiteGenAPI(valMetaGen,config) ;
    }

    /**
     * Generate, save, and report the suites.
     */
    private SUITE suites() throws Exception {
        SUITE  ADT = null ;
    	SUITE  nonADT = null ;
    	
    	boolean mkADT = true ;
    	boolean mkNonADT = true ;
    	
    	if (cmd.hasOption(o_ADT.getLongOpt())) {
        	boolean b = Boolean.parseBoolean(cmd.getOptionValue(o_ADT.getLongOpt())) ;
        	if (b) mkNonADT = false ; else mkADT = false ;
        }
    	
    	if (mkADT) {
    	   List<SUITE>  ADTsuites =  T3API.suites(true,dirToSaveTrace) ;
    	   ADT = new SUITE(T3API.config.CUT.getName()) ;
           for (SUITE T : ADTsuites) ADT = SUITE.union(ADT,T) ;
           T3API.reportCoverage(ADT);
    	}
        if (mkNonADT) {   
           List<SUITE>  nonADTsuites =  T3API.suites(false,dirToSaveTrace) ;
           nonADT = new SUITE(T3API.config.CUT.getName()) ;
           for (SUITE T : nonADTsuites) nonADT = SUITE.union(nonADT,T) ;
           T3API.reportCoverage(nonADT);
        }
        if (!mkADT)    return nonADT ;
        if (!mkNonADT) return ADT ;
    	return SUITE.union(ADT,nonADT) ;
    }
    
    /**
     * Run T3 
     */
    private void runT3(String[] args) throws Exception {
    	//Logger.getLogger(CONSTANTS.T3loggerName).info("** T3 runs...") ;
    	CommandLineParser parser = new GnuParser();
        cmd = parser.parse(options, args);
        
    	if (cmd.getArgs().length==0 || cmd.hasOption(o_help.getLongOpt())) {
            printHelpInfo() ;
            return ;
        }
        String CUTname = getTarget() ;
        if (CUTname==null) { printHelpInfo() ; return ; }
    	configure(Class.forName(CUTname)) ;
    	
    	 SUITE S ;
    	//System.out.println(">>> About to save the suite(s)...") ;
    	if (cmd.hasOption(o_timeout.getLongOpt())) {
            long t = Long.parseLong(cmd.getOptionValue(o_timeout.getLongOpt())) ;
            TimeOut time = new TimeOut(t) ;
            time.start() ;
            try { S = suites() ; }
            finally {
              //exec(" --scandir=D:/workshop/EclipseT3workspace/T3_v0/bin  Sequenic.T3.Examples.Friends.Person") ;
              //System.out.println(">>> about to interrupt timer...") ;
              time.interrupt();
              Logger.getLogger(CONSTANTS.T3loggerName).info("** T3 times out! (" + time + " ms)");
              if (loghandler != null) loghandler.flush();
            }
        }
        else S = suites() ;
        
        if (cmd.hasOption(o_replay1.getLongOpt())) {       
           T3API.replay(S);
        }
           
        Long runtime = System.currentTimeMillis() - startingTime ;
        Logger.getLogger(CONSTANTS.T3loggerName).info(">> T3 runtime = " + runtime + "ms");
        if (loghandler != null) loghandler.flush();
    }

      
    static class TimeOut extends Thread {
    	
     	 long TIMEOUT = 180*1000 ; // set time out to this much milli-secs
    
     	 TimeOut(long timeout) {
     		 this.TIMEOUT = timeout ;
     	 }
     	 
         public void run() {
            try {
            	Logger.getLogger(CONSTANTS.T3loggerName).info("** T3 sets a timer...");
                sleep(TIMEOUT);
                Logger.getLogger(CONSTANTS.T3loggerName).warning("** T3 has timeout! Performing system exit...");
                System.exit(-1);
            }
            catch (Exception e) {
                // the sleep was interrupted... ok.
            }
         }
    }

    /**
     * Command-line entry point to run T3.
     */
    public static void main(String[] args) throws Exception {
        new T3Cmd(). runT3(args) ;   
    }

    // Access to T3-tool through APIs, below
    
    /**
     * API. As main, but passing all options as a single string. 
     */
    public static void main(String optionsString) throws Exception {
        String[] args = optionsString.split("( )+") ;
        //for (int i=0; i<args.length; i++)
        //    System.out.println("args[" + i + "] = " + args[i]) ;
        main(args) ;
    }
    

}
