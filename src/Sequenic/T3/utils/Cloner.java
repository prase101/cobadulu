package Sequenic.T3.utils ;
import java.io.* ;
import java.util.* ;
import java.lang.reflect.* ;
//import Sequenic.T2.examples.* ;

/** 
 *   Utility to do deep cloning of an object.
 */
public class Cloner {

	/**
	 * Return a deep-clone of an object. Currently implemented via
	 * serialization; thus assuming that the cloned object is
	 * serializable.
	 */
	static public <T> T clone(T o) 
		throws IOException, 
			   ClassNotFoundException 
	{
		ByteArrayOutputStream outstream = new ByteArrayOutputStream() ;
		ObjectOutputStream cout = new ObjectOutputStream(outstream) ;
		cout.writeObject(o) ; 
		ByteArrayInputStream instream = new ByteArrayInputStream(outstream.toByteArray()) ;
		ObjectInputStream cin = new ObjectInputStream(instream) ;
		T copy = (T) cin.readObject() ;
		return copy ;

	}
	
}
