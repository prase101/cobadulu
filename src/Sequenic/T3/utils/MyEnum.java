package Sequenic.T3.utils;

/**
 * Just a simple enumeration for the purpose of generating instance of Enum.
 */
public enum MyEnum {
   ZERO, ONE, TWO
}
