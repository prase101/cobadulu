/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.utils;

import java.util.function.Function;

/**
 * In Java-8 it does not seem to be possible to construct a recursive function
 * through a lambda-expression. As a work-around an instance of this class can
 * be passed on as a place holder to the reference to the top level function
 * (which we want to recurse on).
 */
public class FUN<F> {

    public F fun = null ;
    
    static public <F> F recbind(Function<FUN<F>,F> foo) {
    	FUN<F> rec = new FUN<F>() ;
    	F g = foo.apply(rec) ;
    	rec.fun = g ;
    	return g ;
    }

}
