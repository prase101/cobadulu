package Sequenic.T3.utils;

import java.util.*;

/**
 * Provides a bunch of methods to format a string.
 */
public class StringFormater {

    static public String mkSpace(int n) {
        String s = "";
        for (; 0 < n; n--) {
            s = s + " ";
        }
        return s;
    }

    /**
     * Pad the string on the left to the given width.
     */
    static public String padLeft(String s, int N) {
        return (mkSpace(N - s.length()) + s);
    }

    /**
     * Pad the string on the right to the given width.
     */
    static public String padRight(String s, int N) {
        return (s + mkSpace(N - s.length()));
    }

    /**
     * Reformat s to possibly multi lines such that each is at most of
     * the specified width. The formatting is token-based.
     */
    static public List<String> alignLeft(String s, int width) {
        List<String> result = new LinkedList<String>();
        StringTokenizer scanner = new StringTokenizer(s, " \t\n", false);
        int k = 0;
        String tok = null;
        String line = "";
        if (scanner.hasMoreElements()) {
            line = scanner.nextToken();
            k = line.length();
        }
        while (scanner.hasMoreElements()) {
            tok = scanner.nextToken();
            //System.out.println(">: " + k) ;
            if (k < width) {
                line = line + " " + tok;
                k = k + tok.length() + 1;
            } else {
                result.add(line);
                line = tok;
                k = line.length();
            }
        }
        result.add(line);
        return result;
    }

    static private String indent(String[] lines, int n) {
        String space = mkSpace(n) ;
        String result = "" ;
        int i=0 ;
        int N = lines.length  ;
        if (N>0) { result = space + lines[0] ; i++ ; }
        else return result ;
        while (i<N) {
            result = result + "\n" + space + lines[i] ;
            i++ ;
        }
        return result ;
    }
    
    /**
     * As the other alignLeft, but will also indent the lines and return 
     * a single string. 
     */
    static public String alignLeft(String s, int width, int indent) {
        List<String> lines = alignLeft(s, width);
        String[] lines_ = new String[lines.size()] ;
        for (int i=0; i<lines_.length; i++) lines_[i] = lines.get(i) ; 
        return indent(lines_, indent) ;
    }
    
    
    /**
     * Indent every 'lines' in the given string n spaces.
     */
    public static String indent(String s, int n) {
        String[] lines = s.split("\n") ;
        return indent(lines,n) ;
    }
    
    /**
     * As the other indent, but does not indent the first line.
     */
    public static String indentButFirst(String s, int n) {
        String[] lines = s.split("\n") ;
        if (lines.length==0) return "" ;
        if (lines.length==1) return lines[0] ;
        String firstline = lines[0] ;
        String[] lines1 = new String[lines.length-1] ;
        for (int i=1; i<lines.length; i++) {
            lines1[i-1] = lines[i] ;
            //System.out.println("**" + lines[i-1]) ;
        }
        return firstline + "\n" + indent(lines1,n) ;
    }
    
    // tests
    public static void main(String [] args){
    	String s = indentButFirst("foo(12345,\n123,\n123)",3) ;
    	System.out.println(s) ;
    	String t = indentButFirst("foo(5678,\n0,\n" + s + ")", 3) ;
    	System.out.println(t) ;
    	
    }
    
}
