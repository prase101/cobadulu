package Sequenic.T3.utils;

public class Unit {

    private static Unit unit ;

    public static synchronized Unit get() {
        if (unit == null) unit = new Unit() ;
        return unit ;
    }
}
