package Sequenic.T3.utils;

public class Pair<T,U> {
	
	public T fst ;
	public U snd ;
	
	public Pair(T a, U b) { fst = a ; snd = b ; }

}
