package Sequenic.T3.Examples.Staticx;

public class StringUtils {

    public static int midPoint(String s) {
        return s.length() / 2 ;
    }

    public static String concat(String s1, String s2) {
        return "" + s2 + s2 ;
    }

    public static String reverse(String s) {
        String r = "" ;
        char[] s_ = s.toCharArray() ;
        for (int k=0; k < s_.length ; k++) {
            r +=  s_[s_.length - k -1] ;
        }
        return r ;
    }

    public static String drop(String s, int k) {
    	if (k<0) throw new IllegalArgumentException() ;
        return s.substring(k) ;
    }

    public static String take(String s, int k) {
    	if (k<0) throw new IllegalArgumentException() ;
        return s.substring(0,k) ;
    }

}
