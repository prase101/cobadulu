package Sequenic.T3.Examples.Cycles;

import java.util.*;

public class Cycle<T> {

    T item  ;
    Cycle<T> next ;

    public Cycle() {}

    public Cycle(T x) {
        item = x ;
        next = this ;
    }

    static int maxSize = 100 ;

    public boolean isCycle() {
        List<Cycle<T>> visited = new LinkedList<Cycle<T>>() ;
        int k = 0 ;
        Cycle now = this ;
        while (! visited.contains(now) && k < maxSize) {
            if (now.next == null) return false ;
            visited.add(now) ;
            now = now.next ;
        }
        return  k<maxSize ;
    }


}
