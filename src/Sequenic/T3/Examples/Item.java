package Sequenic.T3.Examples;

public class Item {
    String name  ;
    int    price ;
    private String code  ;
    
    public Item(String name) {
    	this.name = name ;
    	this.price = 100 ;
    	code = null ;
    }
    
    public void incPrice(int x) { price += x ; }
    public String setCode(String code) { this.code = code; return this.code ; }
    public String getCode() { return code ; }
    public int getPrice() { return price ; }
    public String resetCode() { code = null ; return code ; }
    
}
