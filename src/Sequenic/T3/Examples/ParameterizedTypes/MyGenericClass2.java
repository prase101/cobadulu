package Sequenic.T3.Examples.ParameterizedTypes;

import java.util.*;

/**
 * These parameters have actually no solution. Note that if we try to
 * get e.g. the type variable I through reflection, and call getBounds
 * on I, it will go stack overflow, probably because it naively just
 * keeps expanding the upper bound. 
 */
public class MyGenericClass2 <
    I extends Set<J>, 
    J extends Set<I>
    >
{

}
