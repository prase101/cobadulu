package Sequenic.T3.Examples.ParameterizedTypes;

import java.util.*;
import Sequenic.T3.Examples.Abstractx.* ;

public class MyGenericMethods {
	
	static <T> void f1(T x) { }
	
	static <U extends Vehicle> void f1b(U x, Vehicle y) { }
	
	static <T, U> void f2(T x, U y) { }
	
	static <T, U extends T> void f3(T x, U y) { }
	
	static <T, U extends List<T>> void f3(T x, U y) { }
	
	// when compiled from command line gives type error:
	// static <T, U extends Set<T> & List<T>> void f4(T x, U y) { }
	
	static <T, U extends Collection<T> & List<T>> void f5(T x, U y) { }
	
	static <T> void f6(T x, Collection<? extends Integer> y) { }
	
	static <T> void f7(Collection<? super List<T>> y) { }
	
	static <T> void f8(Collection<? super LinkedList<T>> y) { }
	

}
