package Sequenic.T3.Examples.CustomInputGenerator;

import java.util.function.Function;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.*;
import static Sequenic.T3.Generator.GenCombinators.* ;
import static Sequenic.T3.Generator.Value.ValueMGCombinators.* ;

public class CustomInputGenerator {
	
	static Person.Email genEmail(int k) {
		switch (k) { 
		case 0 : return new Person.Email("root") ;
		case 1 : return new Person.Email("annaz") ;
		default : return new Person.Email("guest") ;
		}
	}
	
	public static Generator<PARAM,STEP> myvalgen =
	     FirstOf(
            Integer(OneOf(17,18,60)).If(hasParamName("age")),
            Integer(OneOf(-1,0,1)).If(hasParamName("code")),
            String(OneOf("anna","bob")).If(hasParamName("name")),
            String(OneOf("anna@gmail.com","k1@ppp")).If(hasParamName("email")),
            Apply(CustomInputGenerator.class,"genEmail",OneOf(0,1,2)).If(hasClass(Person.Email.class).and(hasParamName("email")))
         ) ;
            
            
}
