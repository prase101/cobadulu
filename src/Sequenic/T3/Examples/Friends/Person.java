package Sequenic.T3.Examples.Friends;

import java.util.*;

public class Person {

    public String name ;
    public List<Email> emails ;
    List<Person> friends ;
    
    
    public Person(String name, Email email) {
        this.name = name ;
        emails =  new LinkedList<Email>() ;
        friends = new LinkedList<Person>() ;
        emails.add(email) ;
        email.mainemail = true ;
        //System.out.println(">> Person(" + name + "," + email + ")") ;
    }
   

    
    public Person(String name, Email ... emails) {
    	this.name = name ;
        this.emails =  new LinkedList<Email>() ;
        friends = new LinkedList<Person>() ;
        for (int i=0; i<emails.length; i++) {
            this.emails.add(emails[i]) ;
            if (i==0) emails[i].mainemail = true ;
        }
        //System.out.println(">> Person(" + name + ", array of emails)") ;
    }
    
    
    public Person(String name, Set<Email> emails) {
        this.emails =  new LinkedList<Email>() ;
        for (Email e : emails) {
        	// introduced bug --> below, it should be this.emails instead:
            emails.add(e) ;
        }
        //System.out.println(">> Person(" + name + ", set of emails)") ;
    }
    
    
    
    public String getEmail() { 	
    	//System.out.println(">> getEmail") ;
    	return emails.get(0).email.toLowerCase() ;
    }
  
    public void friend(Person p) {
    	//System.out.println(">> friend(" + p + ")") ;
        friends.add(p);
        p.friends.add(this);
    }


    public void unfriend(Person p) {
    	//System.out.println(">> unfriend(" + p + ")") ;
        friends.remove(p) ;
        p.friends.remove(this) ;
    }

}
