package Sequenic.T3.Examples;

public class Coba1 {
	
	public Coba1(int x) {
	   System.out.println(">> Coba1(" + x + ")") ;
	}
	
	public Coba1() {
		System.out.println(">> Coba1()") ;
	}
	
	public void foo(String x) {
		System.out.println(">> foo(" + x + ")") ;
	}

	public void m() {
		System.out.println(">> m()") ;
	}

	
	public void ouch() {
		System.out.println(">> ouch()") ;
		//assert false ;
	}
	
}
