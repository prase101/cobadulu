package Sequenic.T3.Examples.Abstractx;

public class PaperVehicle extends Vehicle {

    public PaperVehicle() {
        super() ;
    }

    public int move(int t) {
        if (t<0) throw new IllegalArgumentException() ;
        speed = 1 ;
        x += t ;
        time += t ;
        return x ;
    }

    public int accelerate(int t) {
        move(t) ;
        return x ;
    }

    public int stop() {
        return x ;
    }

    public int dummy() { return 10 ; }
    /*
    public static void main(String[] args) {
        PaperVehicle v = new PaperVehicle() ;
        System.out.println("v.x =" + v.x) ;
    }
    */
}
