package Sequenic.T3.Examples.Abstractx;

public abstract class Vehicle {

    protected int x = 90 ;
    protected int time = 0 ;
    protected int speed = 0 ;

    abstract public int move(int t) ;

    public int accelerate(int t)  {
        if (t<0) throw new IllegalArgumentException() ;
        for (int k=0; k<t; k++) {
            move(1) ;
            speed++ ;
        }
        return x ;
    }

    abstract public int stop() ;

}
