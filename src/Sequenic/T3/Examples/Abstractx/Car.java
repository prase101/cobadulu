package Sequenic.T3.Examples.Abstractx;

public class Car extends Vehicle {

    public Car(int x) {
        super() ;
        if (x<0) throw new IllegalArgumentException() ;
        this.x = x ;
    }

    public int move(int t) {
        x += speed*t ;
        time += t ;
        return x ;
    }

    public int stop() {
        if (speed>0) speed-- ;
        move(1) ;
        return x ;
    }

}
