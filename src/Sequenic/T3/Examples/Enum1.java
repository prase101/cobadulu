package Sequenic.T3.Examples;

import java.lang.reflect.*;


public enum Enum1 {
   AA, BB, CC ;
   
   static void printTyVar(TypeVariable t) {
	   System.out.print(", " + t.getName()) ;
	   for (Type upperbound : t.getBounds())
		   System.out.print(" <= " + upperbound) ;
   }
   
   public static void main(String[] args) {
	   System.out.println("Class: " + Enum1.class) ;
	   System.out.print("tyargs: ") ;
	   for(TypeVariable t : Enum1.class.getTypeParameters()) {
		   printTyVar(t) ;
	   }
	   Method[] ms = Enum1.class.getMethods() ;
	   for (Method m : ms) {
		  System.out.print("\n>" + m.getName() + " : ");
		  Type[] targs = m.getGenericParameterTypes() ;
		  for (Type t : targs) {
			  if (t instanceof TypeVariable) printTyVar((TypeVariable) t) ;
			  else System.out.print(", " + t) ;
		  }
	   }
   }
}
