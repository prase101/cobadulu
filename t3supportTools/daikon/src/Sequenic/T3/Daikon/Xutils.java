package Sequenic.T3.Daikon;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.*;

/**
 * A copy of Sequenic.T3.SuiteUtils.Xutils, from T3interactive. :(  Its copied here
 * to avoid dependency on T3interactive.
 */
public class Xutils {
	
	private static boolean isCreationStep(STEP step) {
		if (step instanceof CONSTRUCTOR) {
			return ((CONSTRUCTOR) step).isObjectUnderTestCreationStep ;
		}
		if (step instanceof METHOD) {
			return ((METHOD) step).isObjectUnderTestCreationStep ;
		}
		return false ;
	}
		
	
	// remove INSTRUMENT
	static List<STEP> removeInstrumentation(List<STEP> seq) {
		List<STEP> seq2 =new LinkedList<STEP>() ;
		for (STEP s : seq) {
			if (! (s instanceof INSTRUMENT)) seq2.add(s) ;
		}
		return seq2 ;
	}
	
	/**
	 * Execute the k-th STEP. Returns the resulting execution-info if it succeeds,
	 * else null.
	 */
	static STEP_RT_info executeSTEP(Class CUT, 
		Pool pool, 
		STEP.PreConditionCheckPlaceHolder precondHolder, 
		STEP step,
		int k) 
	{
		try {
			STEP_RT_info info = step.exec(CUT,pool,precondHolder) ;
			if (k==0 && isCreationStep(step) && info.returnedObj != null) {
				pool.markAsObjectUnderTest(info.returnedObj) ;
				info.objectUnderTest = info.returnedObj ;
			}
			return info ;
		}
		catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			
			Logger.getLogger(CONSTANTS.T3loggerName).warning("*** A sequence failed to execute completely!" + step
					+ ", throwing " + sw.toString()
					) ;
			return null ;
		}
	}


}
