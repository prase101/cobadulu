package Sequenic.T3.Daikon;

import java.util.*;

/**
 * A representation of a program point declaration.
 */
public class ProgramPointDecl {
	
	String name ;
	String pptype ;
	List<VariableDecl> vardecls = new LinkedList<VariableDecl>() ;
	
	public ProgramPointDecl(String name, String ty) {
		this.name = name ; pptype = ty ;
	}
	
	static class VariableDecl {		
		String name ;
		String kind ;
		Integer arraydim ;
		String declType ;
		String daikonType ;
		String comparability ;
		
		public String toString() {
			StringBuffer buf = new StringBuffer() ;
			buf.append("  variable " + name) ;			
			buf.append("\n     var-kind " + kind) ;
			if (arraydim != null) buf.append("\n     array " + arraydim) ;
			buf.append("\n     dec-type " + declType) ;
			buf.append("\n     rep-type " + daikonType) ;
			buf.append("\n     comparability " + comparability) ;
			return buf.toString() ;
		}
	}
	
	
	public String toString() {
		StringBuffer buf = new StringBuffer() ;
		buf.append("ppt " + name) ;
		buf.append("\nppt-type " + pptype) ;
		for (VariableDecl var : vardecls) buf.append("\n" + var) ;
		return buf.toString() ;
	}

}
