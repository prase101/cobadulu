package Sequenic.T3.Daikon;

import Sequenic.T3.*;
import Sequenic.T3.Sequence.Datatype.*;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.*;
import java.util.* ;
import java.util.function.Function;
import java.util.logging.Logger;

/**
 * Used to execute a given test suite, either to infer Daikon invariants, or to check
 * invariants.
 */
public class T3Daikon {

    List<Sample> log ;
    int currentNonce ;
    preStateInstrumenter instrumenter ;
	
    public void reset() { log = new LinkedList<Sample>() ; currentNonce = 0 ; }
    
    public T3Daikon() {
    	this.reset() ;
    	instrumenter = new preStateInstrumenter() ;
    }
    
	private class preStateInstrumenter extends STEP.PreConditionCheckPlaceHolder {
		
		public void test(Object targetObj, STEP step, Object[] stepArgs) {
			result = true   ;
			try {
			  if  (step instanceof METHOD) {
				logMethodCallEntry((METHOD) step,targetObj,stepArgs) ; return ;
			  }
		  	  if (step instanceof CONSTRUCTOR) {
				logConstructorCallEntry((CONSTRUCTOR) step,stepArgs) ; return ; 
			  }	
			}
			catch(Exception e) { 
				Logger.getLogger(CONSTANTS.T3loggerName).warning("** Failed to reate a sample of a step's entry " + step 
						+ "\nThrowing: " + e);
			}
		}
    }
	
	static private String constructMethodName(Method m) {
		String name = m.getName() + "(" ;
		Class[] argsty = m.getParameterTypes() ;
		for (int k=0; k<argsty.length; k++) {
			if (k>0) name += "," ;
			name += argsty[k].getSimpleName() ; // ok for now using simple-name rather than the more verbose full name
		}
		name += ")" ;
		return name ;
	}
	
	static private String constructConstructorName(Constructor c) {
		String name = c.getName() + "(" ;
		Class[] argsty = c.getParameterTypes() ;
		for (int k=0; k<argsty.length; k++) {
			if (k>0) name += "," ;
			name += argsty[k].getSimpleName() ; // ok for now using simple-name rather than the more verbose full name
		}
		name += ")" ;
		return name ;
	}
	
	static private void serializeObject(String varname, Object o, int k, List<Value> log) throws Exception {
		log.add(new Value(varname,o)) ;
		if (k<=0) return ;
		if (o==null) return ;
		Class C = o.getClass() ;
		Field[] fields = C.getFields() ; // get o's public fields
		for (Field f : fields) {
			String fname = varname + "." + f.getName() ;
			serializeObject(fname, f.get(o), k-1, log) ;
		}
	}
	
	static private void serializeReceiverObj(Object receiver, List<Value> log) throws Exception {
		if (receiver==null) return ;
		Class C = receiver.getClass() ;
		Field[] fields = C.getFields() ; // get only the public fields
		for (Field f : fields) {
			serializeObject(f.getName(), f.get(receiver), 0, log) ; // only serialize the top-values of the fields
		}
	}
	

	private void logMethodCallEntry(METHOD m, Object receiver, Object[] args) throws Exception {
		Sample s = new Sample(constructMethodName(m.method) + ":::ENTER", true) ;
		s.nonce = currentNonce ;
		serializeReceiverObj(receiver,s.values) ;
		for (int k=0; k<args.length; k++) {
			serializeObject("arg" + k, args[k], 0, s.values) ; // serialize args only at the top level
		}
		log.add(s) ;
	}
	
	private void logtMethodCallExit(METHOD m, Object receiver, Object[] args, Object retval, Object exc) throws Exception {
		Sample s = new Sample(constructMethodName(m.method) + ":::EXIT", false) ;
		s.nonce = currentNonce ;
		serializeReceiverObj(receiver,s.values) ;
		for (int k=0; k<args.length; k++) {
			serializeObject("arg" + k, args[k], 0, s.values) ; // serialize args only at the top level
		}
		serializeObject("exception",interpretException(exc),0,s.values) ;
		serializeObject("return",retval,1,s.values) ;
		log.add(s) ;
	}
	
	private String interpretException(Object o) {
		if (o==null) return "" ;
		else return o.getClass().getName() ;
	}
	
	private void logConstructorCallEntry(CONSTRUCTOR c, Object[] args) throws Exception {
		Sample s = new Sample(constructConstructorName(c.con) + ":::ENTER", true) ;
		s.nonce = currentNonce ;
		for (int k=0; k<args.length; k++) {
			serializeObject("arg" + k, args[k], 0, s.values) ; // serialize args only at the top level
		}
		log.add(s) ;
	}
	
	private void logtConstructorCallExit(CONSTRUCTOR c, Object[] args, Object retval, Object exc) throws Exception {
		Sample s = new Sample(constructConstructorName(c.con) + ":::EXIT", false) ;
		s.nonce = currentNonce ;
		for (int k=0; k<args.length; k++) {
			serializeObject("arg" + k, args[k], 0, s.values) ; // serialize args only at the top level
		}
		serializeObject("exception",interpretException(exc),0,s.values) ;
		serializeObject("return",retval,1,s.values) ;
		log.add(s) ;
	}
	
	private void logExit(STEP_RT_info info) {
		STEP step = (STEP) info.step;
		try {
  		  if (step instanceof METHOD)
			logtMethodCallExit((METHOD) step,info.receiverObj,info.args,info.returnedObj,info.exc) ;
		  else if (step instanceof CONSTRUCTOR)
			logtConstructorCallExit((CONSTRUCTOR) step,info.args,info.returnedObj,info.exc) ;
		}
		catch(Exception e) { 
			StringWriter sw = new StringWriter() ;
			PrintWriter psw = new PrintWriter(sw) ;
			e.printStackTrace(psw);
			psw.flush();
			Logger.getLogger(CONSTANTS.T3loggerName).warning("** Failed to reate a sample of a step's exit " + step 
					+ "\nThrowing: " + sw.getBuffer());
		}

	}
	
	private void logSequence(Class CUT, Pool pool,List<STEP> sequence) {
		pool.reset() ;
		int k = 0 ;
		for (STEP step : sequence) {
			if (step instanceof INSTRUMENT) continue ;
			STEP_RT_info info = Xutils.executeSTEP(CUT,pool,instrumenter,step,k) ;
			if (info==null) break ;
			logExit(info) ;
			k++ ;
			currentNonce++ ;
		}
	}
	
	/**
	 * Run the suite and return the list of state-samples obtained from it.
	 */
	public SampleList getStateSamples(SUITE suite) throws Exception {
		reset() ;
		Class CUT = Class.forName(suite.CUTname) ;
		Pool pool = new Pool() ;
		for (SEQ seq : suite.suite) logSequence(CUT,pool,seq.steps) ;
		SampleList log_ = new SampleList(log) ;
		log_.fix() ;
		return log_ ;
	}

	/**
	 * Run the suite, and extract a Daikon dtrace log from this execution. The log is written
	 * into the given stream. 
	 */
	public void getDtrace(SUITE suite, PrintStream out) throws Exception {
		SampleList samples = getStateSamples(suite) ;
		samples.writeAsDtrace(out);
		out.flush();
	}
	
	/**
	 * Run the suite, and extract a Daikon dtrace log from this execution. The log is written
	 * into the file.
	 */
	public void getDtrace(SUITE suite, String file) throws Exception {
		PrintStream out = new PrintStream(file) ;
		getDtrace(suite,out) ;
		Logger.getLogger(CONSTANTS.T3loggerName).info("** Saving runtime data to a file: " + file) ;
		out.close();
	}
	
	
	/**
	 * Infer Daikon invariants from a given test suite.
	 */
	public void infer(SUITE suite, String dtraceFile, String invFile) throws Exception {
		getDtrace(suite,dtraceFile) ;
		StringSystemOut altSysOut = new StringSystemOut() ;
		String[] daikonArgs = {"--output_num_samples","--nohierarchy", "-o", invFile, dtraceFile} ;
		altSysOut.reroute();
		try {
		   daikon.Daikon.mainHelper(daikonArgs) ;
		   String s = altSysOut.toString() ;
		   writeFile(invFile + ".txt" , s) ;
		}
		finally {
			altSysOut.restore();
		}
	}
	
	//private void waitx() {
	//	try { Thread.sleep(1000); }
	//	catch (Exception e) {} 
	//}
	
	public void infer(Function<Void,SUITE> generator, int k, String dtraceFile, String invFile) throws Exception {
		SUITE S = generator.apply(null) ;
		SUITE T = null ;
		int i = 0 ;
		boolean consistent = false ;

		while (i<k) {
			infer(S,dtraceFile,invFile) ;
			T = generator.apply(null) ;
			//System.err.println(">>> checking invariants...") ;
			consistent = check(T,invFile,null) ;
			if (consistent) break ;
			S.plus(T) ;
			i++ ;
		}
		String reportName = timestamp("tmpcheckreport__") +  ".txt" ;
		check(T,invFile,reportName) ;
		if(consistent) System.err.println(">>> invariants stabilize. i=" + i + ", suite size=" + S.suite.size()) ;
		else System.err.println(">>> iteration upperbound is exceeded. i=" + i + ", suite size=" + S.suite.size()) ;
	}
	
	static private String timestamp(String s) {
		return s + System.currentTimeMillis() ;
	}
	
	
	public String check_(SUITE suite, String dtraceFile, String invFile, String checkReportFile) throws Exception {
		getDtrace(suite,dtraceFile) ;
		StringSystemOut altSysOut = new StringSystemOut() ;
		String[] daikonArgs ;
		if (checkReportFile == null) {
			String[] args = {"--filter","--conf","--verbose",invFile, dtraceFile} ;
			daikonArgs = args ;			
		}
		else {
			String[] args = {"--filter","--conf","--verbose", "--output", checkReportFile,
					         "--dbg", "--debug",
					         invFile, dtraceFile} ;
			daikonArgs = args ;
		}
		try {
		   altSysOut.reroute(); // rerouting System.out
		   daikon.tools.InvariantChecker.mainHelper(daikonArgs);
		   String s = altSysOut.toString() ;
		   System.err.println(s) ;
		   return s ;
		}
		finally {
			altSysOut.restore();
		}
	}
	
	/**
	 * Check the invariants in a given invariant-file against a given test suite.
	 */
	public boolean check(SUITE suite, String invFile, String checkReportFile) throws Exception {
		//Files.deleteIfExists(Paths.get(checkReportFile)) ;
		String dtraceFile = timestamp("tmp__") + ".dtrace" ;
		String r = check_(suite,dtraceFile,invFile,checkReportFile) ;
		boolean ok = r.contains(" 0 error") ;
		System.err.println(">>> " + ok) ;
		return ok ;
	}
	
	static private String readFile(String f) throws Exception {
		BufferedReader br = null; 
		StringBuffer s = new StringBuffer();
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(f));
			while ((sCurrentLine = br.readLine()) != null) {
				s.append(sCurrentLine) ;
			}
			return s.toString() ;
		} catch (IOException e) { return s.toString() ; } 
		finally { br.close(); }
	}
	
	static private void writeFile(String f, String s) {
	    try {
	    	BufferedWriter out = new BufferedWriter(new FileWriter(f));
	    	out.write(s);
		    out.close();
		} catch (IOException e) {}
	}
	
	
	//  test
	static public void main(String[] args) throws Exception {
		Config config = new Config() ;
		config.CUT = Sequenic.T3.Examples.Friends.Person.class ;
		//config.CUT = Sequenic.T3.Examples.Coba1.class ;
		config.maxPrefixLength = 2;
		config.maxSuffixLength = 1;
		config.suiteSizeMultiplierPerGoal = 10 ;
		config.regressionMode = true ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(null,config) ;
		T3Daikon instr = new T3Daikon() ;
		SUITE S = t3.suite(true) ;
		// test getDtrace
		//instr.getDtrace(S,System.out) ;
		//instr.getDtrace(S,"cobadaikon1.dtrace");
		
		// test infer
		instr.infer(S,"cobadaikon2.dtrace", "cobadaikon2.inv");
		
		// test check
		config.suiteSizeMultiplierPerGoal = 10 ;
		T3SuiteGenAPI t3b = new T3SuiteGenAPI(null,config) ;
		SUITE T = t3b.suite(true) ;
		instr.check(T,"cobadaikon2.inv", "checkResult2.txt") ;
		
		// test iterative infer
		config.suiteSizeMultiplierPerGoal = 5 ;
		//T3SuiteGenAPI t3c = new T3SuiteGenAPI(null,config) ;
		//instr.infer( any -> t3c.suite(true), 10, "cobadaikon3.dtrace", "cobadaikon3.inv");
	}
	
	
}
