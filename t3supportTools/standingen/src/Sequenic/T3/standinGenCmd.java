/*
 * Copyright 2015 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import java.io.*;
import java.lang.reflect.* ;

/**
 * Used to generate a stand-in class, which is used to test an abstract class.
 * An abstract class cannot be instantiated through its constructor, even if a
 * concrete constructor is provided. T3 can still instantiate it if it has a
 * creation method, else there is no way T3 can create an instance.
 * 
 * A stand-in is a concrete class that simply extends an abstract class; so
 * instances of the abstract class can be created through its stand-in.
 * 
 * There are some TODOs left... see the code.
 */
public class standinGenCmd {
	
	static String mkStandInCode(String standinName, Class abstractClass) throws Exception {
		String s = "// a stand-in class for " +  abstractClass + "\n";
		s += "public class " + standinName + " extends " + abstractClass.getName() + "{\n" ;
		
		Constructor[] cons = abstractClass.getDeclaredConstructors() ;
		for (Constructor co : cons) {
			int flag = co.getModifiers() ;
			if (Modifier.isPublic(flag) || Modifier.isProtected(flag)) {
				Type[] tys = co.getGenericParameterTypes() ;
				s += "  public " + standinName + "(" ;
				for (int k=0; k<tys.length; k++) {
					if (k>0) s += ", " ;
					s += tys[k].getTypeName() + " x" + k ;
				}
				s += ") throws Exception { " ;
				s += "super(" ;
				for (int k=0; k<tys.length;k++) {
					if (k>0) s += ", " ;
					s += "x" + k ;
				}
				s += ") ; }\n" ;
			}
		}
		
		// still not complete if the class has an abstract inherited method; TODO!
		Method[] methods = abstractClass.getDeclaredMethods() ;
		for (Method m : methods) {
			int flag = m.getModifiers() ;
			// currently ignoring default method, which require this class to be in the same package; TODO!
			if (Modifier.isAbstract(flag) && (Modifier.isPublic(flag) || Modifier.isProtected(flag))) {
				Type[] tys   = m.getGenericParameterTypes() ;
				Type retty   = m.getGenericReturnType() ;
				Class[] excs = m.getExceptionTypes() ;
				if (Modifier.isStatic(flag)) s += " static " ;
				if (Modifier.isPublic(flag)) s += " public " ;
				if (Modifier.isProtected(flag)) s += " protected " ;
				s += retty.getTypeName() + " " + m.getName() + "(" ;
				for (int k=0; k<tys.length; k++) {
					if (k>0) s += ", " ;
					s += tys[k].getTypeName() + " x" + k ;
				}
				s += ") " ;
				if (excs!=null && excs.length>0) {
					s += "throws " ;
					int k=0 ;
					for (Class e : excs) {
						if (k>0) s += ", " ;
						s += e.getName() ;
						k++;
					}
				}
				
				s += " { " ;
				
				String retty_ = retty.getTypeName() ;
				String ret = " return null " ;
				String[] numeric = { "byte", "short", "int" , "long" , "float", "double" } ;
				for (int k=0; k<numeric.length; k++) if (retty_.equals(numeric[k])) ret = " return 0 " ; 
				if (retty_.equals("boolean")) ret = " return false " ; 
				if (retty_.equals("char")) ret = " return \'0\' " ; 
				s += ret ;
				s += " ; }\n" ;
			}
		}
		
		
		s += "}" ;
		return s ;
	}
	
	
	/**
	 * Save the content in the file.
	 */
    static private void save(File file, String content) throws Exception {
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
    }
	

	public static void generateStandIn(String absclassName, String srcpath, String compileCmd) throws Exception {
		//String absclassName = "com.puppycrawl.tools.checkstyle.api.AbstractLoader" ;
		Class absclass = Class.forName(absclassName) ;
		
		if (! Modifier.isAbstract(absclass.getModifiers())) {
			// if it is not abstract, there is no need to create a stand-in
			return ;
		}
	 	
		String standInName =  absclass.getSimpleName() + "_StandIn" ;
		//String srcpath = "./src" ;
		String filename  = srcpath + "/" + standInName + ".java" ;
    	File file = new File(filename);   	 
    	
    	// if (file.exists()) return ; well just overwrite 
    	
		save(file, mkStandInCode(standInName,absclass)) ;
		System.err.println("** a stand-in " + filename + " is created.") ;
		
		compileCmd += " " + filename ;
		System.err.println("** about to execute: " + compileCmd) ;
		Process p = Runtime.getRuntime().exec(compileCmd);
		p.waitFor();

		// echo msg we get from the javac:
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		String line = "";			
		while ((line = reader.readLine())!= null) {
			System.err.println(">> " + line) ; 
		}
		
		System.err.println("** a stand-in " + filename + " is compiled.") ;
	}
	
	/**
	 * arg[0] : the full name of the abstract class whose stand-in must be created
	 * arg[1] : the path to the src directory to put the stand-in
	 * arg[2] : javac command to compile, e.g. javac -cp bla
	 */
	public static void main(String[] args) throws Exception {
		generateStandIn(args[0],args[1],args[2]) ;	
		// generateStandIn("Sequenic.T3.Examples.Abstractx.Vehicle",".","c:/apps/Java/jdk1.8u31/bin/javac -cp ./bin;../T3_v0/bin") ;
		// generateStandIn("Sequenic.T3.Examples.Abstractx.Vehicle",".","c:/apps/Java/jdk1.8u31/bin/javac -version") ;
	}

}
