package Sequenic.T3.Configure;

import java.io.* ;
import java.lang.reflect.*;
import java.util.*;
import java.util.Map.* ;
import java.util.function.Function;

import Sequenic.T3.Config;
import Sequenic.T3.T3SuiteGenAPI;
import Sequenic.T3.TestingScope;
import Sequenic.T3.Generator.*;
import Sequenic.T3.Sequence.Datatype.*;

public class SBSTConfWizard {
	
	InheritenceMap inh ;
	
	
	/**
	 * Check if it is sensical to apply an ADT test to a class.
	 */
	public static boolean sensicalForADTtest(Class CUT) {
		return has_NonPrivateDeclared_IMethod_OrConstructor(CUT) ;
	}
	
	/**
	 * Check if it is sensical to apply non-ADT test to a class. Note that both ADT and
	 * nonADT can be sensical.
	 */
	public static boolean sensicalForNonADTtest(Class CUT) {
		return has_NonPrivateDeclared_SMethod(CUT) ;
	}
	
	/**
	 * Check if C has non-private instance method or constructor that it declares itself.
	 */
	static private boolean has_NonPrivateDeclared_IMethod_OrConstructor(Class C) {
		Method[] methods = C.getDeclaredMethods() ;
		for (Method m : methods) {
			int flag = m.getModifiers() ;
			if (!Modifier.isStatic(flag) && !Modifier.isPrivate(flag)) return true ;
		}
		Constructor[] constructors = C.getDeclaredConstructors() ;
		for (Constructor c : constructors) {
			int flag = c.getModifiers() ;
			if(!Modifier.isStatic(flag) && !Modifier.isPrivate(flag))  return true ;
		}
		return false ;
	}
	
	/**
	 * Check if C has non-private static method that it declares itself.
	 */
	static private boolean has_NonPrivateDeclared_SMethod(Class C) {
		Method[] methods = C.getDeclaredMethods() ;
		for (Method m : methods) {
			int flag = m.getModifiers() ;
			if (Modifier.isStatic(flag) && !Modifier.isPrivate(flag)) return true ;
		}
		return false ;
	}
	
	/**
	 * Return static inner classes.
	 */
	public static List<Class> getStaticInnerClasses(Class C) {
		Class[] innerclasses = C.getDeclaredClasses() ;
		List<Class> R = new LinkedList<Class>() ;
		for (Class ic : innerclasses) {
			if (ic.isInterface()) continue ;
			int flag = ic.getModifiers() ;
			//if (Modifier.isStatic(flag) && !Modifier.isPrivate(flag)) R.add(ic) ;
			if (Modifier.isStatic(flag)) R.add(ic) ;
		}
		return R ;
	}
	
	public List<T3SuiteGenAPI> configure(
			Class CUT,
			Function<Class,Config>  configMaker,
			Generator<PARAM,STEP>   customValueGen,
			String inhMapFile
			) throws Exception
	{ 		 
		InheritenceMap inhMap = new InheritenceMap() ;
	    inhMap.readInhMap(inhMapFile);
	    List<T3SuiteGenAPI> t3instances = new LinkedList<T3SuiteGenAPI>() ;
	    return configureWorker(CUT,configMaker,customValueGen,inhMap,t3instances) ;
	}
	
	private List<T3SuiteGenAPI> configureWorker(
			Class CUT,
			Function<Class,Config>  configMaker,
			Generator<PARAM,STEP>   customValueGen,
			InheritenceMap inhMap,
			List<T3SuiteGenAPI> t3instances
			) 
	{
		 Config config = configMaker.apply(CUT) ;
		 T3SuiteGenAPI t3 = T3SuiteGenAPI.mkT3SuiteGenAPI(customValueGen,config, inhMap.classes ) ;
		 boolean adt = sensicalForADTtest(CUT) ;
		 boolean nonAdt = sensicalForNonADTtest(CUT) ;
		 
		 // if nonAdt testing is appropriate, always do it:
		 t3instances.add(t3) ;
		 
		 // handle the special case that CUT is abstract, adt-compatible, and has no
		 // creator method:
		 if (Modifier.isAbstract(CUT.getModifiers()) && adt) {
			 TestingScope scope = t3.scope ;
			 scope.configureForADTtesting();
			 if (! scope.creatorMethods.isEmpty()) {
				 // CUT is adt-compatible, but abstract and has no creatot method!
				 // test tru a subclass instead:
				 System.err.println("** " + CUT.getName() 
				           + " is an abstract class, adt-compatible, but no creator method!" ) ;
				 Class replacement = t3.impMap.getImp1(CUT) ;
				 if (replacement != null) {
					 System.err.println("** Targeting " + replacement.getName() + " instead...") ;
					 configureWorker(replacement,configMaker,customValueGen,inhMap,t3instances) ;
				 }
			 }	 
		 }
		 
		 // if CUT is concrete and adt-compatible, then test it 
		 if (! Modifier.isAbstract(CUT.getModifiers()) && adt) {
			if (!t3instances.contains(t3)) t3instances.add(t3) ;
			 
			// handle the case that CUT is concrete, but has no non-private constructor nor
			 // creation method. 
			 // --> forcefully add all, incl private, constructors in the scope :
			 TestingScope scope = t3.scope ;
			 scope.configureForADTtesting();
			 if (scope.constructors.isEmpty() && scope.creatorMethods.isEmpty()) {
			       Constructor[] cos = CUT.getDeclaredConstructors() ; 
			       for(Constructor co : cos) scope.forceConstructors.add(co) ;
			 }
		 }
		 
		 // if CUT has inner classes, test them too
		 for (Class D : getStaticInnerClasses(CUT)) {
			 configureWorker(D,configMaker,customValueGen,inhMap,t3instances) ;
		 }
		 
		 return t3instances ;
	}
	
	
	

	
}
