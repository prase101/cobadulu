package Sequenic.T3.Configure;

import java.io.*;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.Map.*;

public class InheritenceMap {
	
    Map<Class,List<Class>> inhMap = new HashMap<Class,List<Class>>() ;
    Class[] classes ;
	
	public void printInhMap() {
		//System.out.print("\n>> #entries: " + inhMap.size() ) ;
		for (Entry<Class,List<Class>> e : inhMap.entrySet()) {
			System.out.print("\n>> " + e.getKey() + " super of: " ) ;
			int k = 0 ;
			for (Class D : e.getValue()) {
				if (k>0) System.out.print(", ") ;
				System.out.print("" + D) ;
				k++ ;
			}
		}
	}
		
	/**
	 * To read an inheritence-map from a text file.
	 */
	public void readInhMap(String fname) throws Exception {
		BufferedReader br = null;
		try {
			String s ;
			br = new BufferedReader(new FileReader(fname));
			while ((s = br.readLine()) != null) {
				String[] classnames = s.split(":") ;
			    //System.out.println("** " + classnames.length) ;
				if (classnames.length == 0) continue ;
				try {
					Class C = Class.forName(classnames[0]);
					//System.out.println("** C: " + C) ;
					List<Class> subclasses = new LinkedList<Class>() ;
					inhMap.put(C, subclasses) ;
					//System.out.println("** size: " + inhMap.size()) ;
					for (int k=1; k<classnames.length; k++) {
						try {
							Class D = Class.forName(classnames[k]);
							//System.out.println("** D: " + D) ;
							subclasses.add(D) ;
						}
						catch (Exception e){}
					}
				}
				catch(Exception e) {}
			}
		}
		finally {
			if (br != null) br.close();
			classes = new Class[inhMap.size()] ;
			inhMap.keySet().toArray(classes) ;
			//System.out.println(">> #classes " + classes.length) ;
			transitiveClosure();
		}
		
	}
	
	private void transitiveClosure() {
		for (Entry<Class,List<Class>> e : inhMap.entrySet()) {
			List<Class> subclasses = e.getValue() ;
			Set<Class> seen = new HashSet<Class>() ;
			Class C = e.getKey() ;
			seen.add(C) ;
			subclasses.addAll(getSubclasses(seen,C)) ;
		}
	}
	
	private Set<Class> getSubclasses(Set<Class> seen, Class C) {
		Set<Class> descs = new HashSet<Class>() ;
		if (seen.contains(C)) return descs ;
		descs.add(C) ;
		seen.add(C) ;
		for (Class D : inhMap.get(C)) descs.addAll(getSubclasses(seen,D)) ;
		return descs ;
	}
	
	/**
	 * Return a list of all subclasses of C, as far as they are known in the
	 * inheritence map, which are not interface nor abstract. 
	 */
	public List<Class> getPublicConcreteSubclasses(Class C) {
		List<Class> r = new LinkedList<Class>() ;
		List<Class> candidates = inhMap.get(C) ;
		if (candidates == null) return r ;
		for (Class D : candidates) {
			if (D.isInterface() && Modifier.isAbstract(D.getModifiers())) continue ;
			r.add(D) ;
		}
		return r ;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("...") ;
		InheritenceMap m = new InheritenceMap() ;
		m.readInhMap("imaptest.txt");
		m.printInhMap();
	}

}
