package Sequenic.T3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Modifier;
import java.util.*;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

/**
 * Utility to scan JVM classpath to collect all class names under a given package, along with 
 * their inheritence relations, then save the info to a ":" separated text file.
 */
public class cpscannerCmd {
	
	static private void save(String fname, String content) throws Exception {
    	File file = new File(fname);   	 
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
    }
	
	/**
	 * List all classes under (recursive) the given package. For each, we also list all its subclasses
	 * found under the package. Each class is also listed with its modifier. Then, the whole information
	 * is saved to a file.
	 */
	public static String MkInheritenceMap(String pckgName, String savefile) throws Exception {
		
		Reflections reflections = new Reflections(pckgName, new SubTypesScanner(false));

		Set<Class<? extends Object>> allclasses = reflections.getSubTypesOf(Object.class) ;
		Map inheritence = new HashMap<Class,List<Class>>() ;
		String z = "" ;
		int k = 0 ;
		for (Class C : allclasses) {
			List<Class> subclasses = new LinkedList<Class>() ;
			if (k>0) z += "\n" ;
			// System.out.print("" + C + ":" + C.getModifiers()) ;
			z += C.getName()  ;
			inheritence.put(C, subclasses) ;
			for (Class D : allclasses) {
				if (C.isAssignableFrom(D)) { 
					//System.out.print(":" + D ) ;
					z += ":" + D.getName() ;
					subclasses.add(D) ;
				}
			}
			//System.out.println("") ;
			k++ ;
		}
		save(savefile,z) ;
		return z ;
	}

	/**
	 * arg0: package full name
	 * arg1: name of the text-file to save the inheritence-info
	 */
	
	public static void main(String[] args) throws Exception {
		//System.out.print(MkInheritenceMap("Sequenic.T3.Examples","imaptest.txt")) ; 
		MkInheritenceMap(args[0],args[1]) ;
	}

}
